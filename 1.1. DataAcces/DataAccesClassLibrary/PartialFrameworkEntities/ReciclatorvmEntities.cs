﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    /**
     *  Al ser Partial "extiende" al ReciclatorvmEntities autogenerado dandole un constructor sobrecargado con el string de conexion.
     *  Al estar por fuera de las cosas autogeneradas nose eliminara.
     *      Su creacion y el codigo de pruebas de Program.cs se realizaron despues de instalar y armar el modelo de ADO.Net Entity Data Model
     **/
    public partial class ReciclatorvmEntities{

        public ReciclatorvmEntities(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

    }
}
