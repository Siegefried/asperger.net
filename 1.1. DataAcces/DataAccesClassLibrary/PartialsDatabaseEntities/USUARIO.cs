﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObjects.Entities;

namespace DataAccesClassLibrary
{
    public partial class USUARIO
    {

        /*public Usuarios()
        {
        }*/
        
        public DTLogIn GetDTO()
        {
            return new DTLogIn()
            {
                id = this.Id,
                nombre = this.Nombre,
                apellido = this.Apellido,
                email = this.Mail,
                nickname = this.Nickname,
                password = this.Password,
                edad = this.Edad,
                bloqueado = this.Bloqueado
            };
        }

    }
}
