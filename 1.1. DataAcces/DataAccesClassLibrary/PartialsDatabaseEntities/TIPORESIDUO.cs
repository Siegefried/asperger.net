﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class TIPOS_DE_BASURA
    {
        public DTTipoResiduo GetDTO()
        {
            return new DTTipoResiduo()
            {
                nombre = this.Nombre,
                color = this.Color,
                id = this.Id,
                bloqueado = this.Bloqueado
            };
        }
    }
}
