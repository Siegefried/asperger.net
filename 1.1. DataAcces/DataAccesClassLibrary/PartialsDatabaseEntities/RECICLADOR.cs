﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class RECICLADORE
    {
        public DTReciclador GetDTO()
        {
            return new DTReciclador()
            {
                Id = this.Id,
                usuario = this.USUARIO.GetDTO()
            };
        }
    }


}
