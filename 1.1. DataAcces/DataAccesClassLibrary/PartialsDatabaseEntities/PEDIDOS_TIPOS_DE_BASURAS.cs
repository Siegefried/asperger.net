﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class PEDIDOS_TIPOS_DE_BASURAS
    {
        public DTMaterialCantidad GetDTO()
        {
            return new DTMaterialCantidad
            {
                Cantidad = this.Cantidad,
                Nombre = this.TIPOS_DE_BASURAS.Nombre
            };
        }
    }
}
