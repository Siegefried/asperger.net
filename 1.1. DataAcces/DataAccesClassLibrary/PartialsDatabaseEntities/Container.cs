﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObjects.Entities;

namespace DataAccesClassLibrary
{
    public partial class CONTAINER
    {
        public DTContainer GetDTO()
        {
            return new DTContainer()
            {
                Latitud = this.UBICACIONE.Latitud,
                Longitud = this.UBICACIONE.Longitud,
                Id = this.Id,
                TipoBasura = this.TIPOS_DE_BASURAS.Nombre,
                Color = this.TIPOS_DE_BASURAS.Color,
                Lleno = this.Lleno
            };
        }
    }
}
