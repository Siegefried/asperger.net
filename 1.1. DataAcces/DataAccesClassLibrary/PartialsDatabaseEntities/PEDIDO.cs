﻿using DataTransferObjects.Entities;
using DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class PEDIDO
    {
        public DTPedido GetDTO()
        {
            DTPedido Res = new DTPedido()
            {
                Id = this.Id,
                Fecha = this.Fecha,
                Hora = this.Hora,
                Persona = this.PERSONA.USUARIO.Nombre + " " + this.PERSONA.USUARIO.Apellido,
                Reciclador = null,
                Ubicacion = new Ubicacion { Id = this.UBICACIONE.Id, Latitud = this.UBICACIONE.Latitud, Longitud = this.UBICACIONE.Longitud },
                Valoracion = (Valoracion)this.Valoracion,
                Estado = this.ESTADO1.GetDTO(),
                Observaciones = this.Observaciones,
                Materiales = null
            };
            
            if (this.IdReciclador > 0)
            {
                Res.Reciclador = this.RECICLADORE.GetDTO();
            }

            if (this.PEDIDOS_TIPOS_DE_BASURAS != null)
            {
                Res.Materiales = new List<DTMaterialCantidad>();

                foreach (PEDIDOS_TIPOS_DE_BASURAS Index in this.PEDIDOS_TIPOS_DE_BASURAS)
                {
                    Res.Materiales.Add(Index.GetDTO());
                }
            }

            return Res;
        }

    }
}
