﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class UBICACIONE
    {
        public Ubicacion GetDTO()
        {
            return new Ubicacion()
            {
                Latitud = this.Latitud,
                Longitud = this.Longitud,
                Id = this.Id,
                Alias = this.Alias
            };
        }
    }
}
