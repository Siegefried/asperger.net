﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public partial class ESTADO
    {
        public DTEstado GetDTO()
        {
            return new DTEstado()
            {
                Id = this.Id,
                Estado = this.Estado1
            };
        }

    }
}
