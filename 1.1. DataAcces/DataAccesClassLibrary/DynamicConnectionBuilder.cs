﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesClassLibrary
{
    public class DynamicConnectionBuilder
    {
        public static string build()
        {


            String machineName = Environment.MachineName;
            String modelName = "Model1"; // Nombre puesto al instalar lo deel Entity Framework BD First

            //Build an SQL connection string
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {
                DataSource = machineName + "\\SQLEXPRESS",
                InitialCatalog = "Reciclatorvm",
                IntegratedSecurity = true
                //UserID = database.Username,
                //Password = database.Password,
            };

            //Build an entity framework connection string
            EntityConnectionStringBuilder ecsBuilder = new EntityConnectionStringBuilder()
            {
                Provider = "System.Data.SqlClient",
                //Metadata = Settings.Default.Metadata,
                Metadata = @"res://*/" + modelName + @".csdl|
                            res://*/" + modelName + @".ssdl|
                            res://*/" + modelName + @".msl",
                ProviderConnectionString = sqlString.ToString()
            };

            string nuConnString = ecsBuilder.ToString();
            return nuConnString;

        }


    }
}
