//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccesClassLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class TIPOS_DE_BASURA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TIPOS_DE_BASURA()
        {
            this.CONTAINERS = new HashSet<CONTAINER>();
            this.PEDIDOS_TIPOS_DE_BASURAS = new HashSet<PEDIDOS_TIPOS_DE_BASURAS>();
        }
    
        public string Nombre { get; set; }
        public long Id { get; set; }
        public string Color { get; set; }
        public bool Bloqueado { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTAINER> CONTAINERS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PEDIDOS_TIPOS_DE_BASURAS> PEDIDOS_TIPOS_DE_BASURAS { get; set; }
    }
}
