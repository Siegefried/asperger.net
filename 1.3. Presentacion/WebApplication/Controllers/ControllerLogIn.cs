﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using WebApplication.ServiceReference2;   // al actualizar referencia coordina DTlogin DataTransferObjects con el de ServiceReferenceN

namespace WebApplication.Controllers
{
    public class ControllerLogIn
    {
        public DTLogIn logIn(string nick, string pass)
        {
            ServiceReference2.ServiceLogInClient proxy = new ServiceReference2.ServiceLogInClient();
            DTLogIn login = proxy.login(nick, pass);
            return login;
        }

        public DTLogIn externallogIn(string email)
        {
            ServiceReference2.ServiceLogInClient proxy = new ServiceReference2.ServiceLogInClient();
            DTLogIn login = proxy.externalLogin(email);
            return login;
        }

        public bool emailUserValidation(string email, string validationToken)
        {
            ServiceReference2.ServiceLogInClient proxy = new ServiceReference2.ServiceLogInClient();
            return proxy.emailUserValidation(email, validationToken);
        }

        public bool isAdmin(string nick)
        {
            return new ServiceReference2.ServiceLogInClient().isAdmin(nick);
        }
        public bool isPersona(string nick)
        {
            return new ServiceReference2.ServiceLogInClient().isPersona(nick);
        }
        public bool isReciclador(string nick)
        {
            return new ServiceReference2.ServiceLogInClient().isRecolector(nick);
        }

        public bool checkMail(string email)
        {
            return new ServiceReference2.ServiceLogInClient().checkMail(email);
        }

        public void RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ServiceReference2.ServiceLogInClient().RegistrarPersona(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        public void RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ServiceReference2.ServiceLogInClient().RegistrarReciclador(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        public bool checkUser(string nick)
        {
            return new ServiceReference2.ServiceLogInClient().checkUser(nick);
        }

        public Byte[] getUserImage(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getUserImage(Id);
        }

        public List<Ubicacion> getUbicacionesPersona(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getUbicacionesPersona(Id).ToList();
        }

        public List<Ubicacion> getUbicacionReciclador(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getUbicacionReciclador(Id).ToList();
        }

        public List<DTPedido> getPedidosPersona(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getPedidosPersona(Id).ToList();
        }

        public List<DTPedido> getPedidosReciclador(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getPedidosReciclador(Id).ToList();
        }

        public List<DTPedido> getAllPedidosReciclador(long Id)
        {
            return new ServiceReference2.ServiceLogInClient().getAllPedidosReciclador(Id).ToList();
        }

        public void BorrarBajaUbicacion(long Id)
        {
            new ServiceReference2.ServiceLogInClient().BorrarBajaUbicacion(Id);
        }

        public void AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new ServiceReference2.ServiceLogInClient().AddUbicacionPersona(idPersona, Alias, Latitud, Longitud);
        }

        public void AddUbicacionReciclador(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new ServiceReference2.ServiceLogInClient().AddUbicacionReciclador(idPersona, Alias, Latitud, Longitud);
        }

        public void CambiarFotoPerfilPersona(Byte[] InfoImagen, long id)
        {
            new ServiceReference2.ServiceLogInClient().CambiarFotoPerfilPersona(InfoImagen, id);
        }

        public void CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ServiceReference2.ServiceLogInClient().CambiarDatosUsuario(id, Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        public List<DTContainer> getContainersNoLlenos()
        {
            return new ServiceReference2.ServiceLogInClient().getContainersNoLlenos().ToList();
        }

        public void modifyContainerLleno(long IdC, long IdP) {
            new ServiceReference2.ServiceLogInClient().modifyContainerLleno(IdC, IdP);
        }

        public bool CheckTipoDeBasura(string NombreTipoDeBasura)
        {
           return new ServiceReference2.ServiceLogInClient().CheckTipoDeBasura(NombreTipoDeBasura);
        }

        public void AgregarTipoDeBasura(string Nombre, string Color)
        {
           new ServiceReference2.ServiceLogInClient().AgregarTipoDeBasura(Nombre, Color);
        }

        public List<string> TraeTiposBasuraString()
        {
            return new ServiceReference2.ServiceLogInClient().TraeTiposBasuraString().ToList();
        }

        public void AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos)
        {
            new ServiceReference2.ServiceLogInClient().AgregarPedido(Fecha, Hora, IdPersona, IdUbicacion, Observaciones, Residuos.ToArray());
        }

        public void updatePedidosResumen(DTPedido pedido)
        {
            new ServiceReference2.ServiceLogInClient().updatePedidosResumen(pedido);
        }

        public List<DTMiReciclador> TraerMisRecicladores(long idPersona) {
            return new ServiceReference2.ServiceLogInClient().TraerMisRecicladores(idPersona).ToList();
        }

        public void BloquearReciclador(long idPersona, long idReciclador)
        {
            new ServiceReference2.ServiceLogInClient().BloquearReciclador(idPersona, idReciclador);
        }

        public void DesbloquearReciclador(long idPersona, long idReciclador)
        {
            new ServiceReference2.ServiceLogInClient().DesbloquearReciclador(idPersona, idReciclador);
        }

        public List<string> TraerListaUsuarios()
        {
            return new ServiceReference2.ServiceLogInClient().TraerListaUsuarios().ToList();
        }

        public DTLogIn GetDatosPersona(string nickname)
        {
            return new ServiceReference2.ServiceLogInClient().GetDatosPersona(nickname);
        }

        public List<DTContainer> getContainersAll() {
            return new ServiceReference2.ServiceLogInClient().getContainersAll().ToList();
        }

        public void modifyContainerLlenoAdmin(long IdC) {
            new ServiceReference2.ServiceLogInClient().modifyContainerLlenoAdmin(IdC);
        }

        public List<DTMaterialCantidad> TraerInfoMaterialCantidad(long IdPedido)
        {
            return new ServiceReference2.ServiceLogInClient().TraerInfoMaterialCantidad(IdPedido).ToList();
        }

        public void EliminarContainer(long IdC)
        {
            new ServiceReference2.ServiceLogInClient().EliminarContainer(IdC);
        }

        public void AddContainer(long IdTB, double Latitud, double Longitud)
        {
            new ServiceReference2.ServiceLogInClient().AddContainer(IdTB, Latitud, Longitud);
        }

        public void BloquearDesbloquearUsuario(long Id)
        {
            new ServiceReference2.ServiceLogInClient().BloquearDesbloquearUsuario(Id);
        }

        public List<DTTipoResiduo> TraerDatosTiposDeBasura()
        {
            return new ServiceReference2.ServiceLogInClient().TraerDatosTiposDeBasura().ToList();
        }

        public void BloquearDesbloquearTipoResiduo(long Id)
        {
            new ServiceReference2.ServiceLogInClient().BloquearDesbloquearTipoResiduo(Id);
        }

        public void EditarTipoResiduo(long Id, string Nombre, string Color)
        {
            new ServiceReference2.ServiceLogInClient().EditarTipoResiduo(Id, Nombre, Color);
        }

        public bool isUsuario(long id)
        {
            return new ServiceReference2.ServiceLogInClient().isUsurio(id);
        }

        public bool isTipoResiduo(long id)
        {
            return new ServiceReference2.ServiceLogInClient().isTipoResiduo(id);
        }

        public bool ExisteTipoResiduo(string Nombre)
        {
            return new ServiceReference2.ServiceLogInClient().ExisteTipoResiduo(Nombre);
        }

        public List<DTContainer> getContainersLlenos()
        {
            return new ServiceReference2.ServiceLogInClient().getContainersLlenos().ToList();
        }

        public List<DTEstadistica<DTLogIn>> rankPersonas(int daysBefore)
        {
            return new ServiceReference2.ServiceLogInClient().rankPersonas(daysBefore).ToList();
        }

        public List<DTEstadistica<DTLogIn>> rankRecicladores(int daysBefore)
        {
            return new ServiceReference2.ServiceLogInClient().rankRecicladores(daysBefore).ToList();
        }

        public List<DTEstadistica<DTTipoResiduo>> tiposResiduosMasReciclados(int daysBefore)
        {
            return new ServiceReference2.ServiceLogInClient().tiposResiduosMasReciclados(daysBefore).ToList();
        }

        public Ubicacion getUbicacionPrincipalRecicladores(long IdR)
        {
            return new ServiceReference2.ServiceLogInClient().getUbicacionPrincipalRecicladores(IdR);
        }

        public void modifyUbicacionPrincipalRecicladores(long IdR, double Latitud, double Longitud)
        {
            new ServiceReference2.ServiceLogInClient().modifyUbicacionPrincipalRecicladores(IdR, Latitud, Longitud);
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorFiltro(long IdR, long IdTR, bool Horario, DateTime Fecha)
        {
            return new ServiceReference2.ServiceLogInClient().TraerPedidosDisponiblesRecicladorFiltro(IdR, IdTR, Horario, Fecha).ToList();
        }

        public void LockPedido(long IdR, long IdP)
        {
            new ServiceReference2.ServiceLogInClient().LockPedido(IdR, IdP);
        }

        public void unLockPedido(long IdP)
        {
            new ServiceReference2.ServiceLogInClient().unLockPedido(IdP);
        }

        public List<DTPedido> TraerPedidosReciclador(long IdR, bool Horario, DateTime Fecha) {
            return new ServiceReference2.ServiceLogInClient().TraerPedidosReciclador(IdR, Horario, Fecha).ToList();
        }

        public List<Ubicacion> getRUTA(long IdR, List<Ubicacion> Pasos)
        {
            return new ServiceReference2.ServiceLogInClient().getRUTA(IdR, Pasos.ToArray()).ToList();
        }

        public void RecolectPedido(long IdP)
        {
            new ServiceReference2.ServiceLogInClient().RecolectPedido(IdP);
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha)
        {
            return new ServiceReference2.ServiceLogInClient().TraerPedidosDisponiblesRecicladorMultiFiltro(IdR, ltTipos.ToArray(), Horario, Fecha).ToList();
        }
    }
}