﻿using Nemiro.OAuth;
using Nemiro.OAuth.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebApplication
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            OAuthManager.RegisterClient(
                new GoogleClient(
                    "357943556610-3li9ud3pnjq6do2r3305hkcl8lhohru7.apps.googleusercontent.com",
                    "DLge6YUywvqntu1ixApf6jvp"
                )
                {
                    Scope = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/user.birthday.read"
                }
            );
        }
    }
}