﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication.Pages
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["LoggedUser"] = null;
            Session["extAuthResult"] = null;
            Response.Redirect("/Pages/PaginaPrincipal.aspx");
        }
    }
}