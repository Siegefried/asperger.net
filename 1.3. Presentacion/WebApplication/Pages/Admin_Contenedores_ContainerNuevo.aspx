﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_Contenedores_ContainerNuevo.aspx.cs" Inherits="WebApplication.Pages.Admin_Contenedores_ContainerNuevo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/Admin_Contenedor_ContainerNuevo.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class=" loginbox">
            <div class="Contenedor2">
                <div class="ninjadiv"></div>
                <asp:ScriptManager ID="ScriptManagerAlpha" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div>
                            <map:GoogleMap ID="MapUbicaciones"
                                runat="server"
                                Latitude="-34.9000015" Longitude="-54.9500008"
                                Zoom="16"
                                MapType="Roadmap">
                            </map:GoogleMap>
                            <map:GoogleMarkers ID="MapMarcadores"
                                TargetControlID="MapUbicaciones"
                                runat="server" OnDragEnd="MapMarcadores_DragEnd">
                            </map:GoogleMarkers>
                        </div>
                        <p>
                            Latitud:<asp:Label ID="lblLatitud" runat="server" Text="-34.9000015"></asp:Label>
                        </p>
                        <p>
                            Longitud:<asp:Label ID="lblLongitud" runat="server" Text="-54.9500008"></asp:Label>
                        </p>
                        <p>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:DropDownList ID="ddlTiposResiduo" runat="server" OnSelectedIndexChanged="ddlTiposResiduo_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <asp:Button ID="btnAgregar" CssClass="Botones" runat="server" Text="Agregar" OnClick="btnAgregar_Click" />
            <br />
            <asp:Button ID="btnAtras" CssClass="Botones" runat="server" Text="Atras" OnClick="btnAtras_Click" />

        </div>
    </form>
</body>
</html>

