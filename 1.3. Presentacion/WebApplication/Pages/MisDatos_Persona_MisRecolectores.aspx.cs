﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisDatos_Persona_MisRecolectores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["LoggedUser"] = new ControllerLogIn().logIn("CamilaG", "123456");
            if (!IsPostBack)
            {
                Session["Recolectores"] = new ControllerLogIn().TraerMisRecicladores(((DTLogIn)Session["LoggedUser"]).id);
                lvRecicladores.DataSource = ((List<DTMiReciclador>) Session["Recolectores"]);
                lvRecicladores.DataBind();
            }
        }

        protected void btnBloquear_Click(object sender, EventArgs e)
        {
            if (lvRecicladores.SelectedIndex>-1) {
                new ControllerLogIn().BloquearReciclador(((DTLogIn)Session["LoggedUser"]).id, ((List<DTMiReciclador>)Session["Recolectores"])[lvRecicladores.SelectedIndex].IdReciclador);
            }
            Response.Redirect(Request.RawUrl);
        }

        protected void btnDesbloquear_Click(object sender, EventArgs e)
        {
            if (lvRecicladores.SelectedIndex > -1)
            {
                new ControllerLogIn().DesbloquearReciclador(((DTLogIn)Session["LoggedUser"]).id, ((List<DTMiReciclador>)Session["Recolectores"])[lvRecicladores.SelectedIndex].IdReciclador);
            }
            Response.Redirect(Request.RawUrl);
        }

        protected void lvRecicladores_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((List<DTMiReciclador>)Session["Recolectores"])[lvRecicladores.SelectedIndex].Bloqueado == true)
            {
                btnBloquear.Visible = false;
                btnDesbloquear.Visible = true;
            }
            else
            {
                btnBloquear.Visible = true;
                btnDesbloquear.Visible = false;
            }
        }
        
        protected void lvRecicladores_SelectedIndexChanging(object sender, ListViewSelectEventArgs e)
        {
            lvRecicladores.SelectedIndex = e.NewSelectedIndex;
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Persona.aspx");
        }
    }
}