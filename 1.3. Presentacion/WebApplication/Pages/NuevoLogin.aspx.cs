﻿using DataTransferObjects.Entities;
using Nemiro.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class NuevoLogin : System.Web.UI.Page
    {
        //private static string authName = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario.Attributes.Add("PlaceHolder", "Introduce tu usuario");
            contrasenia.Attributes.Add("PlaceHolder", "Introduce tu contraseña");
            if (Session["Bloqueado"] != null && (bool)Session["Bloqueado"])
            {
                Label1.Visible = true;
            }
        }
        protected void Loggear_Click(object sender, EventArgs e)
        {
            DTLogIn Persona = new ControllerLogIn().GetDatosPersona(usuario.Text);
            if (Persona.bloqueado)
            {
                Label1.Visible = true;
            }
            else
            {
                Label1.Visible = false;
                string nick = usuario.Text.Trim();
                string pass = contrasenia.Text.Trim();

                DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);

                if (logIn != null)
                {
                    Session["LoggedUser"] = logIn;
                    Response.Redirect("/Pages/LoginConfirm.aspx");
                }
            }
        }

        protected void RedirectToLogin_Click(object sender, EventArgs e)
        {
            //DTLogIn Persona = new ControllerLogIn().GetDatosPersona(usuario.Text);
            //else
            //{
            //Label1.Visible = false;
            string provider = ((Button)sender).Attributes["data-provider"];
            // build the return address
            string returnUrl = new Uri(Request.Url, "ExternalLoginResult.aspx").AbsoluteUri;
            // redirect user into external site for authorization
            OAuthWeb.RedirectToAuthorization(provider, returnUrl);
            //}

        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (new ControllerLogIn().logIn(usuario.Text.Trim(), contrasenia.Text.Trim()) == null)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/PaginaPrincipal.aspx");
        }
    }
}