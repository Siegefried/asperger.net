﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Admin_Contenedores_ContainerNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MapMarcadores.MarkerOptions.Draggable = true;
            MapUbicaciones.MapType = MapType.Roadmap;
            if (!IsPostBack)
            {
                Session["TiposResiduo"] = new ControllerLogIn().TraerDatosTiposDeBasura();
                Session["TipoResiduo"] = ((List<DTTipoResiduo>)Session["TiposResiduo"])[0].id;
                foreach (DTTipoResiduo Index in (List<DTTipoResiduo>)Session["TiposResiduo"]) {
                    ListItem IndexListItem = new ListItem();
                    IndexListItem.Text = Index.nombre;
                    IndexListItem.Value = Index.nombre;
                    //IndexListItem.Attributes.Add("style", "background-color:"+Index.color);
                    Color Color = System.Drawing.ColorTranslator.FromHtml("#"+Index.color);
                    Color Contrario = Color.FromArgb(255 - Color.R, 255 - Color.G, 255 - Color.B);
                    IndexListItem.Attributes.Add("style", "color:" + ColorTranslator.ToHtml(Contrario) + "; background-color: #" + Index.color);

                    ddlTiposResiduo.Items.Add(IndexListItem);
                }
                Session["Ubicacion"] = new LatLng((double)MapUbicaciones.Latitude, (double)MapUbicaciones.Longitude);
                MapMarcadores.Add(new Marker
                {
                    Position = getLatLngUbicacion(),
                    Info = "Nueva ubicacion :D",
                    Draggable = true
                });
            }
        }
        protected LatLng getLatLngUbicacion()
        {
            return (LatLng)Session["Ubicacion"];
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            new ControllerLogIn().AddContainer((long)Session["TipoResiduo"], getLatLngUbicacion().Latitude, getLatLngUbicacion().Longitude); //TODO nuevo container
            Response.Redirect("Admin_Contenedores.aspx");
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("Admin_Contenedores.aspx");
        }

        protected void MapMarcadores_DragEnd(object sender, GoogleMaps.Markers.MarkerEventArgs e)
        {
            Session["Ubicacion"] = e.Position;
            lblLatitud.Text = e.Position.Latitude.ToString();
            lblLongitud.Text = e.Position.Longitude.ToString();
        }

        protected void ddlTiposResiduo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["TipoResiduo"] = ((List<DTTipoResiduo>)Session["TiposResiduo"])[ddlTiposResiduo.SelectedIndex].id;
        }
    }
}