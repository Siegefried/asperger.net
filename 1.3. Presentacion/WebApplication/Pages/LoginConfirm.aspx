﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginConfirm.aspx.cs" Inherits="WebApplication.Pages.LoginConfirm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table height="100%" width="100%">
                <td align="center" valign="center">
                    <asp:Label ID="NombreUser" runat="server" Text="Procesando" Font-Size="XX-Large"></asp:Label>
                    <br>
                    <asp:Image ID="Image1" src="../Imagenes/Procesando.gif" runat="server" />
                </td>
            </table>
        </div>
    </form>
</body>
</html>
