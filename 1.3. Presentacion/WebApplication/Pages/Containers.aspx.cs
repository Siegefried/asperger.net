﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Containers : System.Web.UI.Page
    {
        static int index;
        static List<DTContainer> ListaContenedores;
        static string TipoResiduo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                index = -1;
                divVisible.Visible = false;
            }
            MapUbicaciones.MapType = MapType.Roadmap;
            ListaContenedores = new ControllerLogIn().getContainersNoLlenos();
            MapMarcadores.Markers.Clear();
            foreach (DTContainer index in ListaContenedores)
            {
                Marker Temporal = new Marker
                {
                    Position = new LatLng(index.Latitud, index.Longitud),
                    Info = index.TipoBasura,
                    Draggable = false
                };
                Temporal.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%23" + index.Color + "&icon=fa-recycle&color=%23FFFFFF&voffset=0&hoffset=1&";
                MapMarcadores.Markers.Add(Temporal);
            }
        }

        protected void btnAlertar_Click(object sender, EventArgs e)
        {
            new ControllerLogIn().modifyContainerLleno(ListaContenedores[index].Id, ((DTLogIn)Session["LoggedUser"]).id);
            Response.Redirect(Request.RawUrl);
        }

        protected void MapMarcadores_Click(object sender, MarkerEventArgs e)
        {
            index = (int)e.Index;
            TipoResiduo = ListaContenedores[(int)e.Index].TipoBasura;
            lblTipoResiduo.Text = ListaContenedores[(int)e.Index].TipoBasura;
            divVisible.Visible = true;
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Persona.aspx");
        }
    }
}