﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_Persona_EditarUbicaciones.aspx.cs" Inherits="WebApplication.Pages.MisDatos_Persona_EditarUbicaciones" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_Persona_EditarUbicaciones.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class=" loginbox">
            <div class="Contenedor2">
                <div class="ninjadiv"></div>
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <map:GoogleMap ID="MapUbicaciones"
                                runat="server"
                                Latitude="-34.9000015" Longitude="-54.9500008"
                                Zoom="16" MapType="Roadmap"
                                autopostback="false" xmlns:asp="#unknown">
                            </map:GoogleMap>
                            <map:GoogleMarkers ID="MapMarcadores"
                                TargetControlID="MapUbicaciones"
                                runat="server" OnClick="MapMarcadores_Click">
                            </map:GoogleMarkers>
                        </div>
                        <asp:Panel runat="server" ID="PanelM">
                            <asp:Label ID="lblAlias" runat="server" Text="" Font-Size="X-Large"></asp:Label>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MapUbicaciones" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="MapMarcadores" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnRemover" runat="server" OnClick="btnRemover_Click" Text="Remover ubicacion" Enabled="False" CssClass="Botones" Visible="false" />
                    <asp:Button ID="btnRegresar" runat="server" OnClick="btnRegresar_Click" Text="Regresar al perfil" Enabled="False" CssClass="Botones" Visible="false" />
                    <asp:Button ID="btnRegresar2" runat="server" OnClick="btnRegresar2_Click" Text="Regresar al perfil" Enabled="False" CssClass="Botones" Visible="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <p>Recuerda: Para remover una ubicación primero debes seleccionar el marcador en el mapa para luego hacer click en "Remover ubicacion".</p>
            <asp:Button ID="Button1" runat="server" OnClick="Nueva_Ubicacion" Text="Nueva ubicacion" Enabled="true" CssClass="Botones" />
        </div>
    </form>
</body>
</html>
