﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisDatos_Reciclador_EditarUbicacionPrincipal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["LoggedUser"] = new ControllerLogIn().logIn("Gaby98", "123456");
            MapMarcadores.MarkerOptions.Draggable = true;
            MapUbicaciones.MapType = MapType.Roadmap;
            if (!IsPostBack)
            {
                Ubicacion Current = new ControllerLogIn().getUbicacionPrincipalRecicladores(((DTLogIn)Session["LoggedUser"]).id);
                if (Current == null)
                {
                    Session["Ubicacion"] = new LatLng((double)MapUbicaciones.Latitude, (double)MapUbicaciones.Longitude);
                }
                else
                {
                    Session["Ubicacion"] = new LatLng(Current.Latitud, Current.Longitud);
                    MapUbicaciones.Latitude = ((LatLng)Session["Ubicacion"]).Latitude;
                    MapUbicaciones.Longitude = ((LatLng)Session["Ubicacion"]).Longitude;
                }


                MapMarcadores.Add(new Marker
                {
                    Position = (LatLng)Session["Ubicacion"],
                    Info = "Nueva ubicacion :D",
                    Draggable = true
                });
            }
        }
        protected LatLng getLatLngUbicacion()
        {
            return (LatLng)Session["Ubicacion"];
        }

        protected void MapMarcadores_DragEnd(object sender, GoogleMaps.Markers.MarkerEventArgs e)
        {
            Session["Ubicacion"] = e.Position;
            lblLatitud.Text = e.Position.Latitude.ToString();
            lblLongitud.Text = e.Position.Longitude.ToString();
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            new ControllerLogIn().modifyUbicacionPrincipalRecicladores(((DTLogIn)Session["LoggedUser"]).id, getLatLngUbicacion().Latitude, getLatLngUbicacion().Longitude);
            Response.Redirect("/Pages/MisDatos_Reciclador.aspx");
        }
    }
}