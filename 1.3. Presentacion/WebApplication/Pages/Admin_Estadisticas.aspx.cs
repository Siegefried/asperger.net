﻿using DataTransferObjects.Entities;
using DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;
using System.Data;

namespace WebApplication.Pages
{
    public partial class Admin_Estadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DropDownList1.DataSource = Enum.GetValues(typeof(Epoch));
                DropDownList1.DataBind();
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            View active = MultiView1.GetActiveView();
            
            if (active == null)
            {
                return;
            }

            if (active.Equals(ContainersLlenos))
            {
                this.btnGetContainersLlenos_Click(sender, e);
            }
            if (active.Equals(RankPersonas))
            {
                this.btnRankPersonas_Click(sender, e);
            }
            if (active.Equals(RankRecicladores))
            {
                this.btnRankRecicladores_Click(sender, e);
            }
            if (active.Equals(TiposResiduosMasReciclados))
            {
                this.btnTiposResiduosMasReciclados_Click(sender, e);
            }
        }

        protected void btnGetContainersLlenos_Click(object sender, EventArgs e)
        {
            List<DTContainer> containers = new ControllerLogIn().getContainersLlenos();
            Session["ListContainer"] = containers;

            if (containers.Count > 0)
            {
                ListView1.DataSource = containers;
                ListView1.DataBind();
            }
            else
            {
                Label dynamicLabel1 = new Label();
                dynamicLabel1.Text = "Sin Containers Llenos/Con Alerta";
                ContainersLlenos.Controls.Add(dynamicLabel1);
            }
            MultiView1.SetActiveView(ContainersLlenos);
        }

        protected void lstView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "verResumen")
            {
                var commandIdContainer = e.CommandArgument;
                string commandIdContainerString = commandIdContainer.ToString();
                long commandIdContainerLong = long.Parse(commandIdContainerString);

                if(Session["ListContainer"] != null)
                {
                    List<DTContainer> containers = ((List<DTContainer>)Session["ListContainer"]);
                    DTContainer contai = containers.Where(x => x.Id == commandIdContainerLong).First();
                    Session["Container"] = contai;
                    Response.Redirect("Admin_Estadisticas_Container.aspx");
                }
                else
                {
                    Response.Write("Session[ListContainer] == null");
                }
            }
            else
            {
                Response.Write("e.CommandName != verResumen)");
            }
        }

        protected void btnRankPersonas_Click(object sender, EventArgs e)
        {
            List<DTEstadistica<DTLogIn>> rankPersonas = new ControllerLogIn().rankPersonas(this.getDaysInThePast());

            if (rankPersonas.Count > 0)
            {
                //obtengo primer item y sus valores para comparar
                List<DTEstadisticaPair> firstStats = rankPersonas.First().stats;

                DataTable dt = new DataTable();
                dt.Columns.Add("Nick", typeof(string));

                //Autogeneracion de las columnas
                foreach (DTEstadisticaPair stat in firstStats)
                {
                    string key = stat.key;
                    //dt.Columns.Add(key+"!", typeof(string), key);
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = key;
                    //dc.data
                    dc.DataType = typeof(int);
                    //dc.Expression = key;

                    //dt.Columns.Add(  key, typeof(string),  key);
                    dt.Columns.Add(dc);
                }

                //armo las rows
                foreach (DTEstadistica<DTLogIn> obj in rankPersonas)
                {
                    DataRow subDR = dt.NewRow();
                    foreach (DTEstadisticaPair stat in obj.stats)
                    {
                        subDR["Nick"] = obj.Entidad.nickname;
                        subDR[stat.key] = stat.value;
                    }
                    dt.Rows.Add(subDR);
                }

                Session["DataTable"] = dt;
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                Response.Write("EMPTY");
            }
            
            MultiView1.SetActiveView(RankPersonas);
        }

        protected void btnRankRecicladores_Click(object sender, EventArgs e)
        {
            List<DTEstadistica<DTLogIn>> rankPersonas = new ControllerLogIn().rankRecicladores(this.getDaysInThePast());

            if (rankPersonas.Count > 0)
            {
                //obtengo primer item y sus valores para comparar
                List<DTEstadisticaPair> firstStats = rankPersonas.First().stats;

                DataTable dt = new DataTable();
                dt.Columns.Add("Nick", typeof(string));

                //Autogeneracion de las columnas
                foreach (DTEstadisticaPair stat in firstStats)
                {
                    string key = stat.key;
                    //dt.Columns.Add(key+"!", typeof(string), key);
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = key;
                    //dc.data
                    dc.DataType = typeof(int);
                    //dc.Expression = key;

                    //dt.Columns.Add(  key, typeof(string),  key);
                    dt.Columns.Add(dc);
                }

                //armo las rows
                foreach (DTEstadistica<DTLogIn> obj in rankPersonas)
                {
                    DataRow subDR = dt.NewRow();
                    foreach (DTEstadisticaPair stat in obj.stats)
                    {
                        subDR["Nick"] = obj.Entidad.nickname;
                        subDR[stat.key] = stat.value;
                    }
                    dt.Rows.Add(subDR);
                }

                Session["DataTable"] = dt;
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            else
            {
                Response.Write("EMPTY");
            }

            MultiView1.SetActiveView(RankRecicladores);
        }

        protected void btnTiposResiduosMasReciclados_Click(object sender, EventArgs e)
        {
            List<DTEstadistica<DTTipoResiduo>> rankPersonas = new ControllerLogIn().tiposResiduosMasReciclados(this.getDaysInThePast());

            if (rankPersonas.Count > 0)
            {
                //obtengo primer item y sus valores para comparar
                List<DTEstadisticaPair> firstStats = rankPersonas.First().stats;

                DataTable dt = new DataTable();
                dt.Columns.Add("Nombre", typeof(string));

                //Autogeneracion de las columnas
                foreach (DTEstadisticaPair stat in firstStats)
                {
                    string key = stat.key;
                    //dt.Columns.Add(key+"!", typeof(string), key);
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = key;
                    //dc.data
                    dc.DataType = typeof(int);
                    //dc.Expression = key;

                    //dt.Columns.Add(  key, typeof(string),  key);
                    dt.Columns.Add(dc);
                }

                //armo las rows
                foreach (DTEstadistica<DTTipoResiduo> obj in rankPersonas)
                {
                    DataRow subDR = dt.NewRow();
                    foreach (DTEstadisticaPair stat in obj.stats)
                    {
                        subDR["Nombre"] = obj.Entidad.nombre;
                        subDR[stat.key] = stat.value;
                    }
                    dt.Rows.Add(subDR);
                }

                Session["DataTable"] = dt;
                GridView3.DataSource = dt;
                GridView3.DataBind();
            }
            else
            {
                Response.Write("EMPTY");
            }

            MultiView1.SetActiveView(TiposResiduosMasReciclados);
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            //GridView1.Sort(e.SortExpression, e.SortDirection);
            DataTable dt = (DataTable)Session["DataTable"];
            dt.DefaultView.Sort = "[" +e.SortExpression +"] " +this.inverseSortDirection(e.SortDirection, "GridView1");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = (DataTable)Session["DataTable"];
            //dt.DefaultView.Sort = "[" + e.SortExpression + "]" + " DESC";
            dt.DefaultView.Sort = "[" + e.SortExpression + "] " +this.inverseSortDirection(e.SortDirection, "GridView2");
            GridView2.DataSource = dt;
            GridView2.DataBind();
        }

        protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = (DataTable)Session["DataTable"];
            dt.DefaultView.Sort = "[" + e.SortExpression + "] " + this.inverseSortDirection(e.SortDirection, "GridView3");
            GridView3.DataSource = dt;
            GridView3.DataBind();
        }

        private int getDaysInThePast()
        {
            int days = 0;
            int epochAmount = 1;

            Epoch epoch;
            Enum.TryParse<Epoch>(DropDownList1.SelectedValue.ToString(), out epoch);

            switch (epoch)
            {
                case Epoch.Año:
                    days = epochAmount * 365;
                    break;
                case Epoch.Trimestre:
                    days = epochAmount * ((28 + 2) * 3); // keep relation to enum! refact!
                    break;
                case Epoch.Mes:
                    days = epochAmount * (28 + 2);
                    break;
                case Epoch.Semana:
                    days = epochAmount * 7;
                    break;
                case Epoch.Dia:
                    days = epochAmount * 1; // :D
                    break;
            }
            return days;
        }
        
        private string inverseSortDirection(SortDirection dire, string grid)
        {
            if(Session["SortDirection" + grid] == null)
            {
                Session["SortDirection" + grid] = "ASC";
            }

            if (Session["SortDirection"+ grid].Equals("ASC"))
            {
                Session["SortDirection" + grid] = "DESC";
            }
            else
            {
                Session["SortDirection" + grid] = "ASC";
            }
            return Session["SortDirection" + grid].ToString();
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Admin.aspx");
        }



    }
}