﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Containers.aspx.cs" Inherits="WebApplication.Pages.Containers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../Estilos/Containers.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="ContenedorPrincipal">
            <label id="Titulo1" class="Titulo">CONTAINERS</label>
            <div class="Contenedor2">
                <div class="ninjadiv">
                </div>
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <map:GoogleMap ID="MapUbicaciones"
                            runat="server"
                            Latitude="-34.9000015" Longitude="-54.9500008"
                            Zoom="16" MapType="Roadmap">
                        </map:GoogleMap>
                        <map:GoogleMarkers ID="MapMarcadores"
                            TargetControlID="MapUbicaciones"
                            runat="server" OnClick="MapMarcadores_Click">
                        </map:GoogleMarkers>
                        <div runat="server" id="divVisible">
                            <asp:Label ID="Label1" runat="server" Text="Tipo residuo:"></asp:Label>
                            <asp:Label ID="lblTipoResiduo" runat="server" Text=""></asp:Label>
                            <br />
                            <asp:Button ID="btnAlertar" runat="server" Text="Alertar container lleno" OnClick="btnAlertar_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" CssClass="Atras" OnClick="Atras" />
        </div>
    </form>
</body>
</html>
