﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisDatos_Persona_EditarUbicaciones_NuevaUbicacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MapMarcadores.MarkerOptions.Draggable = true;
            MapUbicaciones.MapType = MapType.Roadmap;
            if (!IsPostBack)
            {
                Session["Ubicacion"] = new LatLng((double)MapUbicaciones.Latitude, (double)MapUbicaciones.Longitude);
                MapMarcadores.Add(new Marker
                {
                    Position = getLatLngUbicacion(),
                    Info = "Nueva ubicacion :D",
                    Draggable = true
                });
            }

            int VariableDeReciclar = 0;
            if (new ControllerLogIn().isReciclador(((DTLogIn)Session["LoggedUser"]).nickname))
            {

            }
            else
            {
                VariableDeReciclar = (int)Session["Reciclar"];
                if (VariableDeReciclar == 1)
                {
                    Button2.Visible = true;
                }
                else
                {
                    Button1.Visible = true;
                }
            }
            
        }

        protected void MapMarcadores_DragEnd(object sender, GoogleMaps.Markers.MarkerEventArgs e)
        {
            Session["Ubicacion"] = e.Position;
            lblLatitud.Text = e.Position.Latitude.ToString();
            lblLongitud.Text = e.Position.Longitude.ToString();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if(new ControllerLogIn().isReciclador(((DTLogIn)Session["LoggedUser"]).nickname))
            {
                string Alias = txtAlias.Text;
                new ControllerLogIn().AddUbicacionReciclador(((DTLogIn)Session["LoggedUser"]).id, Alias, getLatLngUbicacion().Latitude, getLatLngUbicacion().Longitude);
                Response.Redirect("MisDatos_Reciclador.aspx");
            }
            else
            {
                string Alias = txtAlias.Text;
                new ControllerLogIn().AddUbicacionPersona(((DTLogIn)Session["LoggedUser"]).id, Alias, getLatLngUbicacion().Latitude, getLatLngUbicacion().Longitude);
                int VariableDeReciclar = 0;
                VariableDeReciclar = (int)Session["Reciclar"];
                if (VariableDeReciclar == 1)
                {
                    Session["Reciclar"] = 2;
                    Response.Redirect("Reciclar.aspx");
                }
                else
                {
                    Response.Redirect("MisDatos_Persona.aspx");
                }
            }
        }

        protected void Regresar_Perfil(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_Persona.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session["Reciclar"] = 2;
            Response.Redirect("Reciclar.aspx");
        }
        protected LatLng getLatLngUbicacion()
        {
            return (LatLng)Session["Ubicacion"];
        }
    }


}