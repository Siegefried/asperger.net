﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="WebApplication.Pages.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../Estilos/Admin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="Contenedor">
        <div class="secciones">
            <a href="Usuarios_Admin.aspx" title="Usuarios"><img src="../imagenes/misrecolectores.png" class="icono"></a>
            <a href="Usuarios_Admin.aspx" title="Usuarios"><label class="reciclar">Usuarios</label></a>
        </div>
        <div class="secciones">
            <a href="Admin_Contenedores.aspx" title="Contenedores"><img src="../imagenes/contenedores.png" class="icono"></a>
            <a href="Admin_Contenedores.aspx" title="Contenedores"><label class="reciclar">Contenedores</label></a>
        </div>
        <div class="secciones">
            <a href="Admin_Estadisticas.aspx" title="Estadistica"><img src="../imagenes/estadistica.png" class="icono"></a>
            <a href="Admin_Estadisticas.aspx" title="Estadistica"><label class="reciclar">Estadistica</label></a>
        </div>
        <div class="secciones">
            <a href="TipoResiduo_Admin.aspx" title="Tipo Residuo"><img src="../imagenes/tipos-desechos.png" class="icono"></a>
            <a href="TipoResiduo_Admin.aspx" title="Tipo Residuo"><label class="reciclar">Tipo Residuo</label></a>
        </div>
        <div class="secciones">
            <a href="Logout.aspx" title="Salir"><img src="../imagenes/salir.png" class="icono"></a>
            <a href="Logout.aspx" title="Salir"><label class="reciclar">Salir</label></a>
        </div>
    </div>
</body>
</html>