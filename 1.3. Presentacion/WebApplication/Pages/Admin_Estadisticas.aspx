<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_Estadisticas.aspx.cs" Inherits="WebApplication.Pages.Admin_Estadisticas" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Estilos/Admin_Estadisticas.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="Contenedor">
        <label id="Titulo">Estadisticas</label>
        <asp:Table ID="Table1" runat="server">
            <asp:TableRow >
                <asp:TableCell>
                    <div class="ContFiltro">
                    <label id="lblfiltro">Filtro:</label>
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </asp:TableCell>
                <asp:TableCell>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow >
                <asp:TableCell>
                    <div class="ContBotones">
                    <asp:Button CssClass="Botones" ID="btnGetContainersLlenos" runat="server" Text="Motrar Containers Llenos" OnClick="btnGetContainersLlenos_Click" />
                    <br />
                    <asp:Button CssClass="Botones" ID="btnRankPersonas" runat="server" Text="Ranking Personas" OnClick="btnRankPersonas_Click" />
                    <br />
                    <asp:Button CssClass="Botones" ID="btnRankRecicladores" runat="server" Text="Ranking Recolector" OnClick="btnRankRecicladores_Click" />
                    <br />
                    <asp:Button CssClass="Botones" ID="btnTiposResiduosMasReciclados" runat="server" Text="Ranking Tipos de Residuos" OnClick="btnTiposResiduosMasReciclados_Click" />
                    <br />
                    </div>

                </asp:TableCell>
                <asp:TableCell>
                    <div class="ContElResto">
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="ContainersLlenos" runat="server">
                            <asp:Label ID="Label1" runat="server" Text="Containers con Alerta"></asp:Label>


                            <asp:ListView ID="ListView1" runat="server" OnItemCommand="lstView_ItemCommand">
                                <LayoutTemplate>
                                    <div class="ScrollBar2">
                                        <table id="Tabla_Datos_Generales" border="1">
                                            <tr>
                                                <th>ID</th>
                                                <th>Tipo Basura</th>
                                                <th>Latitud</th>
                                                <th>Longitud</th>
                                            </tr>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                        </table>
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td> <%# Eval("Id") %> </td>
                                        <td> <%# Eval("TipoBasura") %> </td>
                                        <td> <%# Eval("Latitud") %> </td>
                                        <td> <%# Eval("Longitud") %> </td>
                                        <td>
                                            <asp:Button ID="btnResumen" runat="server" Text="Ver En Mapa" CommandName="verResumen" CommandArgument='<%#Eval("Id")%>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" Font-Size="20px" runat="server" Text="No hay Datos"></asp:Label>
                                </EmptyDataTemplate>
                            </asp:ListView>



                            
                        </asp:View>
                        <asp:View ID="RankPersonas" runat="server">
                            <asp:Label ID="Label2" runat="server" Text="Rank de Personas"></asp:Label>

                            <asp:GridView ID="GridView1" AllowSorting="True" runat="server" OnSorting="GridView1_Sorting">
                            </asp:GridView>
                            
                        </asp:View>
                        <asp:View ID="RankRecicladores" runat="server">
                            <asp:Label ID="Label3" runat="server" Text="Rank de Recicladores"></asp:Label>
                            <asp:GridView ID="GridView2" AllowSorting="True" runat="server" OnSorting="GridView2_Sorting">
                            </asp:GridView>
                        </asp:View>
                        <asp:View ID="TiposResiduosMasReciclados" runat="server">
                            <asp:Label ID="Label4" runat="server" Text="Residuos Mas Reciclados"></asp:Label>
                            <asp:GridView ID="GridView3" AllowSorting="True" runat="server" OnSorting="GridView3_Sorting">
                            </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                  </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="btnAtras_Click" />
       </div>
    </form>
</body>
</html>
