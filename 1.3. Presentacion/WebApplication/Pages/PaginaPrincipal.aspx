﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaPrincipal.aspx.cs" Inherits="WebApplication.Pages.PaginaPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../Estilos/PaginaPrincipal.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h1>Reciclatorvm</h1>
        <a id="boton1" class="boton" href="NuevoLogin.aspx">Iniciar Sesion</a>
        <a id="boton2" class="boton" href="NuevoRegistro.aspx">Registrar</a>
        <p id="etiqueta1">¿Aún no tienes cuenta? Regístrate </p>
    </header>
    <div id="contenedor1">
        <span><img src="../imagenes/fondoprincipal3.png" id="imagen1" class="imagen1"></span>
        <span><p id="titulo1">Reciclar es más que una acción, es el valor de la responsabilidad por preservar los recursos
            naturales</p></span>
    </div>
    <footer>
    </footer>
</body>
</html>
