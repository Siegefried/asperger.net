﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Usuarios_Admin.aspx.cs" Inherits="WebApplication.Pages.Usuarios_Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/Usuarios_Admin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form" runat="server">
        <div class="VentanaPrincipal">
            <label id="Titulo1" class="Titulo">USUARIOS</label>
            <div class="Filtro">
                <div class="contenedor">
                    <asp:Label CssClass="labelFiltro" ID="Label1" runat="server" Text="Nombre:"></asp:Label>
                    <asp:TextBox CssClass="txtFiltro" ID="TextBox1" runat="server"></asp:TextBox>
                    <br />
                </div>
                <div class="contenedor">
                    <asp:Label CssClass="labelFiltro" ID="Label2" runat="server" Text="Apellido:"></asp:Label>
                    <asp:TextBox CssClass="txtFiltro" ID="TextBox2" runat="server"></asp:TextBox>
                    <br />
                </div>
                <div class="contenedor">
                    <asp:Label CssClass="labelFiltro" ID="Label3" runat="server" Text="Edad:"></asp:Label>
                    <asp:TextBox CssClass="txtFiltro" ID="TextBox3" runat="server"></asp:TextBox>
                    <br />
                </div>
                <div class="contenedor">
                    <asp:Label CssClass="labelFiltro" ID="Label4" runat="server" Text="Email:"></asp:Label>
                    <asp:TextBox CssClass="txtFiltro" ID="TextBox4" runat="server"></asp:TextBox>
                    <br />
                </div>
                <div class="contenedor">
                    <asp:Label CssClass="labelFiltro" ID="Label5" runat="server" Text="NickName:"></asp:Label>
                    <asp:TextBox CssClass="txtFiltro" ID="TextBox5" runat="server"></asp:TextBox>
                    <br />
                </div>
            </div>
            <div class="contenedorBloquear">
                <p>
                    Para Bloquear/Desbloquear un usuario debe ingresar su ID.
                </p>
                <asp:TextBox ID="TextBox6" CssClass="txtBloquear" runat="server"></asp:TextBox>
                <asp:Button ID="Button2" ValidationGroup="1" runat="server" Text="Bloquear" OnClick="Button2_Click" />
                <asp:Button ID="Button3" ValidationGroup="1" runat="server" Text="Desbloquear" OnClick="Button3_Click" />
                <br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Solo se aceptan numeros." ValidationExpression="^\d+$" Display="Dynamic"  ControlToValidate="TextBox6" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ValidationGroup="1" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo requerido" ControlToValidate="TextBox6" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" CssClass="Validador"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvIdUsuario" runat="server" ErrorMessage="El id de usuario ingresado no existe" ControlToValidate="TextBox6" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
            </div>
            <asp:Button ID="Button1" runat="server" Text="Aplicar Filtro" OnClick="Button1_Click" />
            <asp:Button ID="Button4" runat="server" Text="Limpiar Filtro" OnClick="Button4_Click" />
            <br />
            <div class="ScrollBar">
                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
            </div>
            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" CssClass="Atras" OnClick="Atras"/>
        </div>
    </form>
</body>
</html>
