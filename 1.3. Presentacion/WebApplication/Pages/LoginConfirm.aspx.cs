﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class LoginConfirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedUser"] == null)
            {
                Response.Redirect("/Pages/LogIn.aspx");
            }
            else
            {
                NombreUser.Text = "Bienvenido/a " + ((DTLogIn)Session["LoggedUser"]).nombre + " " + ((DTLogIn)Session["LoggedUser"]).apellido;
                if (new ControllerLogIn().isPersona(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Response.Redirect("/Pages/Persona.aspx");
                }
                else if (new ControllerLogIn().isReciclador(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Response.Redirect("/Pages/Reciclador.aspx");
                }
                else if (new ControllerLogIn().isAdmin(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Response.Redirect("/Pages/Admin.aspx");
                }

            }
        }
    }
}