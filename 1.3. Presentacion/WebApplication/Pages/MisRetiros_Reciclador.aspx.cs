using DataTransferObjects.Entities;
using DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisRetiros_Reciclador : System.Web.UI.Page
    {
        
        //static List<DTPedido> listPedidos = null;
        //static DTPedido seleted = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["LoggedUser"] == null){
            //    //Response.Write("Usuario no Logueado");
            //    Session["LoggedUser"] = new DTLogIn() {id = 2};
            //    //return;
            //    //Response.Redirect("/Pages/NuevoLogin.aspx");
            //}

            if (!Page.IsPostBack)
            {
                DropDownList1.DataSource = Enum.GetValues(typeof(Epoch));
                DropDownList1.DataBind();

                //ddlbValoraciones.DataSource = Enum.GetValues(typeof(Valoracion));
                //ddlbValoraciones.DataBind();

                ddlbValoraciones.Items.Add(GetDescription(Valoracion.porValorar, "porValorar"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.pesimo, "pesimo"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.mediocre, "mediocre"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.normal, "normal"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.buena, "buena"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.excelente, "excelente"));
                

                Session["listDTPedidos"] = new ControllerLogIn().getPedidosReciclador(((DTLogIn)Session["LoggedUser"]).id);
                Session["listDTPedidosFilteAndSor"] = filteAndSor((List<DTPedido>)Session["listDTPedidos"], Epoch.Dia, 1);
                ListView1.DataSource = filteAndSor((List<DTPedido>)Session["listDTPedidos"], Epoch.Dia, 1);
                ListView1.DataBind();

                MultiView.SetActiveView(ViewGrilla);
            }
                
        }

        protected void lstView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "verResumen")
            {
                List<DTPedido> dummyList = new List<DTPedido>();
                

                var commandIdPedido = e.CommandArgument;
                string commandIdPedidoString = commandIdPedido.ToString();
                long commandIdPedidoLong = long.Parse(commandIdPedidoString);

                Session["selectedDTPedidos"] = ((List<DTPedido>)Session["listDTPedidos"]).FirstOrDefault(p => p.Id == commandIdPedidoLong);

                if (Session["selectedDTPedidos"] != null){
                    dummyList.Add((DTPedido)Session["selectedDTPedidos"]);
                }else{
                    Response.Write("ERROR al cargar pedido en la Vista de Resumen");
                }

                DetailsView1.DataSource = dummyList;
                DetailsView1.DataBind();

                txtObservaciones2.Value = ((DTPedido)Session["selectedDTPedidos"]).Observaciones;
                ddlbValoraciones.SelectedIndex = (int)((DTPedido)Session["selectedDTPedidos"]).Valoracion;

                DataTable table = new DataTable("Tabla1");
                List<DTMaterialCantidad> ListaResiduos;
                ListaResiduos = new ControllerLogIn().TraerInfoMaterialCantidad(commandIdPedidoLong);
                table.Columns.Add(new DataColumn("Tipo de Residuo", typeof(string)));
                table.Columns.Add(new DataColumn("Cantidad", typeof(int)));
                foreach (DTMaterialCantidad Index in ListaResiduos)
                {
                    DataRow row = table.NewRow();
                    row["Tipo de Residuo"] = Index.Nombre;
                    row["Cantidad"] = Index.Cantidad;
                    table.Rows.Add(row);
                }
                GridView1.DataSource = table;
                GridView1.DataBind();

                MultiView.SetActiveView(ViewResumen);
                //this.estadisticas(((List<DTPedido>)Session["listDTPedidosFilteAndSor"]));
            }
        }

        private List<DTPedido> filteAndSor(List<DTPedido> oriList, Epoch epoch, int epochAmount)
        {
            int days = 0;
            DateTime moreThan;
            List<DTPedido> clonList = oriList.ConvertAll(p => p);

            switch (epoch)
            {
                case Epoch.Año:
                    days = epochAmount * 365;
                    break;
                case Epoch.Trimestre:
                    days = epochAmount * ((28 + 2) * 3); // keep relation to enum! refact!
                    break;
                case Epoch.Mes:
                    days = epochAmount * (28 + 2);
                    break;
                case Epoch.Semana:
                    days = epochAmount * 7;
                    break;
                case Epoch.Dia:
                    days = epochAmount * 1; // :D
                    break;
            }
            moreThan = DateTime.Today.AddDays(days * -1);

            clonList = clonList.Where(x => x.Fecha >= moreThan).OrderByDescending(x => x.Fecha).ToList();
            return clonList;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Epoch status;
            Enum.TryParse<Epoch>(DropDownList1.SelectedValue.ToString(), out status);

            ListView1.Dispose();
            Session["listDTPedidosFilteAndSor"] = filteAndSor((List<DTPedido>)Session["listDTPedidos"], status, 1);
            ListView1.DataSource = ((List<DTPedido>)Session["listDTPedidosFilteAndSor"]);
            ListView1.DataBind();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MultiView.SetActiveView(ViewGrilla);
        }

        private void estadisticas(List<DTPedido> pedidos)
        {
            List<string> tiposBasura = new ControllerLogIn().TraeTiposBasuraString();
            Dictionary<string, int> stats = new Dictionary<string, int>();

            //Response.Write("<hr/>");

            //inicializa
            foreach (string tipoBasura in tiposBasura)
            {
                stats.Add(tipoBasura, 0);

                foreach (DTPedido pedido in pedidos)
                {
                    int innnerSum = pedido.Materiales.Where(x => x.Nombre == tipoBasura).Sum(y => y.Cantidad);
                    stats[tipoBasura] = stats[tipoBasura] + innnerSum;
                }

                //Response.Write("{Residuo = '" + tipoBasura + "', Total = " + stats[tipoBasura] + "}");
            }
            //Response.Write("<hr/>");
            DataTable table = new DataTable("Tabla1");
            table.Columns.Add(new DataColumn("Tipo de Residuo", typeof(string)));
            table.Columns.Add(new DataColumn("Cantidad", typeof(int)));
            foreach (KeyValuePair<string, int> Index in stats)
            {
                DataRow row = table.NewRow();
                row["Tipo de Residuo"] = Index.Key;
                row["Cantidad"] = Index.Value;
                table.Rows.Add(row);
                //Response.Write("{Residuo = '" + Index.Key + "', Total = " + Index.Value + "}");
                //Response.Write("<br/>");
            }
            GridView2.DataSource = table;
            GridView2.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            MultiView.SetActiveView(ViewEstadisticas);
            this.estadisticas(((List<DTPedido>)Session["listDTPedidosFilteAndSor"]));
        }

        public static string GetDescription(object enumValue, string defDesc)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            MultiView.SetActiveView(ViewGrilla);
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Reciclador.aspx");
        }
    }// public partial class MisRetiros : System.Web.UI.Page END
}// namespace WebApplication.Pages END