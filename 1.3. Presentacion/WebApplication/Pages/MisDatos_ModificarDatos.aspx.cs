﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisDatos_ModificarDatos : System.Web.UI.Page
    {
        int VariableDeControlDeView = 0; //Si la variable esta en 0, cambia de vista normalmente
                                         //Si esta 1 es que se genero un error y se debe corregir un campo en el formulario
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MultiView1.ActiveViewIndex = 0;
            }

            if (new ControllerLogIn().isPersona(((DTLogIn)Session["LoggedUser"]).nickname))
            {
                TituloPrincipal1.InnerText = "PERSONA";
                TituloPrincipal2.InnerText = "PERSONA";
            }
            else
            {
                TituloPrincipal1.InnerText = "RECICLADOR";
                TituloPrincipal2.InnerText = "RECICLADOR";
            }
            //Inicio - quitar esto en la version final, solo lo use para testear mas rapido
            //string nick = "Manuelete";
            //string pass = "123";

            //DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);

            //if (logIn != null)
            //{
            //    Session["LoggedUser"] = logIn;
            //}
            //Fin

            //Cargo Imagen de Perfil - Inicio
            long IdInt = ((DTLogIn)Session["LoggedUser"]).id;
            byte[] bytes = new ControllerLogIn().getUserImage(IdInt);
            string strBase64 = Convert.ToBase64String(bytes);
            Image1.ImageUrl = "data:Image/png;base64," + strBase64;
            //Fin
    }

    protected void txtContinuar_Click(object sender, EventArgs e)
        {
            //cambiar vista
            if(VariableDeControlDeView == 0)
            {
                if(BtnSubirImg.PostedFile.ContentLength != 0)
                {
                    Byte[] InfoImagen = null;
                    HttpPostedFile archivo = BtnSubirImg.PostedFile;
                    System.Drawing.Image Imagen = System.Drawing.Image.FromStream(archivo.InputStream);
                    if (Imagen.Width > 200 && Imagen.Height > 200)
                    {
                        InfoImagen = ResizeBitmap((Bitmap)Imagen);
                    }
                    else
                    {
                        ImageConverter converter = new ImageConverter();
                        InfoImagen = (byte[])converter.ConvertTo(Imagen, typeof(byte[]));
                    }
                    Session["Imagen"] = InfoImagen;
                }
                

                //HttpPostedFile archivo = BtnSubirImg.PostedFile;
                //Stream stream = archivo.InputStream;
                //BinaryReader binaryReader = new BinaryReader(stream);
                //Byte[] InfoImagen = binaryReader.ReadBytes((int)stream.Length);
                //Session["Imagen"] = InfoImagen;

                //HttpPostedFile archivo = ImagenUsuario.PostedFile;
                //System.Drawing.Image Imagen = System.Drawing.Image.FromStream(archivo.InputStream);
                //if (Imagen.Width > 200 && Imagen.Height > 200)
                //{
                //    InfoImagen = ResizeBitmap((Bitmap)Imagen);
                //}
                //else
                //{
                //    ImageConverter converter = new ImageConverter();
                //    InfoImagen = (byte[])converter.ConvertTo(Imagen, typeof(byte[])); ;
                //}

                //long imgint = BitConverter.ToInt64(InfoImagen, 0);
                //txtNinja.Text = Convert.ToString(imgint);

                //long nuevo = 0;
                //Int64.TryParse(txtNinja.Text, out nuevo);
                //byte[] bytes = BitConverter.GetBytes(nuevo);

                //Array.Reverse(bytes);
                //byte[] result = bytes;

                //txtNinja.Text = System.Text.Encoding.UTF8.GetString(InfoImagen);

                MultiView1.ActiveViewIndex = 1;
            }
            
        }

        protected void txtConfirmar_Click(object sender, EventArgs e)
        {
            DTLogIn UsuarioConfirmar = new DTLogIn();
            UsuarioConfirmar.id = ((DTLogIn)Session["LoggedUser"]).id;
            if (txtNombre.Text == "")
            {
                UsuarioConfirmar.nombre = null;
            }
            else
            {
                UsuarioConfirmar.nombre = txtNombre.Text;
            }

            if (txtApellido.Text == "")
            {
                UsuarioConfirmar.apellido = null;
            }
            else
            {
                UsuarioConfirmar.apellido = txtApellido.Text;
            }

            if (txtEdad.Text == "")
            {
                UsuarioConfirmar.edad = -1;
            }
            else
            {
                UsuarioConfirmar.edad = int.Parse(txtEdad.Text);
            }

            if (txtEmail.Text == "")
            {
                UsuarioConfirmar.email = null;
            }
            else
            {
                UsuarioConfirmar.email = txtEmail.Text;
            }

            if (txtConfContrasenia.Text == "")
            {
                UsuarioConfirmar.password = null;
            }
            else
            {
                UsuarioConfirmar.password = txtConfContrasenia.Text;
            }

            byte[] ImagenConfirmar = (byte[])Session["Imagen"];
            
            if (VariableDeControlDeView == 0)
            {
                new ControllerLogIn().CambiarDatosUsuario(UsuarioConfirmar.id, UsuarioConfirmar.nombre, UsuarioConfirmar.apellido, null, UsuarioConfirmar.password, UsuarioConfirmar.edad, UsuarioConfirmar.email, ImagenConfirmar);
                //new ControllerLogIn().CambiarFotoPerfilPersona(UsuarioConfirmar.imagen, UsuarioConfirmar.id);

                if (UsuarioConfirmar.password == null)
                {
                    Session["LoggedUser"] = new ControllerLogIn().logIn(((DTLogIn)Session["LoggedUser"]).nickname, ((DTLogIn)Session["LoggedUser"]).password);
                }
                else
                {
                    Session["LoggedUser"] = new ControllerLogIn().logIn(((DTLogIn)Session["LoggedUser"]).nickname, UsuarioConfirmar.password);
                }

                if (new ControllerLogIn().isPersona(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Response.Redirect("MisDatos_Persona.aspx");
                }
                else if (new ControllerLogIn().isReciclador(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Response.Redirect("MisDatos_Reciclador.aspx");
                }
            }
        }
        //Inicio - Valido el tipo de imagen
        //protected void cvImagen_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    var NombreArchivo = args.Value;
        //    args.IsValid =
        //    ((NombreArchivo.ToLower().EndsWith(".jpg")) ||
        //    (NombreArchivo.ToLower().EndsWith(".jpeg")) ||
        //    (NombreArchivo.ToLower().EndsWith(".gif")) ||
        //    (NombreArchivo.ToLower().EndsWith(".png")));
        //}
        //Fin

        //Inicio - Cambio foto
        //protected void BtnCambiarFotoConfirmar(object sender, EventArgs e)
        //{
        //    int IdInt = unchecked((int)((DTLogIn)Session["LoggedUser"]).id);
        //    HttpPostedFile archivo = BtnSubirImg.PostedFile;
        //    Stream stream = archivo.InputStream;
        //    BinaryReader binaryReader = new BinaryReader(stream);
        //    Byte[] InfoImagen = binaryReader.ReadBytes((int)stream.Length);
        //    new ControllerLogIn().CambiarFotoPerfilPersona(InfoImagen, IdInt);
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}
        //Fin

        protected void vtMailUso_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string Email = args.Value.Trim();
            if (Email != "" && new ControllerLogIn().checkMail(Email) == false)
            {
                args.IsValid = true;
                VariableDeControlDeView = 0;
            }
            else
            {
                args.IsValid = false;
                VariableDeControlDeView = 1;
            }
            
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (new ControllerLogIn().logIn(txtConfirmNick.Text.Trim(), txtConfirmPass.Text.Trim()) == null)
            {
                args.IsValid = false;
                VariableDeControlDeView = 1;
            }
            else
            {
                args.IsValid = true;
                VariableDeControlDeView = 0;
            }
        }

        //Inicio
        private byte[] ResizeBitmap(Bitmap b)
        {
            int nWidth = 0, nHeight = 0;
            if (b.Width > b.Height)
            {
                nWidth = 130;
                nHeight = b.Height * 130 / b.Width;
            }
            else if (b.Width < b.Height)
            {
                nHeight = 130;
                nWidth = b.Width * 130 / b.Height;
            }
            else
            {
                nHeight = 130;
                nWidth = 130;
            }
            Bitmap result = new Bitmap(nWidth, nHeight);

            using (Graphics g = Graphics.FromImage((System.Drawing.Image)result))
            {
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            }

            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(result, typeof(byte[]));
        }
        //Fin
    }
}