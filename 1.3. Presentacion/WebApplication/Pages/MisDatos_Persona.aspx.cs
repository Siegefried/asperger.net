﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using DataTransferObjects.Entities;
using WebApplication.Controllers;
using GoogleMaps.Markers;
using GoogleMaps;
using System.IO;

namespace WebApplication.Pages
{
    public partial class MisDatos_Persona : System.Web.UI.Page
    {
        private List<Ubicacion> Ubicaciones;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicio - quitar esto en la version final, solo lo use para testear mas rapido
            //string nick = "CamilaG";
            //string pass = "123456";

            //DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);

            //if (logIn != null)
            //{
            //    Session["LoggedUser"] = logIn;

            //}
            //Fin

            MapUbicaciones.MapType = MapType.Roadmap;
            Ubicaciones = new ControllerLogIn().getUbicacionesPersona(((DTLogIn)Session["LoggedUser"]).id);
            foreach (Ubicacion Index in Ubicaciones)
            {
                MapMarcadores.Add(new Marker
                {
                    Position = new LatLng(Index.Latitud, Index.Longitud),
                    Info = "Alias: " + Index.Alias
                });
            }

            //Cargo Imagen de Perfil - Inicio
            long IdInt = ((DTLogIn)Session["LoggedUser"]).id;
            byte[] bytes = new ControllerLogIn().getUserImage(IdInt);
            byte[] newarraybytes = new byte[0];
            if(bytes.Length != newarraybytes.Length)
            {
                string strBase64 = Convert.ToBase64String(bytes);
                Image1.ImageUrl = "data:Image/png;base64," + strBase64;
            }
            
            //Fin


            //Cargo Datos del Perfil - Inicio
            //USUARIO persona = new ControllerLogIn().GetUsuarioById(4);
            //nombre.Text = persona.Nombre;
            //apellido.Text = persona.Apellido;
            //DTLogIn UsuarioModificado = new DTLogIn();
            //if (txtnickname.Text == "")
            //{
            //    UsuarioModificado.nickname = null;
            //}
            //else
            //{
            //    UsuarioModificado.nickname = txtnickname.Text;
            //}

            txtapellido.Text = ((DTLogIn)Session["LoggedUser"]).apellido;
            txtedad.Text = ((DTLogIn)Session["LoggedUser"]).edad.ToString();
            txtemail.Text = ((DTLogIn)Session["LoggedUser"]).email;
            txtnickname.Text = ((DTLogIn)Session["LoggedUser"]).nickname;
            txtnombre.Text = ((DTLogIn)Session["LoggedUser"]).nombre;
            //Fin

            //Yo siendo alto mono - Inicio
            //string cs = "Data Source=MSI\\SQLEXPRESS;Initial Catalog=Reciclatorvm;Integrated Security=True";
            //using (SqlConnection con = new SqlConnection(cs))
            //{
            //    SqlCommand cmd = new SqlCommand("TraerImagenPorId", con);
            //    cmd.CommandType = CommandType.StoredProcedure;

            //    SqlParameter paramId = new SqlParameter()
            //    {
            //        ParameterName = "@IdImg",
            //        Value = ((DTLogIn)Session["LoggedUser"]).id
            //    };

            //    cmd.Parameters.Add(paramId);

            //    con.Open();
            //    byte[] bytes= (byte[])cmd.ExecuteScalar();

            //    string strBase64 = Convert.ToBase64String(bytes);


            //    Image1.ImageUrl = "data:Image/png;base64," + strBase64;

            //    con.Close();

            //    SqlCommand cmd2 = new SqlCommand("Select * From USUARIOS where Id = @IdUser", con);
            //    cmd2.Parameters.AddWithValue("@IdUser", ((DTLogIn)Session["LoggedUser"]).id);
            //    con.Open();
            //    SqlDataReader registro = cmd2.ExecuteReader();
            //    if(registro.Read())
            //    {
            //        nombre.Text = registro["Nombre"].ToString();
            //        apellido.Text = registro["Apellido"].ToString();
            //        email.Text = registro["Mail"].ToString();
            //        edad.Text = registro["Edad"].ToString();
            //        contrasenia.Text = registro["Password"].ToString();
            //        nickname.Text = registro["Nickname"].ToString();
            //    }
            //    con.Close();
            //}
            //Fin
        }
        protected void cvImagen_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var NombreArchivo = args.Value;
            args.IsValid =
            ((NombreArchivo.ToLower().EndsWith(".jpg")) ||
            (NombreArchivo.ToLower().EndsWith(".jpeg")) ||
            (NombreArchivo.ToLower().EndsWith(".gif")) ||
            (NombreArchivo.ToLower().EndsWith(".png")));
        }

        //protected void BtnCambiarFotoConfirmar(object sender, EventArgs e)
        //{
        //    HttpPostedFile archivo = BtnSubirImg.PostedFile;
        //    Stream stream = archivo.InputStream;
        //    BinaryReader binaryReader = new BinaryReader(stream);
        //    Byte[] InfoImagen = binaryReader.ReadBytes((int)stream.Length);
        //    new ControllerLogIn().CambiarFotoPerfilPersona(InfoImagen, ((DTLogIn)Session["LoggedUser"]).id);
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarNombreConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarApellidoConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarEdadConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarEmailConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarNicknameConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        //protected void BtnCambiarContraseniaConfirmar(object sender, EventArgs e)
        //{
        //    Response.Redirect("MisDatos_Persona.aspx");
        //}

        protected void BtnModificarDatos(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_ModificarDatos.aspx");
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Persona.aspx");
        }

    }
}