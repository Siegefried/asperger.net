﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_Persona_MisRecolectores.aspx.cs" Inherits="WebApplication.Pages.MisDatos_Persona_MisRecolectores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_Persona_MisRecolectores.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="VentanaPrincipal">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:Label ID="Label1" runat="server" Text="Mis Recolectores" CssClass="Titulo"></asp:Label>
            <div class="ScrollBar">
            <ul class="listing">
                <asp:ListView ID="lvRecicladores" runat="server" OnSelectedIndexChanged="lvRecicladores_SelectedIndexChanged" OnSelectedIndexChanging="lvRecicladores_SelectedIndexChanging">
                    <ItemTemplate>
                        <asp:LinkButton ID="SelectButton" runat="server" CommandName="Select" CssClass="MyLabel">
                            <li>
                                <img class="Imgli" src='<%# string.Format("data:image/gif;base64,{0}", Convert.ToBase64String((byte[])Eval("ImagenReciclador")))%>' />
                                <div class="lblli"> Nombre:<%#Eval("NombreReciclador")%></div>
                                <div class="lblli">Cantidad de materiales reciclados: <%#Eval("CantidadReciclado")%></div>
                                <asp:Label CssClass="lblli" ID="lblBloqueo" runat="server" Text="¡Bloqueado!" ForeColor="Red" Visible='<%#Eval("Bloqueado")%>'></asp:Label>
                            </li>
                        </asp:LinkButton>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" CssClass="lblVacio" runat="server" Text="No hay recolectores relacionados contigo."></asp:Label>
                    </EmptyDataTemplate>
                </asp:ListView>
            </ul>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="lblDescripcion" runat="server" Text="" Visible="false"></asp:Label>
                <br />
                <asp:Button ID="btnBloquear" CssClass="BtnBloq-Desb" runat="server" Text="Bloquear" Visible="false" OnClick="btnBloquear_Click" />
                <asp:Button ID="btnDesbloquear" CssClass="BtnBloq-Desb" runat="server" Text="Desbloquear" Visible="false" OnClick="btnDesbloquear_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" CssClass="Atras" OnClick="Atras" />
            </div>
    </form>
</body>
</html>
