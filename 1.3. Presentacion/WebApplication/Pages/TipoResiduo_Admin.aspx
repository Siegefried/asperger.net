﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TipoResiduo_Admin.aspx.cs" Inherits="WebApplication.Pages.TipoResiduo_Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script type="text/javascript" src="../scripts/jquery.js"></script>
    <script type="text/javascript" src="../scripts/farbtastic.js"></script>
    <link href="../Estilos/TipoResiduo_Admin.css" rel="stylesheet" type="text/css" />
    <link href="../Estilos/farbtastic.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#picker1').farbtastic('#color1');
  });
 </script>
    <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#picker2').farbtastic('#color2');
  });
 </script>
</head>
<body>
    <form id="form" runat="server">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <div class="VentanaPrincipal">
                    <label id="Titulo1" class="Titulo">Tipo Residuo</label>
                    <div class="ScrollBar">
                        <asp:GridView ID="GridView1" runat="server" OnRowDataBound="MergeGrid_RowDataBound"></asp:GridView>
                    </div>
                    <div class="contenedorBloquear">
                        <p>
                            Para bloquear un tipo de residuo debe ingresar su ID.
                        </p>
                        <asp:TextBox ID="TextBox6" CssClass="txtBloquear" runat="server"></asp:TextBox>
                        <asp:Button ID="Button2" runat="server" Text="Bloquear" OnClick="Button2_Click" ValidationGroup="1" />
                        <br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Solo se aceptan numeros." ValidationExpression="^\d+$" Display="Dynamic"  ControlToValidate="TextBox6" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="cvIdTipoResiduo1" runat="server" ErrorMessage="El id de residuo ingresado no existe" ControlToValidate="TextBox6" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ValidationGroup="1" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo requerido" ControlToValidate="TextBox6" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                    </div>
                    <div class="contenedorBloquear">
                        <p>
                            Para desbloquear un tipo de residuo debe ingresar su ID.
                        </p>
                        <asp:TextBox ID="TextBox4" CssClass="txtBloquear" runat="server"></asp:TextBox>
                        <asp:Button ValidationGroup="2" ID="Button6" runat="server" Text="Desbloquear" OnClick="Button6_Click" />
                        <br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Solo se aceptan numeros." ValidationExpression="^\d+$" Display="Dynamic"  ControlToValidate="TextBox4" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="cvIdTipoResiduo2" runat="server" ErrorMessage="El id de residuo ingresado no existe" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" OnServerValidate="CustomValidator2_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ValidationGroup="2" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo requerido" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                    </div>
                    <div class="contenedorBloquear">
                        <p>
                            Para editar un tipo de residuo debe ingresar su ID.
                        </p>
                        <asp:TextBox ID="TextBox1" CssClass="txtBloquear" runat="server"></asp:TextBox>
                        <asp:Button ValidationGroup="3" ID="Button4" runat="server" Text="Editar" OnClick="Button4_Click" />
                        <br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Solo se aceptan numeros." ValidationExpression="^\d+$" Display="Dynamic"  ControlToValidate="TextBox1" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="cvIdTipoResiduo3" runat="server" ErrorMessage="El id de residuo ingresado no existe" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" OnServerValidate="CustomValidator3_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ValidationGroup="3" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo requerido" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                    </div>
                    <asp:Button ID="Button1" runat="server" Text="Agregar tipo de residuo" OnClick="Button1_Click" />
                    <br />
                    <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="Atras" CssClass="Atras" />
                </div>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <div class="VentanaPrincipal2">
                    <label id="Titulo2" class="Titulo2">Tipo Residuo</label>
                    <div class="Filtro2">
                        <div class="contenedor">
                            <asp:Label CssClass="labelFiltro" ID="Label1" runat="server" Text="Nombre:"></asp:Label>
                            <asp:TextBox CssClass="txtFiltro" ID="TextBox2" runat="server"></asp:TextBox>
                            <br />
                        </div>
                        <div class="ContenedorDeSelectorDeColores">
                            
                        <div class="form-item"><label class="labelFiltro" for="color">Color:</label><input type="text" class="txtFiltro" id="color1" name="color"  runat="server" value="#123456" /></div><div id="picker1"></div>
                            
                         </div>
                    </div>
                    <asp:Label ID="Label3" CssClass="lblControlador" runat="server" Text="El nombre de residuo ingresado ya existe" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator ValidationGroup="4" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Debe ingresar un nombre" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" CssClass="ValidacionVacio" Font-Size="Smaller"></asp:RequiredFieldValidator>
                    <asp:Button ValidationGroup="4" ID="Button3" runat="server" Text="Agregar" OnClick="Button3_Click" />
                    <br />
                    <asp:Button ID="btnAtras2" runat="server" Text="Atras" OnClick="View_Anterior" CssClass="AtrasView" />
                </div>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <div class="VentanaPrincipal3">
                    <label id="Titulo3" class="Titulo2">Tipo Residuo</label>
                    <div class="Filtro3">

                        <div class="contenedor">
                            <asp:Label CssClass="labelFiltro" ID="Label2" runat="server" Text="Nombre:"></asp:Label>
                            <asp:TextBox CssClass="txtFiltro" ID="TextBox3" runat="server"></asp:TextBox>
                            <br />
                        </div>

                        <div class="ContenedorDeSelectorDeColores">
                            
                        <div class="form-item"><label class="labelFiltro" for="color">Color:</label><input type="text" class="txtFiltro" id="color2" name="color" value="#123456" runat="server"/></div><div id="picker2"></div>
                            
                         </div>
                    </div>
                    <asp:Button ID="Button5" runat="server" Text="Confirmar Cambios" OnClick="Button5_Click" />
                    <br />
                    <asp:Button ID="btnAtras3" runat="server" Text="Atras" OnClick="View_Anterior" CssClass="AtrasView" />
                </div>
            </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>
