﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Reciclar : System.Web.UI.Page
    {
        private List<string> ListaTiposBasura;
        private List<TextBox> ListaTextbox;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MultiView1.ActiveViewIndex = 0;
                Calendar1.Visible = false;
                List<Ubicacion> Ubicaciones;
                Ubicaciones = new ControllerLogIn().getUbicacionesPersona(((DTLogIn)Session["LoggedUser"]).id);
                foreach (Ubicacion Index in Ubicaciones)
                {
                    DropDownList1.Items.Add(Index.Alias);
                }
            }
            int VariableDeReciclar = 0;
            VariableDeReciclar = (int)Session["Reciclar"]; //Si esto te da null exception es que no pasaron por Persona.aspx que es donde asigo Session["Reciclar"]=0; Dejo esto acá por las dudas... luego borrar ! 
            if (VariableDeReciclar == 2)
            {
                MultiView1.ActiveViewIndex = 0;
                Session["Reciclar"] = 0;
            }

            ListaTextbox = new List<TextBox>();
            ListaTiposBasura = new ControllerLogIn().TraeTiposBasuraString();
            foreach (string Index in ListaTiposBasura)
            {
                Label TemporalLabel = new Label();
                TemporalLabel.CssClass = "TableCell";
                TemporalLabel.Text = Index + ": ";
                TextBox TemporalTextBox = new TextBox();
                TemporalTextBox.Text = "0";
                ListaTextbox.Add(TemporalTextBox);
                TemporalTextBox.CssClass = "TableCell2";
                TemporalTextBox.Attributes.Add("PlaceHolder", "Introduce la cantidad a reciclar en Kg");
                Panel PanelTemporal = new Panel();
                PanelTemporal.CssClass = "TableRow";
                PanelTemporal.Controls.Add(TemporalLabel);
                PanelTemporal.Controls.Add(TemporalTextBox);
                PanelPrincipal.Controls.Add(PanelTemporal);
            }

        }
        protected void SiguienteView1_Click(object sender, EventArgs e)
        {
            //Inicio Controlo Cantidades de Materiales de Reciclaje
            int x = 0;
            int y = 0;
            int numero = 0;
            foreach (string Index in ListaTiposBasura)
            {
                while (x < ListaTiposBasura.Count)
                {
                    while (y < ListaTextbox.Count)
                    {
                        if (!int.TryParse(ListaTextbox.ElementAt(y).Text, out numero))
                        {
                            labelninja.Visible = true;
                            MultiView1.ActiveViewIndex = 0;
                            break;
                        }
                        else
                        {
                            labelninja.Visible = false;
                            MultiView1.ActiveViewIndex = 1;
                        }
                        y++;
                        break;
                    }
                    x++;
                    break;
                }
            }
        }

        protected void SiguienteView2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
            int x = 0;
            int y = 0;
            foreach (string Index in ListaTiposBasura)
            {
                Label TemporalLabel = new Label();
                Panel PanelTemporal = new Panel();
                PanelTemporal.CssClass = "TableRow2";
                while (x < ListaTiposBasura.Count)
                {
                    while (y < ListaTextbox.Count)
                    {
                        TemporalLabel.Text = ListaTiposBasura.ElementAt(x) + ": " + ListaTextbox.ElementAt(y).Text;
                        PanelTemporal.Controls.Add(TemporalLabel);
                        y++;
                        break;
                    }
                    x++;
                    break;
                }
                PanelPrincipal2.Controls.Add(PanelTemporal);
            }
            Label1.Text = DropDownList1.SelectedItem.Text;
            Label3.Text = "Horario: " + rbHorario.SelectedItem.Value;
            Label5.Text = "Fecha: " + txtFecha.Text;
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            txtFecha.Text = Calendar1.SelectedDate.ToShortDateString();
            Calendar1.Visible = false;
        }

        protected void Calendario_Click(object sender, ImageClickEventArgs e)
        {
            if (Calendar1.Visible)
            {
                Calendar1.Visible = false;
            }
            else
            {
                Calendar1.Visible = true;
            }
        }

        protected void btnAdministrarUbicacion_Click(object sender, EventArgs e)
        {
            Session["Reciclar"] = 1;
            Response.Redirect("MisDatos_Persona_EditarUbicaciones_NuevaUbicacion.aspx");
        }

        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date < DateTime.Now.Date)
            {
                e.Day.IsSelectable = false;
                e.Cell.ForeColor = System.Drawing.Color.Red;
                e.Cell.Font.Strikeout = true;
            }

        }

        protected void Aceptar_Click(object sender, EventArgs e)
        {
            //Inicio
            //Fecha
            CultureInfo MyCultureInfo = new CultureInfo("es-ES");
            DateTime FechaPedido = DateTime.Parse(txtFecha.Text, MyCultureInfo);

            //Hora -------- Horarios: 10 - 12 Horas    ó    12 - 16 Horas
            bool HoraPedido;
            if (rbHorario.SelectedIndex == 0)
            {
                HoraPedido = true;
            }
            else
            {
                HoraPedido = false;
            }
            //IdPersona
            long idPersona = ((DTLogIn)Session["LoggedUser"]).id;
            //IdUbicacion
            List<Ubicacion> Ubicaciones;
            long idUbicacion = 0;
            Ubicaciones = new ControllerLogIn().getUbicacionesPersona(((DTLogIn)Session["LoggedUser"]).id);
            foreach (Ubicacion Index in Ubicaciones)
            {
                if (Index.Alias == DropDownList1.SelectedItem.Text)
                {
                    idUbicacion = Index.Id;
                    break;
                }
            }
            //Observaciones
            string Observaciones = TextArea1.Value;
            List<DTMaterialCantidad> Residuos = new List<DTMaterialCantidad>();
            for (int i = 0; i< ListaTextbox.Count; i++) {
                Residuos.Add(new DTMaterialCantidad() {
                    Cantidad = int.Parse(ListaTextbox[i].Text),
                    Nombre = ListaTiposBasura[i]
                });
            }
            //Inserto
            new ControllerLogIn().AgregarPedido(FechaPedido, HoraPedido, idPersona, idUbicacion, Observaciones, Residuos);
            Response.Redirect("Persona.aspx");
            //Fin
        }

        protected void Atras_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }

        protected void Cancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Persona.aspx");
        }

        protected void Atras2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void MenuPrincipal(object sender, EventArgs e)
        {
            Response.Redirect("Persona.aspx");
        }
    }
}