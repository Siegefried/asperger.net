using DataTransferObjects.Entities;
using DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisRetiros_Persona : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ////Inicio - quitar esto en la version final, solo lo use para testear mas rapido
            //string nick = "CamilaG";
            //string pass = "123456";

            //DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);
            //Session["LoggedUser"] = new DTLogIn() { id = 13 };

            //if (logIn != null)
            //{
            //    //Session["LoggedUser"] = logIn;
            //    //Session["LoggedUser"] = new DTLogIn() { id = 12 };

            //}
            ////Fin
            //if (Session["LoggedUser"] == null)
            //{
            //    Response.Write("Usuario no Logueado");
            //    //Session["LoggedUser"] = new DTLogIn() {id = 3};
            //    return;
            //}

            if (!Page.IsPostBack)
            {
                DropDownList1.DataSource = Enum.GetValues(typeof(Epoch));
                DropDownList1.DataBind();

                //ddlbValoraciones.DataSource = Enum.GetValues(typeof(Valoracion));
                //ddlbValoraciones.DataBind();

                ddlbValoraciones.Items.Add(GetDescription(Valoracion.porValorar, "porValorar"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.pesimo, "pesimo"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.mediocre, "mediocre"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.normal, "normal"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.buena, "buena"));
                ddlbValoraciones.Items.Add(GetDescription(Valoracion.excelente, "excelente"));


                Session["listDTPedidos"] = new ControllerLogIn().getPedidosPersona(((DTLogIn)Session["LoggedUser"]).id);
                ListView1.DataSource = filteAndSor((List<DTPedido>)Session["listDTPedidos"], Epoch.Dia, 1);
                ListView1.DataBind();

                MultiView.SetActiveView(ViewGrilla);
            }

        }

        protected void lstView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "verResumen")
            {
                List<DTPedido> dummyList = new List<DTPedido>();
                var commandIdPedido = e.CommandArgument;
                string commandIdPedidoString = commandIdPedido.ToString();
                long commandIdPedidoLong = long.Parse(commandIdPedidoString);

                Session["selectedDTPedidos"] = ((List<DTPedido>)Session["listDTPedidos"]).FirstOrDefault(p => p.Id == commandIdPedidoLong);

                if (((DTPedido)Session["selectedDTPedidos"]) != null)
                {
                    dummyList.Add(((DTPedido)Session["selectedDTPedidos"]));
                }
                else
                {
                    Response.Write("ERROR al cargar pedido en la Vista de Resumen");
                }

                DetailsView1.DataSource = dummyList;
                DetailsView1.DataBind();

                txtObservaciones.Value = ((DTPedido)Session["selectedDTPedidos"]).Observaciones;
                ddlbValoraciones.SelectedIndex = (int)((DTPedido)Session["selectedDTPedidos"]).Valoracion;

                DataTable table = new DataTable("Tabla1");
                List<DTMaterialCantidad> ListaResiduos;
                ListaResiduos = new ControllerLogIn().TraerInfoMaterialCantidad(commandIdPedidoLong);
                table.Columns.Add(new DataColumn("Tipo de Residuo", typeof(string)));
                table.Columns.Add(new DataColumn("Cantidad", typeof(int)));
                foreach (DTMaterialCantidad Index in ListaResiduos)
                {
                    DataRow row = table.NewRow();
                    row["Tipo de Residuo"] = Index.Nombre;
                    row["Cantidad"] = Index.Cantidad;
                    table.Rows.Add(row);
                }

                if (((DTPedido)Session["selectedDTPedidos"]).Estado.Id == 2)
                {
                    if (((DTPedido)Session["selectedDTPedidos"]).Reciclador != null)
                    {
                        phValoracion.Visible = true;
                        if (((DTPedido)Session["selectedDTPedidos"]).Valoracion != 0)
                        {
                            ddlbValoraciones.Enabled = false;
                            btnGuardar.Visible = false;
                        }
                        else
                        {
                            ddlbValoraciones.Enabled = true;
                            btnGuardar.Visible = true;
                        }
                    }
                }
                else
                {
                    phValoracion.Visible = false;
                }

                GridView1.DataSource = table;
                GridView1.DataBind();


                MultiView.SetActiveView(ViewResumen);
            }
        }

        private List<DTPedido> filteAndSor(List<DTPedido> oriList, Epoch epoch, int epochAmount)
        {
            int days = 0;
            DateTime moreThan;
            List<DTPedido> clonList = oriList.ConvertAll(p => p);

            switch (epoch)
            {
                case Epoch.A�o:
                    days = epochAmount * 365;
                    break;
                case Epoch.Trimestre:
                    days = epochAmount * ((28 + 2) * 3); // keep relation to enum! refact!
                    break;
                case Epoch.Mes:
                    days = epochAmount * (28 + 2);
                    break;
                case Epoch.Semana:
                    days = epochAmount * 7;
                    break;
                case Epoch.Dia:
                    days = epochAmount * 1; // :D
                    break;
            }
            moreThan = DateTime.Today.AddDays(days * -1);

            clonList = clonList.Where(x => x.Fecha >= moreThan).OrderByDescending(x => x.Fecha).ToList();
            return clonList;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Epoch status;
            Enum.TryParse<Epoch>(DropDownList1.SelectedValue.ToString(), out status);

            ListView1.Dispose();
            ListView1.DataSource = filteAndSor((List<DTPedido>)Session["listDTPedidos"], status, 1);
            ListView1.DataBind();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MultiView.SetActiveView(ViewGrilla);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Valoracion valorat = Valoracion.porValorar;
            if (ddlbValoraciones.SelectedValue.ToString() == "Sin Valoraci�n")
            {
                valorat = Valoracion.porValorar;
            }
            else if (ddlbValoraciones.SelectedValue.ToString() == "Pesimo")
            {
                valorat = Valoracion.pesimo;
            }
            else if (ddlbValoraciones.SelectedValue.ToString() == "Mediocre")
            {
                valorat = Valoracion.mediocre;
            }
            else if (ddlbValoraciones.SelectedValue.ToString() == "Normal")
            {
                valorat = Valoracion.normal;
            }
            else if (ddlbValoraciones.SelectedValue.ToString() == "Bueno")
            {
                valorat = Valoracion.buena;
            }
            else if (ddlbValoraciones.SelectedValue.ToString() == "Excelente")
            {
                valorat = Valoracion.excelente;
            }
            //Enum.TryParse<Valoracion>(ddlbValoraciones.SelectedValue.ToString(), out valorat);
            String observaciones = txtObservaciones.Value.Trim();

            if (((DTPedido)Session["selectedDTPedidos"]) != null)
            {
                ((DTPedido)Session["selectedDTPedidos"]).Valoracion = valorat;
                ((DTPedido)Session["selectedDTPedidos"]).Observaciones = observaciones;
                new ControllerLogIn().updatePedidosResumen(((DTPedido)Session["selectedDTPedidos"]));
            }

            //lblMessage.Text = "Pedido Actualizado";
            MultiView.SetActiveView(ViewGrilla);
        }

        //protected void btnAceptar_Click(object sender, EventArgs e)
        //{
        //    MultiView.SetActiveView(ViewGrilla);
        //    lblMessage.Text = "";
        //}

        public static string GetDescription(object enumValue, string defDesc)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Persona.aspx");
        }

    }// public partial class MisRetiros : System.Web.UI.Page END
}// namespace WebApplication.Pages END