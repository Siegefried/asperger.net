﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoLogin.aspx.cs" Inherits="WebApplication.Pages.NuevoLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/NuevoLogin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="loginbox">
        <form runat="server" method="POST">
            <p>Usuario</p>
            <asp:TextBox ID="usuario" runat="server"></asp:TextBox>
            <div class="validacion">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo requerido" ControlToValidate="usuario" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator></div>
            <p>Contraseña</p>
            <asp:TextBox ID="contrasenia" runat="server" TextMode="Password"></asp:TextBox>
            <div class="validacion">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo requerido" ControlToValidate="contrasenia" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvLogin" runat="server" ErrorMessage="Usuario y/o contraseña no validos" ControlToValidate="contrasenia" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
                <asp:Label ID="Label1" ForeColor="Red" runat="server" Visible="false" Text="Este usuario se encuentra bloqueado"></asp:Label>
            </div>
            <asp:Button class="BotonIniciarSesion" ID="Loggear" runat="server" Text="Iniciar sesion" OnClick="Loggear_Click" />
            <hr>
            <asp:Button ID="btnGoogle" CssClass="btnGoogle" runat="server" Text="Iniciar sesion con Google" data-provider="google" OnClick="RedirectToLogin_Click" CausesValidation="false" />
            <hr>
            <asp:Button ID="btnAtras" CssClass="btnGoogle" runat="server" Text="Regresar al menu principal" CausesValidation="false" OnClick="Atras" />
        </form>
    </div>
</body>
</html>
