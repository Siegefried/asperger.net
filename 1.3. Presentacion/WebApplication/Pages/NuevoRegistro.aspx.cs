﻿using DataTransferObjects.Entities;
using Nemiro.OAuth;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class NuevoRegistro : System.Web.UI.Page
    {
        private AuthorizationResult authResult = null;
        private Byte[] profileImage = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            usuario.Attributes.Add("PlaceHolder", "Introduce tu usuario");
            contrasenia.Attributes.Add("PlaceHolder", "Introduce tu contraseña");
            confirmarcontrasenia.Attributes.Add("PlaceHolder", "Introduce de nuevo tu contraseña");
            nombre.Attributes.Add("PlaceHolder", "Introduce tu nombre");
            apellido.Attributes.Add("PlaceHolder", "Introduce tu apellido");
            email.Attributes.Add("PlaceHolder", "Introduce tu email");
            edad.Attributes.Add("PlaceHolder", "Introduce tu edad");

            email.Enabled = true;

            if (Session["extAuthResult"] != null)
            {
                authResult = (AuthorizationResult)Session["extAuthResult"];
                calledFromExternalLoginResult();
            }

        }

        protected void Registrar_Click(object sender, EventArgs e)
        {
            #region ImagenCare
            Byte[] InfoImagen = null;
            if (ImagenUsuario.PostedFile.ContentLength != 0)
            {
                ///Inicio
                HttpPostedFile archivo = ImagenUsuario.PostedFile;
                System.Drawing.Image Imagen = System.Drawing.Image.FromStream(archivo.InputStream);
                if (Imagen.Width > 200 && Imagen.Height > 200)
                {
                    InfoImagen = ResizeBitmap((Bitmap)Imagen);
                }
                else
                {
                    ImageConverter converter = new ImageConverter();
                    InfoImagen = (byte[])converter.ConvertTo(Imagen, typeof(byte[]));
                }
                ///Fin
            }
            else
            {
                profileImage = new byte[0];
                InfoImagen = profileImage;
            }
            #endregion
            string Nombre = nombre.Text.Trim();
            string Apellido = apellido.Text.Trim();
            string Mail = email.Text.Trim();
            string User = usuario.Text.Trim();
            string Password = contrasenia.Text.Trim();
            int Edad = int.Parse(edad.Text.Trim());

            if (verify() == true)
            {
                if (rbTipoUsuario.SelectedValue == "Recolector")
                {
                    new ControllerLogIn().RegistrarReciclador(Nombre, Apellido, User, Password, Edad, Mail, InfoImagen);
                }
                else if (rbTipoUsuario.SelectedValue == "Persona")
                {
                    new ControllerLogIn().RegistrarPersona(Nombre, Apellido, User, Password, Edad, Mail, InfoImagen);
                }

                //auto log in
                string nick = User;
                string pass = Password;

                DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);

                if (logIn != null)
                {
                    Session["LoggedUser"] = logIn;
                    Response.Redirect("/Pages/LoginConfirm.aspx");
                }

                //Response.Redirect("NuevoLogin.aspx");
            }
        }

        protected bool verify()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                return true;
            }
            return false;
        }

        protected void vtMailUso_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string Email = args.Value.Trim();
            if (Email != "" && new ControllerLogIn().checkMail(Email) == false)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void cvUser_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string User = args.Value.Trim();
            if (User != "" && new ControllerLogIn().checkUser(User) == false)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void cvImagen_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Si no hay imagen de afuera valido el fileupload, sino no
            if (profileImage == null)
            {
                var NombreArchivo = args.Value;
                args.IsValid = (
                    (NombreArchivo.ToLower().EndsWith(".jpg")) ||
                    (NombreArchivo.ToLower().EndsWith(".jpeg")) ||
                    (NombreArchivo.ToLower().EndsWith(".gif")) ||
                    (NombreArchivo.ToLower().EndsWith(".png"))
                );
            }
            else
            {
                args.IsValid = true;
            }

        }

        protected void calledFromExternalLoginResult()
        {
            AuthorizationResult result = (AuthorizationResult)Session["extAuthResult"];
            UserInfo userInf = result.UserInfo;

            string externalNick = (string)Session["extAuthNick"];
            if (externalNick == "")
            {
                externalNick = usuario.Text;
            }
            int externalAge = (int)Session["extAuthAge"];

            nombre.Text = userInf.FirstName;
            apellido.Text = userInf.LastName;
            usuario.Text = externalNick;
            email.Text = userInf.Email;
            email.ReadOnly = true;
            btnGoogle.Visible = false;
            edad.Text = externalAge.ToString();

            if (userInf.Userpic.Length > 5)
            {
                using (var webClient = new WebClient())
                {
                    //ToDo: check if download is ok
                    //System.Drawing.Image imagen = System.Drawing.Image.FromFile("../Imagenes/misdatos.png", true);
                    //profileImage = ResizeBitmap((Bitmap)imagen);

                    profileImage = funcionpapa(webClient.DownloadData(userInf.Userpic));
                    ImagenUsuario.Enabled = false;
                    //RequiredFieldValidator9.Enabled = false;
                }
            }
        }// protected void calledFromExternalLoginResult() END

        protected void RedirectToLogin_Click(object sender, EventArgs e)
        {
            string provider = ((Button)sender).Attributes["data-provider"];
            // build the return address
            string returnUrl = new Uri(Request.Url, "ExternalLoginResult.aspx").AbsoluteUri;
            // redirect user into external site for authorization
            OAuthWeb.RedirectToAuthorization(provider, returnUrl);
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/PaginaPrincipal.aspx");
        }
        //Inicio
        private byte[] ResizeBitmap(Bitmap b)
        {
            int nWidth = 0, nHeight = 0;
            if (b.Width > b.Height)
            {
                nWidth = 130;
                nHeight = b.Height * 130 / b.Width;
            }
            else if (b.Width < b.Height)
            {
                nHeight = 130;
                nWidth = b.Width * 130 / b.Height;
            }
            else
            {
                nHeight = 130;
                nWidth = 130;
            }
            Bitmap result = new Bitmap(nWidth, nHeight);

            using (Graphics g = Graphics.FromImage((System.Drawing.Image)result))
            {
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            }

            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(result, typeof(byte[]));
        }
        //Fin

        private byte[] funcionpapa(byte[] papa)
        {
            System.IO.MemoryStream myMemStream = new System.IO.MemoryStream(papa);
            System.Drawing.Image fullsizeImage = System.Drawing.Image.FromStream(myMemStream);
            System.Drawing.Image newImage = fullsizeImage.GetThumbnailImage(130, 130, null, IntPtr.Zero);
            System.IO.MemoryStream myResult = new System.IO.MemoryStream();
            newImage.Save(myResult, System.Drawing.Imaging.ImageFormat.Gif);  //Or whatever format you want.
            return myResult.ToArray();
        }
    }
}