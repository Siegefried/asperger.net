<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisRetiros_Persona.aspx.cs" Inherits="WebApplication.Pages.MisRetiros_Persona" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Estilos/MisRetiros_Persona.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:MultiView ID="MultiView" runat="server">
                <asp:View ID="ViewGrilla" runat="server">
                    <div class="VentanaPrincipal">
                        <label id="Titulo1" class="Titulo">Retiros </label>
                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:ListView ID="ListView1" runat="server" OnItemCommand="lstView_ItemCommand">
                            <LayoutTemplate>
                                <div class="ScrollBar">
                                    <table id="Tabla_Datos_Generales" border="1">
                                        <tr>
                                            <th>Fecha  </th>
                                            <th>Hora   </th>
                                            <th>Direccion  </th>
                                            <th>Estado</th>
                                            <th>Reciclador</th>
                                            <th>Resumen</th>
                                        </tr>
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </table>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Fecha", "{0:d}") %> </td>
                                    <td><%# ((bool)Eval("Hora")) ? "10 - 12" : "12 - 16" %></td>
                                    <td><%# Eval("Ubicacion") %> </td>
                                    <td><%# Eval("Estado") %> </td>
                                    <td><%# Eval("Reciclador") %> </td>
                                    <td>
                                        <asp:Button ID="btnResumen" runat="server" Text="Resumen" CommandName="verResumen" CommandArgument='<%#Eval("Id")%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <asp:Label ID="Label1" Font-Size="20px" runat="server" Text="No hay Datos"></asp:Label>
                            </EmptyDataTemplate>
                        </asp:ListView>
                        <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="Atras" CssClass="Atras" />
                    </div>
                </asp:View>
                <asp:View ID="ViewResumen" runat="server">
                    <div class="VentanaPrincipal">
                        <label id="Titulo3" class="SubTitulo1">Datos Materiales para Reciclar </label>
                        <div class="ScrollBar2">
                            <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                        </div>
                        <label id="Titulo4" class="SubTitulo2">Datos Pedido </label>
                        <asp:DetailsView ID="DetailsView1" AllowPaging="false" AutoGenerateRows="false" CssClass="Tabla_Resumen" runat="server">
                            <Fields>
                                <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" HeaderStyle-CssClass="Titulo_Tabla_Resumen" ReadOnly="true" />
                                <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="Titulo_Tabla_Resumen">
                                    <ItemTemplate>
                                        <span><%# Eval("Fecha", "{0:d}") %>  </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hora" HeaderStyle-CssClass="Titulo_Tabla_Resumen">
                                    <ItemTemplate>
                                        <span><%# ((bool)Eval("Hora")) ? "10 - 12" : "12 - 16" %> </span>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--<asp:BoundField DataField="Observaciones" HeaderText="Observaciones" SortExpression="Observaciones" ReadOnly="false" />--%>
                            </Fields>
                        </asp:DetailsView>

                        <asp:Button ID="btnCancelar" runat="server" Text="Atras" OnClick="btnCancelar_Click" />

                        <asp:Label ID="lblObservaciones" runat="server" Text="Observaciones"></asp:Label>
                        <br />
                        <%--<asp:TextBox ID="txtObservaciones" runat="server"></asp:TextBox>--%>
                        <textarea id="txtObservaciones" runat="server"></textarea>
                        <asp:PlaceHolder ID="phValoracion" runat="server" Visible="false">
                            <asp:Label ID="lblValoraciones" runat="server" Text="Valoracion"></asp:Label>
                            <asp:DropDownList ID="ddlbValoraciones" runat="server"></asp:DropDownList>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
                        </asp:PlaceHolder>
                    </div>
                </asp:View>
                <%--<asp:View ID="ViewResumenMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click" />
                </asp:View>--%>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
