﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_ModificarDatos.aspx.cs" Inherits="WebApplication.Pages.MisDatos_ModificarDatos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_ModificarDatos.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function showimagepreview(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    document.getElementsByTagName("img")[0].setAttribute("src", e.target.result);
                    //Si cambias el 0 por 1 agarra el siguiente asp:Image que tengas en el body
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <%--<asp:TextBox ID="txtNinja" runat="server" Visible="false"></asp:TextBox>--%>
                    <div class="ContenedorPrincipal">
                        <div style="margin-bottom: 20px">
                            <label runat="server" id="TituloPrincipal1" class="titulo"></label>
                        </div>
                        <asp:Image ID="Image1" runat="server" Width="300px" Height="300px" />
                        <br />
                        <asp:FileUpload ID="BtnSubirImg" runat="server" onchange="showimagepreview(this)" />
                        <br />
                        <asp:RegularExpressionValidator ID="ValidarImagen" runat="server" ErrorMessage="Formato de imagen no valido. (.JPG, .JPEG, .BMP y .PNG)" Display="Dynamic" ControlToValidate="BtnSubirImg" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.bmp|.BMP|.png|.PNG|.jpeg|.JPEG)$"></asp:RegularExpressionValidator>
                        <br />
                        <div class="separador">
                            <label>Nombre:</label>
                            <br>
                            <asp:TextBox ID="txtNombre" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                            <br>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Solo se aceptan letras." ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic" CssClass="ValidarCampo" ControlToValidate="txtNombre"></asp:RegularExpressionValidator>
                        </div>
                        <br>
                        <div class="separador">
                            <label>Apellido:</label>
                            <br>
                            <asp:TextBox ID="txtApellido" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                            <br>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Solo se aceptan letras." ValidationExpression="^[a-zA-Z ]*$" Display="Dynamic" CssClass="ValidarCampo" ControlToValidate="txtApellido"></asp:RegularExpressionValidator>
                        </div>
                        <br>
                        <div class="separador">
                            <label>Edad:</label>
                            <br>
                            <asp:TextBox ID="txtEdad" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                            <br>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Solo se aceptan numeros. Edades validas: 5 años - 120 años" ValidationExpression="^[5-9]$|^[1-9][0-9]$|^[1][0-1][0-9]$|^120$" Display="Dynamic" CssClass="ValidarCampo" ControlToValidate="txtEdad"></asp:RegularExpressionValidator>
                            <%--Rango de Edad 5 - 120--%>
                        </div>
                        <br>
                        <div class="separador">
                            <label>Email:</label>
                            <br>
                            <asp:TextBox ID="txtEmail" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                            <br />
                            <asp:RegularExpressionValidator CssClass="ValidarCampo" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Formato de mail no valido." ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <asp:CustomValidator CssClass="ValidarCampo" ID="vtMailUso"
                                runat="server"
                                ControlToValidate="txtEmail"
                                ErrorMessage="Ya esta en uso este mail."
                                Display="Dynamic"
                                OnServerValidate="vtMailUso_ServerValidate"></asp:CustomValidator>
                        </div>
                        <div class="separador">
                            <label>Contraseña:</label>
                            <br>
                            <asp:TextBox ID="txtContrasenia" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                        </div>
                        <br>
                        <div class="separador">
                            <label>Confirmar Contraseña:</label>
                            <br>
                            <asp:TextBox ID="txtConfContrasenia" CssClass="Campo" runat="server" ReadOnly="false" ></asp:TextBox>
                            <br />
                            <asp:CompareValidator CssClass="ValidarCampo" ID="CompareValidator1" runat="server" ControlToCompare="txtContrasenia" ControlToValidate="txtConfContrasenia" ErrorMessage="Este campo debe ser igual a la contraseña." Display="Dynamic"></asp:CompareValidator>
                        </div>
                        <br>
                        <div style="position: initial; margin-top: 20px; margin-bottom: 20px; margin-left: 15%;">
                            <asp:Button ID="Button1" runat="server" OnClick="txtContinuar_Click" Text="Continuar" CssClass="Campo2" />
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <br />
                    <div class="ContenedorPrincipal2">
                        <div style="margin-bottom: 20px">
                            <label runat="server" id="TituloPrincipal2" class="titulo"></label>
                        </div>
                        <div class="separador">
                            <label style="font-size: 18px;">Para confirmar cambios, ingrese sus datos de ingreso.</label>
                            <br>
                            <label style="font-size: 18px;">En caso de haber cambiado la contraseña, debe ingresar la contraseña con la que inicio sesión.</label>
                            <br />
                        </div>
                        <br>
                        <div class="separador">
                            <label>NickName:</label>
                            <br>
                            <asp:TextBox ID="txtConfirmNick" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                        </div>
                        <br>
                        <div class="separador">
                            <label>Contraseña:</label>
                            <br>
                            <asp:TextBox ID="txtConfirmPass" CssClass="Campo" runat="server" ReadOnly="false" TextMode="Password"></asp:TextBox>
                        </div>
                        <br>
                        <asp:CustomValidator ID="cvLogin" runat="server" ErrorMessage="Usuario y/o contraseña no validos" ControlToValidate="txtConfirmPass" Display="Dynamic" CssClass="ValidarCampo" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
                        <div style="position: initial; margin-top: 20px; margin-bottom: 20px; margin-left: 15%;">
                            <asp:Button ID="txtConfirmar" runat="server" OnClick="txtConfirmar_Click" Text="Concluir cambios" CssClass="Campo2" />
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
