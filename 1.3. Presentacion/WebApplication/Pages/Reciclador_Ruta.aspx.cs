﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Directions;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Reciclador_Ruta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["LoggedUser"] = new ControllerLogIn().logIn("Gaby98", "123456");
            if (new ControllerLogIn().getUbicacionPrincipalRecicladores(((DTLogIn)Session["LoggedUser"]).id) == null)
            {
                Response.Redirect("MisDatos_Reciclador_EditarUbicacionPrincipal.aspx");
            }
            MapUbicaciones.MapType = MapType.Roadmap;
            MapMarcadores.MarkerOptions.Draggable = false;
            if (!IsPostBack)
            {
                Session["Horario"] = false;
                Session["currentIndex"] = -1;
                Session["Fecha"] = new DateTime();
                //UpdateMap();
            }
        }

        protected void ddlHorario_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Horario"] = ddlHorario.SelectedIndex;
            UpdateMap();
        }

        protected void calFechas_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date < DateTime.Now.Date)
            {
                e.Day.IsSelectable = false;
                e.Cell.ForeColor = System.Drawing.Color.Red;
                e.Cell.Font.Strikeout = true;
            }
        }

        protected void calFechas_SelectionChanged(object sender, EventArgs e)
        {
            Session["Fecha"] = calFechas.SelectedDate;
            UpdateMap();
        }

        protected void MapMarcadores_Click(object sender, MarkerEventArgs e)
        {
            UpdateMap();
            divDatos.Visible = true;
            Session["currentIndex"] = (int)e.Index;
            lblPropietario.Text = "Propietario: " + ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Persona;
            foreach (DTMaterialCantidad Index in ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Materiales)
            {
                Label TemporalLabel = new Label();
                TemporalLabel.Text = Index.Nombre + ": " + Index.Cantidad;
                pnpMateriales.Controls.Add(TemporalLabel);
            }
            lblObservaciones.Text = "Observaciones: " + ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Observaciones;
            if (((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Estado.Id == 1)
            {
                btnRecolectado.Visible = true;
            }
            else
            {
                btnRecolectado.Visible = false;
            }
            MapUbicaciones.Latitude = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Ubicacion.Latitud;
            MapUbicaciones.Longitude = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Ubicacion.Longitud;
            MapUbicaciones.Zoom = 16;
        }

        protected void UpdateMap()
        {
            bool Horario = !Convert.ToBoolean(Session["Horario"]);
            long IdR = ((DTLogIn)Session["LoggedUser"]).id;
            DateTime Fecha = ((DateTime)Session["Fecha"]);

            MapMarcadores.Markers.Clear();
            MapUbicaciones.Directions.Clear();
            Session["ubicaciones"] = new ControllerLogIn().TraerPedidosReciclador(IdR, Horario, Fecha);
            if (((List<DTPedido>)Session["ubicaciones"]).Count > 0)
            {
                List<Ubicacion> Ubicaciones = new List<Ubicacion>();

                foreach (DTPedido Index in (List<DTPedido>)Session["ubicaciones"])
                {
                    Ubicaciones.Add(Index.Ubicacion);
                }
                Session["ruta"] = new ControllerLogIn().getRUTA(IdR, Ubicaciones);
                for (int Index = 0; Index < ((List<Ubicacion>)Session["ruta"]).Count - 1; Index++)
                {
                    GoogleDirections Temporal = new GoogleDirections
                    {
                        Origin = new Location(((List<Ubicacion>)Session["ruta"])[Index].Latitud, ((List<Ubicacion>)Session["ruta"])[Index].Longitud),
                        Destination = new Location(((List<Ubicacion>)Session["ruta"])[Index + 1].Latitud, ((List<Ubicacion>)Session["ruta"])[Index + 1].Longitud),
                        Draggable = false,
                        PanelID = "PanelId",
                        ID = Index.ToString(),
                        RouteIndex = 0
                    };
                    Temporal.MarkerOptions.Visible = false;
                    Temporal.OptimizeWaypoints = true;
                    MapUbicaciones.Add(Temporal);
                }
                foreach (DTPedido Index in (List<DTPedido>)Session["ubicaciones"])
                {
                    Marker Nuevo = new Marker
                    {
                        Position = new LatLng(Index.Ubicacion.Latitud, Index.Ubicacion.Longitud),
                        Draggable = false
                    };
                    if (Index.Estado.Id == 1)
                    {
                        for (int i = 0; i < ((List<Ubicacion>)Session["ruta"]).Count; i++)
                        {
                            if (((List<Ubicacion>)Session["ruta"])[i].Id == Index.Ubicacion.Id)
                            {
                                Nuevo.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%23FCDC00&text=" + i.ToString() + "&color=%23FFFFFF&voffset=2&hoffset=1&";
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ((List<Ubicacion>)Session["ruta"]).Count; i++)
                        {
                            if (((List<Ubicacion>)Session["ruta"])[i].Id == Index.Ubicacion.Id)
                            {
                                Nuevo.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%2368BC00&text=" + i.ToString() + "&color=%23FFFFFF&voffset=2&hoffset=1&";
                            }
                        }
                    }
                    MapMarcadores.Add(Nuevo);
                }
            }
            Marker Principio = new Marker
            {
                Position = new LatLng(new ControllerLogIn().getUbicacionPrincipalRecicladores(IdR).Latitud, new ControllerLogIn().getUbicacionPrincipalRecicladores(IdR).Longitud),
                Draggable = false,
                Clickable = false
            };
            Principio.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%237B64FF&icon=fa-flag-checkered&color=%23FFFFFF&voffset=0&hoffset=1&";
            MapMarcadores.Add(Principio);
            divDatos.Visible = false;
        }

        protected void btnRecolectado_Click(object sender, EventArgs e)
        {
            long IdP = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Id;
            new ControllerLogIn().RecolectPedido(IdP);
            UpdateMap();
        }
        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Reciclador.aspx");
        }
    }
}