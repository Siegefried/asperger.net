﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reciclar.aspx.cs" Inherits="WebApplication.Pages.Reciclar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/Reciclar.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="ViewMateriales" runat="server">
                <div class="VentanaPrincipal">
                    <label id="Titulo1" class="Titulo">Reciclar </label>
                    <asp:Panel ID="PanelPrincipal" runat="server" CssClass="DisplayTable">
                    </asp:Panel>
                    <label id="labelninja" visible="false" runat="server" class="ValidatorUltimate100000IQ">Solo se pueden ingresar numeros, por favor corrija las cantidades ingresadas.</label>
                    <asp:Button ID="Button1" runat="server" Text="Menu Principal" OnClick="MenuPrincipal" CssClass="AnteriorView" />
                    <asp:Button ID="SiguienteView1" runat="server" Text="Siguiente" OnClick="SiguienteView1_Click" CssClass="SiguienteView" />
                </div>
            </asp:View>
            <asp:View ID="ViewCoordinacion" runat="server">
                <div class="VentanaPrincipal2">
                    <label id="Titulo2" class="Titulo">Reciclar </label>
                    <div class="ContenedorFecha">
                        <label class="SubTitulo">Fecha</label>
                        <div class="SubContenedorFecha">
                            <asp:TextBox ID="txtFecha" Enabled="false" runat="server" CssClass="Fecha"></asp:TextBox>
                            <asp:ImageButton CausesValidation="False" ID="Calendario" runat="server" CssClass="Calendario" ImageUrl="~/Imagenes/Calendario.png" OnClick="Calendario_Click" />
                            <asp:Calendar ID="Calendar1" CausesValidation="False" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" OnSelectionChanged="Calendar1_SelectionChanged" Width="200px" OnDayRender="Calendar1_DayRender">
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                            </asp:Calendar>
                            <br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Debe elegir una fecha" ControlToValidate="txtFecha" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="ContenedorHorario">
                        <label class="SubTitulo2">Horario</label>
                        <div class="SubContenedorHorario">
                            <asp:RadioButtonList ID="rbHorario" runat="server" Align="center">
                                <asp:ListItem>10 - 12 Horas</asp:ListItem>
                                <asp:ListItem>12 - 16 Horas</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator8" runat="server" ErrorMessage="Debe elegir un horario" ControlToValidate="rbHorario" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <div class="ContenedorUbicacion">
                        <label class="SubTitulo3">Ubicación</label>
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="ListaUbicaciones" AppendDataBoundItems="true"><asp:ListItem Value=""></asp:ListItem></asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe elegir una ubicacion" ControlToValidate="DropDownList1" InitialValue="" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Button ID="btnAdministrarUbicacion" CausesValidation="False" runat="server" Text="Nueva Ubicación" OnClick="btnAdministrarUbicacion_Click"/>
                    </div>
                        <asp:Button ID="Atras2" CausesValidation="False" runat="server" Text="Atras" CssClass="AnteriorView" OnClick="Atras2_Click" />
                        <asp:Button ID="SiguienteView2" runat="server" Text="Siguiente" OnClick="SiguienteView2_Click" CssClass="SiguienteView" />
                    </div>
            </asp:View>
            <asp:View ID="ViewResumen" runat="server">
                <div class="VentanaPrincipal3">
                    <label id="Titulo3" class="Titulo">Reciclar </label>
                    <div class="ContenedorResumen">
                        <label class="SubTitulo4">Resumen</label>
                             <asp:Panel ID="PanelPrincipal2" runat="server" CssClass="DisplayTable2"></asp:Panel>
                        <div class="lblDireccion">
                        <asp:Label ID="Label2" runat="server" Text="Label">Direccion: </asp:Label>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </div>
                        <div class="lblHora">
                            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                            <br />
                            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="lblObservacion">
                            <asp:Label ID="Label4" runat="server" Text="Label">Obsevaciónes:</asp:Label>
                            <textarea id="TextArea1" runat="server"></textarea>
                        </div>
                    </div>
                    
                    <asp:Button ID="Cancelar" runat="server" Text="Cancelar" CssClass="btnCancelar" OnClick="Cancelar_Click"/>
                    <asp:Button ID="Atras" runat="server" Text="Atras" CssClass="btnAtras" OnClick="Atras_Click"/>
                    <asp:Button ID="Aceptar" runat="server" Text="Aceptar" CssClass="btnAceptar" OnClick="Aceptar_Click"/>
                </div>
            </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>


