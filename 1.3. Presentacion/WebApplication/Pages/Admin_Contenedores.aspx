﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_Contenedores.aspx.cs" Inherits="WebApplication.Pages.Admin_Contenedores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/Admin_Contenedores.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="ContenedorPrincipal">
            <label id="Titulo1" class="Titulo">CONTAINERS</label>
            <div class="Contenedor2">
                <div class="ninjadiv">
                </div>
                <div>
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <map:GoogleMap ID="MapUbicaciones"
                                runat="server"
                                Latitude="-34.9000015" Longitude="-54.9500008"
                                Zoom="16" MapType="Roadmap">
                            </map:GoogleMap>
                            <map:GoogleMarkers ID="MapMarcadores"
                                TargetControlID="MapUbicaciones"
                                OnClick="MapMarcadores_Click"
                                OnClientClick="return;"
                                runat="server">
                            </map:GoogleMarkers>
                            <div runat="server" id="divVisible">
                                <asp:Label ID="lblTipoResiduo" runat="server" Text=""></asp:Label>
                                <br />
                                <asp:Label ID="lblEstado" runat="server" Text=""></asp:Label>
                                <br />
                                <asp:Button ID="btnVaciarContainer" runat="server" Text="Vaciar container" OnClick="btnVaciarContainer_Click" />
                                <br />
                                <asp:Button ID="btnEliminarContainer" runat="server" Text="Dar container de baja" OnClick="btnEliminarContainer_Click" />
                            </div>
                            <asp:Button ID="btnNuevoContainer" runat="server" Text="Agregar nuevo container" OnClick="btnNuevoContainer_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" CssClass="Atras" OnClick="Atras" />
        </div>
    </form>
</body>
</html>
