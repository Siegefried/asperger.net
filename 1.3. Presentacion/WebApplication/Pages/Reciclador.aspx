﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reciclador.aspx.cs" Inherits="WebApplication.Pages.Reciclador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../Estilos/Reciclador.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="Contenedor">
        <div class="secciones">
            <a href="Reciclador_Retirar(VerPedidosDisponibles).aspx" title="Retirar"><img src="../imagenes/retirar.png" class="icono"></a>
            <a href="Reciclador_Retirar(VerPedidosDisponibles).aspx" title="Retirar"><label class="reciclar">Retirar</label></a>
        </div>
        <div class="secciones">
            <a href="Reciclador_Ruta.aspx" title="Ruta"><img src="../imagenes/ruta3.png" class="icono"></a>
            <a href="Reciclador_Ruta.aspx" title="Ruta"><label class="reciclar">Ruta</label></a>
        </div>
        <div class="secciones">
                <a href="MisRetiros_Reciclador.aspx" title="Retiros"><img src="../imagenes/retiros.png" class="icono"></a>
                <a href="MisRetiros_Reciclador.aspx" title="Retiros"><label class="reciclar">Retiros</label></a>
            </div>
        <div class="secciones">
            <a href="MisDatos_Reciclador.aspx" title="Mis Datos"><img src="../imagenes/misdatos.png" class="icono"></a>
            <a href="MisDatos_Reciclador.aspx" title="Mis Datos"><label class="reciclar">Mis Datos</label></a>
        </div>
        <div class="secciones">
            <a href="Logout.aspx" title="Salir"><img src="../imagenes/salir.png" class="icono"></a>
            <a href="Logout.aspx" title="Salir"><label class="reciclar">Salir</label></a>
        </div>
    </div>
</body>
</html>
