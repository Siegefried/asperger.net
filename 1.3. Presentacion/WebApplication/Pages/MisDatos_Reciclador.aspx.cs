﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using DataTransferObjects.Entities;
using WebApplication.Controllers;
using GoogleMaps.Markers;
using GoogleMaps;
using System.IO;
using DataTransferObjects.Enums;

namespace WebApplication.Pages
{
    public partial class MisDatos_Reciclador : System.Web.UI.Page
    {
        //private List<Ubicacion> Ubicaciones;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Inicio - quitar esto en la version final, solo lo use para testear mas rapido
            //string nick = "Gaby98";
            //string pass = "123456";

            //DTLogIn logIn = new ControllerLogIn().logIn(nick, pass);

            //if (logIn != null)
            //{
            //    Session["LoggedUser"] = logIn;

            //}
            //Fin

            MapUbicaciones.MapType = MapType.Roadmap;
            List<Ubicacion> listaubicaciones = new ControllerLogIn().getUbicacionReciclador(((DTLogIn)Session["LoggedUser"]).id);
            foreach (Ubicacion Index in listaubicaciones)
            {
                MapMarcadores.Add(new Marker
                {
                    Position = new LatLng(Index.Latitud, Index.Longitud),
                    Info = "Alias: " + Index.Alias
                });
            }


            //Cargo Imagen de Perfil - Inicio
            long IdInt = ((DTLogIn)Session["LoggedUser"]).id;
            byte[] bytes = new ControllerLogIn().getUserImage(IdInt);
            byte[] newarraybytes = new byte[0];
            if (bytes.Length != newarraybytes.Length)
            {
                string strBase64 = Convert.ToBase64String(bytes);
                Image1.ImageUrl = "data:Image/png;base64," + strBase64;
            }
            //Fin
            //Cargo datos del Perfil
            txtapellido.Text = ((DTLogIn)Session["LoggedUser"]).apellido;
            txtedad.Text = ((DTLogIn)Session["LoggedUser"]).edad.ToString();
            txtemail.Text = ((DTLogIn)Session["LoggedUser"]).email;
            txtnickname.Text = ((DTLogIn)Session["LoggedUser"]).nickname;
            txtnombre.Text = ((DTLogIn)Session["LoggedUser"]).nombre;
            //Fin
            //Calculo y cargo su valoracion
            int valoracion = 0;
            int x = 0;
            List<DTPedido> ListaPedidosReciclador = new ControllerLogIn().getAllPedidosReciclador(IdInt);
            foreach(DTPedido Index in ListaPedidosReciclador)
            {
                if((int)Index.Valoracion != 0)
                {
                    valoracion = valoracion + (int)Index.Valoracion;
                    x++;
                }
            }
            if (x != 0)
            {
                valoracion = valoracion / x;
            }
            txtvaloracion.Text = valoracion.ToString();
            //Fin

        }
        protected void cvImagen_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var NombreArchivo = args.Value;
            args.IsValid =
            ((NombreArchivo.ToLower().EndsWith(".jpg")) ||
            (NombreArchivo.ToLower().EndsWith(".jpeg")) ||
            (NombreArchivo.ToLower().EndsWith(".gif")) ||
            (NombreArchivo.ToLower().EndsWith(".png")));
        }
        protected void BtnModificarDatos(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_ModificarDatos.aspx");
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Reciclador.aspx");
        }
    }
}