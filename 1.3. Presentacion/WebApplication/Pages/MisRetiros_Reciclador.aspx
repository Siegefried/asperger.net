<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisRetiros_Reciclador.aspx.cs" Inherits="WebApplication.Pages.MisRetiros_Reciclador" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Estilos/MisRetiros_Reciclador.css" rel="stylesheet" type="text/css" />
</head>
    <body>
        <form id="form1" runat="server">
            <div>
                <asp:MultiView ID="MultiView" runat="server">
                    <asp:View ID="ViewGrilla" runat="server">
                        <div class="VentanaPrincipal">
                            <label id="Titulo1" class="Titulo">Retiros</label>
                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:ListView ID="ListView1" runat="server" OnItemCommand="lstView_ItemCommand">
                                <LayoutTemplate>
                                    <div class="ScrollBar2">
                                    <table id="Tabla_Datos_Generales" border="1">
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Direccion</th>
                                            <th>Estado</th>
                                            <th>Reciclador</th>
                                            <th>Resumen</th>
                                        </tr>
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </table>
                                        </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td> <%# Eval("Fecha") %> </td>
                                        <td> <%# ((bool)Eval("Hora")) ? "10 - 12" : "12 - 16" %></td>
                                        <td> <%# Eval("Ubicacion") %> </td>
                                        <td> <%# Eval("Estado") %> </td>
                                        <td> <%# Eval("Reciclador") %> </td>
                                        <td>
                                            <asp:Button ID="btnResumen" runat="server" Text="Resumen" CommandName="verResumen" CommandArgument='<%#Eval("Id")%>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" Font-Size="20px" runat="server" Text="No hay Datos"></asp:Label>
                                </EmptyDataTemplate>
                            </asp:ListView>
                            <asp:Button ID="btnEstadisticas" runat="server" OnClick="Button1_Click" Text="Estadisticas" />
                            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="Atras" CssClass="Atras" />
                        </div>
                    </asp:View>
                    <asp:View ID="ViewResumen" runat="server">
                        <div class="VentanaPrincipal">
                            <label id="Titulo3" class="SubTitulo1">Datos Materiales para Reciclar</label>
                            <div class="ScrollBar">
                                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                            </div>
                            <label id="Titulo4" class="SubTitulo2">Datos Pedido</label>
                            <asp:DetailsView ID="DetailsView1" AllowPaging="false" AutoGenerateRows="false" CssClass="Tabla_Resumen" runat="server">
                                <Fields>
                                    <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" HeaderStyle-CssClass="Titulo_Tabla_Resumen" ReadOnly="true" />
                                        <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="Titulo_Tabla_Resumen">
                                            <ItemTemplate>
                                                <span><%# Eval("Fecha", "{0:d}") %>  </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Hora" HeaderStyle-CssClass="Titulo_Tabla_Resumen">
                                            <ItemTemplate>
                                                <span><%# ((bool)Eval("Hora")) ? "10 - 12" : "12 - 16" %> </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                            <asp:Button ID="btnCancelar" runat="server" Text="Atras" OnClick="btnCancelar_Click" />
                            <asp:Label ID="lblObservaciones" runat="server" Text="Observaciones"></asp:Label>
                            <br />
                            <textarea id="txtObservaciones2" readonly="readonly" runat="server"></textarea>
                            <asp:Label ID="lblValoraciones" runat="server" Text="Valoracion"></asp:Label>

                            <asp:DropDownList ID="ddlbValoraciones" Enabled="false" runat="server"></asp:DropDownList>
                            
                        </div>
                    </asp:View>
                    <%--<asp:View ID="ViewResumenMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                        <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click" />
                    </asp:View>--%>
                    <asp:View ID="ViewEstadisticas" runat="server">
                        <div class="VentanaPrincipal3">
                            <asp:Label ID="Label2" runat="server" CssClass="Titulo" Text="Estadisticas"></asp:Label>
                        <div class="ScrollBar3">
                                <asp:GridView ID="GridView2" runat="server"></asp:GridView>
                            </div>
                            <asp:Button ID="Button1" runat="server" Text="Atras" OnClick="Button1_Click1" />
                            </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </form>
    </body>
</html>
