﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Reciclador_Retirar_VerPedidosDisponibles_ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["LoggedUser"] = new ControllerLogIn().logIn("Gaby98", "123456");
            MapUbicaciones.MapType = MapType.Roadmap;
            MapMarcadores.MarkerOptions.Draggable = false;
            if (!IsPostBack)
            {
                //#region TipoResiduo
                //Session["TiposResiduo"] = new ControllerLogIn().TraerDatosTiposDeBasura();
                //ddlTipoResiduo.Items.Add(new ListItem
                //{
                //    Text = "-------------------",
                //    Value = "Vacio"
                //});
                //foreach (DTTipoResiduo Index in (List<DTTipoResiduo>)Session["TiposResiduo"])
                //{
                //    ListItem IndexListItem = new ListItem();
                //    IndexListItem.Text = Index.nombre;
                //    IndexListItem.Value = Index.nombre;
                //    Color Color = System.Drawing.ColorTranslator.FromHtml("#" + Index.color);
                //    Color Contrario = Color.FromArgb(255 - Color.R, 255 - Color.G, 255 - Color.B);
                //    IndexListItem.Attributes.Add("style", "color:" + ColorTranslator.ToHtml(Contrario) + "; background-color: #" + Index.color);

                //    ddlTipoResiduo.Items.Add(IndexListItem);
                //}
                //#endregion
                #region TipoResiduo
                Session["Filtros"] = new List<DTTipoResiduo>();
                Session["TiposResiduo"] = new ControllerLogIn().TraerDatosTiposDeBasura();
                foreach (DTTipoResiduo Index in (List<DTTipoResiduo>)Session["TiposResiduo"])
                {
                    ListItem IndexListItem = new ListItem();
                    IndexListItem.Text = Index.nombre;
                    IndexListItem.Value = Index.nombre;
                    Color Color = System.Drawing.ColorTranslator.FromHtml("#" + Index.color);
                    Color Contrario = IdealTextColor(Color);
                    IndexListItem.Attributes.Add("style", "color:" + ColorTranslator.ToHtml(Contrario) + "; background-color: #" + Index.color);

                    cblTipoResiduo.Items.Add(IndexListItem);
                }

                #endregion
                Session["Horario"] = false;
                Session["TipoResiduo"] = (long)-1;
                Session["currentIndex"] = -1;
                Session["Fecha"] = new DateTime();
                UpdateMap();
            }
        }

        protected void calFechas_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date < DateTime.Now.Date)
            {
                e.Day.IsSelectable = false;
                e.Cell.ForeColor = System.Drawing.Color.Red;
                e.Cell.Font.Strikeout = true;
            }
        }

        protected void UpdateMap()
        {
            bool Horario = !Convert.ToBoolean(Session["Horario"]);
            long IdTR = (long)Session["TipoResiduo"];
            long IdR = ((DTLogIn)Session["LoggedUser"]).id;
            DateTime Fecha = ((DateTime)Session["Fecha"]);
            List<DTTipoResiduo> lstTipoResiduo = (List<DTTipoResiduo>)Session["Filtros"];
            //Session["ubicaciones"] = new ControllerLogIn().TraerPedidosDisponiblesRecicladorFiltro(((DTLogIn)Session["LoggedUser"]).id, IdTR, Horario, Fecha);
            Session["ubicaciones"] = new ControllerLogIn().TraerPedidosDisponiblesRecicladorMultiFiltro(((DTLogIn)Session["LoggedUser"]).id, lstTipoResiduo, Horario, Fecha);
            MapMarcadores.Markers.Clear();
            if (((List<DTPedido>)Session["ubicaciones"]).Count() == 0)
            {
                ninjadiv.Visible = true;
                //map.Visible = false;
            }
            else
            {
                MapMarcadores.Markers = new List<Marker>();
                foreach (DTPedido Index in (List<DTPedido>)Session["ubicaciones"])
                {
                    Marker Nuevo = new Marker
                    {
                        Position = new LatLng(Index.Ubicacion.Latitud, Index.Ubicacion.Longitud),
                        Draggable = false
                    };
                    if (Index.Reciclador != null)
                    {
                        Nuevo.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%2368BC00&icon=fa-check&color=%23000000&voffset=0&hoffset=1&";
                    }
                    MapMarcadores.Add(Nuevo);
                }
            }
            foreach (ListItem Index in cblTipoResiduo.Items)
            {
                Color Color = System.Drawing.ColorTranslator.FromHtml("#" + ((List<DTTipoResiduo>)Session["TiposResiduo"])[cblTipoResiduo.Items.IndexOf(Index)].color);
                Color Contrario = IdealTextColor(Color);
                Index.Attributes.Add("style", "color:" + ColorTranslator.ToHtml(Contrario) + "; background-color: #" + ((List<DTTipoResiduo>)Session["TiposResiduo"])[cblTipoResiduo.Items.IndexOf(Index)].color);
            }
            divDatos.Visible = false;
        }

        //protected void ddlTipoResiduo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlTipoResiduo.SelectedIndex != 0)
        //    {
        //        Session["TipoResiduo"] = ((List<DTTipoResiduo>)Session["TiposResiduo"])[ddlTipoResiduo.SelectedIndex - 1].id;
        //    }
        //    else
        //    {
        //        Session["TipoResiduo"] = (long)-1;
        //    }

        //    UpdateMap();
        //}

        protected void ddlHorario_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Horario"] = ddlHorario.SelectedIndex;
            UpdateMap();
        }

        protected void MapMarcadores_Click(object sender, MarkerEventArgs e)
        {
            UpdateMap();
            divDatos.Visible = true;
            Session["currentIndex"] = (int)e.Index;
            lblPropietario.Text = "Propietario: " + ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Persona;
            foreach (DTMaterialCantidad Index in ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Materiales)
            {
                Label TemporalLabel = new Label();
                TemporalLabel.Text = Index.Nombre + ": " + Index.Cantidad;
                pnpMateriales.Controls.Add(TemporalLabel);
            }
            lblObservaciones.Text = "Observaciones: " + ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Observaciones;
            if (((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Reciclador != null)
            {
                btnDone.Visible = false;
                btnLiberar.Visible = true;
            }
            else
            {
                btnDone.Visible = true;
                btnLiberar.Visible = false;
            }
            MapUbicaciones.Latitude = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Ubicacion.Latitud;
            MapUbicaciones.Longitude = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Ubicacion.Longitud;
        }

        protected void calFechas_SelectionChanged(object sender, EventArgs e)
        {
            Session["Fecha"] = calFechas.SelectedDate;
            UpdateMap();
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            long IdP = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Id;
            long IdR = ((DTLogIn)Session["LoggedUser"]).id;
            new ControllerLogIn().LockPedido(IdR, IdP);
            UpdateMap();
        }

        protected void btnLiberar_Click(object sender, EventArgs e)
        {
            long IdP = ((List<DTPedido>)Session["ubicaciones"])[(int)Session["currentIndex"]].Id;
            new ControllerLogIn().unLockPedido(IdP);
            UpdateMap();
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Reciclador.aspx");
        }

        public Color IdealTextColor(Color bg)
        {
            int nThreshold = 105;
            int bgDelta = Convert.ToInt32((bg.R * 0.299) + (bg.G * 0.587) +
                                          (bg.B * 0.114));

            Color foreColor = (255 - bgDelta < nThreshold) ? Color.Black : Color.White;
            return foreColor;
        }

        protected void cblTipoResiduo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Filtros"] = new List<DTTipoResiduo>();
            foreach (ListItem Index in cblTipoResiduo.Items)
            {
                if (Index.Selected)
                {
                    ((List<DTTipoResiduo>)Session["Filtros"]).Add(((List<DTTipoResiduo>)Session["TiposResiduo"])[cblTipoResiduo.Items.IndexOf(Index)]);
                }
                Color Color = System.Drawing.ColorTranslator.FromHtml("#" + ((List<DTTipoResiduo>)Session["TiposResiduo"])[cblTipoResiduo.Items.IndexOf(Index)].color);
                Color Contrario = IdealTextColor(Color);
                Index.Attributes.Add("style", "color:" + ColorTranslator.ToHtml(Contrario) + "; background-color: #" + ((List<DTTipoResiduo>)Session["TiposResiduo"])[cblTipoResiduo.Items.IndexOf(Index)].color);
            }
            UpdateMap();
        }
    }
}