﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Persona.aspx.cs" Inherits="WebApplication.Pages.Persona" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../Estilos/Persona.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="Contenedor">
        <div class="secciones">
            <a href="Reciclar.aspx" title="Reciclar"><img src="../imagenes/reciclar2.png" class="icono"></a>
            <a href="Reciclar.aspx" title="Reciclar"><label class="reciclar">Reciclar</label></a>
        </div>
        <div class="secciones">
            <a href="MisRetiros_Persona.aspx" title="Retiros"><img src="../imagenes/retiros.png" class="icono"></a>
            <a href="MisRetiros_Persona.aspx" title="Retiros"><label class="reciclar">Retiros</label></a>
        </div>
        <div class="secciones">
            <a href="Containers.aspx" title="Contenedores"><img src="../imagenes/contenedores.png" class="icono"></a>
            <a href="Containers.aspx" title="Contenedores"><label class="reciclar">Contenedores</label></a>
        </div>
        <div class="secciones">
            <a href="MisDatos_Persona.aspx" title="Mis Datos"><img src="../imagenes/misdatos.png" class="icono"></a>
            <a href="MisDatos_Persona.aspx" title="Mis Datos"><label class="reciclar">Mis Datos</label></a>
        </div>
        <div class="secciones">
            <a href="MisDatos_Persona_MisRecolectores.aspx" title="Mis Recolectores"><img src="../imagenes/misrecolectores.png" class="icono"></a>
            <a href="MisDatos_Persona_MisRecolectores.aspx" title="Mis Recolectores"><label class="reciclar">Mis Recolectores</label></a>
        </div>
        <div class="secciones">
            <a href="Logout.aspx" title="Salir"><img src="../imagenes/salir.png" class="icono"></a>
            <a href="Logout.aspx" title="Salir"><label class="reciclar">Salir</label></a>
        </div>
    </div>
</body>
</html>
