﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Usuarios_Admin : System.Web.UI.Page
    {
        private DataTable table = new DataTable("Tabla1");
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> ListaUsuarios;
            ListaUsuarios = new ControllerLogIn().TraerListaUsuarios();
            table.Columns.Add(new DataColumn("Id", typeof(int)));
            table.Columns.Add(new DataColumn("Nombre", typeof(string)));
            table.Columns.Add(new DataColumn("Apellido", typeof(string)));
            table.Columns.Add(new DataColumn("Edad", typeof(int)));
            table.Columns.Add(new DataColumn("NickName", typeof(string)));
            table.Columns.Add(new DataColumn("Email", typeof(string)));
            table.Columns.Add(new DataColumn("Bloqueado", typeof(string)));
            foreach (string Index in ListaUsuarios)
            {
                DataRow row = table.NewRow();
                row["Nombre"] = new ControllerLogIn().GetDatosPersona(Index).nombre;
                row["Apellido"] = new ControllerLogIn().GetDatosPersona(Index).apellido;
                row["Edad"] = new ControllerLogIn().GetDatosPersona(Index).edad;
                row["NickName"] = new ControllerLogIn().GetDatosPersona(Index).nickname;
                row["Email"] = new ControllerLogIn().GetDatosPersona(Index).email;
                row["Id"] = new ControllerLogIn().GetDatosPersona(Index).id;
                if(new ControllerLogIn().GetDatosPersona(Index).bloqueado)
                {
                    row["Bloqueado"] = "Si";
                }
                else
                {
                    row["Bloqueado"] = "No";
                }
                table.Rows.Add(row);
            }
            GridView1.DataSource = table;
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "")
            {
                string nombretxt = "Nombre = '" + TextBox1.Text + "'";
                table.DefaultView.RowFilter = nombretxt;
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
            if (TextBox2.Text != "")
            {
                string nombretxt = "Apellido = '" + TextBox2.Text + "'";
                table.DefaultView.RowFilter = nombretxt;
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
            if (TextBox3.Text != "")
            {
                string nombretxt = "Edad = '" + TextBox3.Text + "'";
                table.DefaultView.RowFilter = nombretxt;
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
            if (TextBox4.Text != "")
            {
                string nombretxt = "Email = '" + TextBox4.Text + "'";
                table.DefaultView.RowFilter = nombretxt;
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
            if (TextBox5.Text != "")
            {
                string nombretxt = "NickName = '" + TextBox5.Text + "'";
                table.DefaultView.RowFilter = nombretxt;
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
            if (TextBox1.Text == "" && TextBox2.Text == "" && TextBox3.Text == "" && TextBox4.Text == "" && TextBox5.Text == "")
            {

                table.DefaultView.RowFilter = "";
                GridView1.DataSource = table;
                GridView1.DataBind();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                long idUsuario = long.Parse(TextBox6.Text);
                new ControllerLogIn().BloquearDesbloquearUsuario(idUsuario);
                Response.Redirect("/Pages/Usuarios_Admin.aspx");
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                long idUsuario = long.Parse(TextBox6.Text);
                new ControllerLogIn().BloquearDesbloquearUsuario(idUsuario);
                Response.Redirect("/Pages/Usuarios_Admin.aspx");
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            TextBox5.Text = "";
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            long idUsuario = long.Parse(TextBox6.Text);
            if (new ControllerLogIn().isUsuario(idUsuario))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Admin.aspx");
        }
    }
}