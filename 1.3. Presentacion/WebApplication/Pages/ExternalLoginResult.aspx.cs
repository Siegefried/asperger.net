﻿using DataTransferObjects.Entities;
using Nemiro.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class ExternalLoginResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthorizationResult result = OAuthWeb.VerifyAuthorization();

            if (result.IsSuccessfully)
            {
                var userInfo = result.UserInfo;
                //userInfo.FirstName, userInfo.LastName, userInfo.Email, userInfo.DisplayName

                //bool mailUsed = new ControllerLogIn().checkMail(userInfo.Email);
                DTLogIn logIn = new ControllerLogIn().externallogIn(userInfo.Email);
                if(logIn == null)
                {
                    logIn = new DTLogIn();
                    logIn.bloqueado = false;
                }
                else if (logIn != null)
                {
                    if (logIn.bloqueado == true)
                    {
                        Session["Bloqueado"] = true;
                        Response.Redirect("NuevoLogin.aspx");
                    }
                    else
                    {
                        Session["LoggedUser"] = logIn;
                        Response.Redirect("LoginConfirm.aspx");
                    }
                }
                
                    //get extra data, preload with session data and delegate to registry
                    GoogleResponse googleResponse = requestFurtherData(result);
                    GoogleResponse.NickNames nicks = null;
                    try
                    {
                        nicks = googleResponse.nicknames.First();
                    }
                    catch (Exception ex)
                    {

                    }

                    Session["extAuthNick"] = (nicks != null) ? nicks.value : "";

                    //GoogleResponse.BirthDay birth = googleResponse.birthdays.First();
                    foreach (GoogleResponse.BirthDay Index in googleResponse.birthdays)
                    {
                        GoogleResponse.BirthDay birth = Index;
                        if (birth != null)
                        {

                            try
                            {
                                GoogleResponse.Date date = birth.date;
                                DateTime dateOfBirth = new DateTime(date.year, date.month, date.day);
                                TimeSpan timeSpan = DateTime.Today.Subtract(dateOfBirth);
                                Session["extAuthAge"] = (int)(timeSpan.TotalDays / 365.25);
                            }
                            catch (Exception ex)
                            {

                            }

                        }
                    }

                    Session["extAuthResult"] = result;
                    Response.Redirect("NuevoRegistro.aspx");
                
            }
            else
            {
                // error
                Response.Write(result.ErrorInfo.Message);
                Response.Write("<hr>");
                Response.Write("Erro al usar Google como proveer externo de identificacion.");
            }
        }//protected void Page_Load(object sender, EventArgs e) END

        //Nicks and birthday don't come with the basic auth userProfile, requesting moar data.
        private GoogleResponse requestFurtherData(AuthorizationResult result)
        {
            String urlbirthday = "https://people.googleapis.com/v1/people/" + result.UserInfo.UserId + "?personFields=birthdays,nicknames&key=AIzaSyALhQNHxtV-ksTzAvz4E9_O1_scQLV8T2M";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, urlbirthday);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Bearer " + result.AccessTokenValue);

            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> response = client.SendAsync(request);
            response.Wait();

            HttpContent shit = response.GetAwaiter().GetResult().Content;

            Task<string> receiveStream = shit.ReadAsStringAsync();
            receiveStream.Wait();

            string resultBodyJson = receiveStream.GetAwaiter().GetResult();

            JavaScriptSerializer ser = new JavaScriptSerializer();
            GoogleResponse googleResponse = ser.Deserialize<GoogleResponse>(resultBodyJson);

            return googleResponse;
        }//private GoogleResponse requestFurtherData(UserInfo userInfo) END

        // Dummy class to deserialize responses fromn googleAPI.
        private class GoogleResponse
        {
            public string resourceName;
            public string etag;
            public List<NickNames> nicknames;
            public List<BirthDay> birthdays;

            public class NickNames
            {
                public Metadata metadata;
                public string value;
            }
            public class BirthDay
            {
                public Metadata metadata;
                public Date date;
            }
            public class Metadata
            {
                public bool primary;
                public Source source;

            }
            public class Source
            {
                public string type;
                public string id;
            }
            public class Date
            {
                public int year;
                public int month;
                public int day;
            }
        }//private class GoogleResponse END


    }//public partial class ExternalLoginResult : System.Web.UI.Page END
}//namespace WebApplication.Pages END