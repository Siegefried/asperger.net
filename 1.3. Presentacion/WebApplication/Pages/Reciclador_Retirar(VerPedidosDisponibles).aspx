﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reciclador_Retirar(VerPedidosDisponibles).aspx.cs" Inherits="WebApplication.Pages.Reciclador_Retirar_VerPedidosDisponibles_" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/Reciclador_Retirar.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class=" loginbox">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div class="Contenedor2">
                <asp:Label ID="Titulo" runat="server" Text="Retirar"></asp:Label>
                <div class="ninjadiv" id="ninjadiv" runat="server" visible="true"></div>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div id="map" runat="server" visible="true">
                            <map:GoogleMap ID="MapUbicaciones"
                                runat="server"
                                Latitude="-34.9000015" Longitude="-54.9500008"
                                Zoom="16" MapType="Roadmap"
                                autopostback="false" xmlns:asp="#unknown">
                            </map:GoogleMap>
                            <map:GoogleMarkers ID="MapMarcadores"
                                TargetControlID="MapUbicaciones"
                                OnClick="MapMarcadores_Click"
                                runat="server">
                            </map:GoogleMarkers>
                        </div>
                        <div runat="server" id="divFiltros">
                            <asp:Label ID="SubTitulo" runat="server" Text="Filtros"></asp:Label>
                            <br />
                            <asp:Label ID="lblhorario" runat="server" Text="Horario:"></asp:Label>
                            <asp:DropDownList ID="ddlHorario" runat="server" OnSelectedIndexChanged="ddlHorario_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Text="10:00 - 12:00" Value="0" />
                                <asp:ListItem Text="12:00 - 16:00" Value="1" />
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lbltiporesiduo" runat="server" Text="Tipo de residuo:"></asp:Label>
                            <asp:CheckBoxList ID="cblTipoResiduo" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="cblTipoResiduo_SelectedIndexChanged"></asp:CheckBoxList>
                            <br />
                            <asp:Label ID="lblfecha" runat="server" Text="Fecha:"></asp:Label>
                            <asp:Calendar ID="calFechas" runat="server" OnDayRender="calFechas_DayRender" AutoPostBack="True" OnSelectionChanged="calFechas_SelectionChanged"></asp:Calendar>
                        </div>
                        <div id="divDatos" runat="server" visible="false">
                            <asp:Label ID="SubTitulo2" runat="server" Text="Datos"></asp:Label>
                            <br />
                            <asp:Label ID="lblPropietario" runat="server" Text="Label"></asp:Label>
                            <br />
                            <asp:Label ID="lblObservaciones" runat="server" Text="Label"></asp:Label>
                            <br />
                            <asp:Label ID="lbltiporesiduo2" runat="server" Text="Tipos de Residuo:"></asp:Label>
                            <asp:Panel ID="pnpMateriales" runat="server">
                            </asp:Panel>
                            <br />
                            <asp:Button ID="btnDone" runat="server" OnClick="btnDone_Click" Text="Aplicar solicitud" />
                            <asp:Button ID="btnLiberar" OnClick="btnLiberar_Click" runat="server"  Text="Liberar" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
            <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="btnAtras_Click" />
        </div>
    </form>
</body>
</html>
