﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class EmailValidationLandingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string str = "user = Dreamangst, Nipioplosion /? validationtoken = dummytoktoken_ovEndlessTrveHate";
  
              //Label1.Text = Request.Url.GetComponents(UriComponents.Query, UriFormat.SafeUnescaped);

            string user = Request.QueryString["user"];
            string validationtoken = Request.QueryString["validationtoken"];
            string email = Request.QueryString["email"];

            bool res = new ControllerLogIn().emailUserValidation(email, validationtoken);

            //Response.Write(user);
            //Response.Write(validationtoken);
            //Response.Write(email);
            Response.Write(res);
        }
    }
}