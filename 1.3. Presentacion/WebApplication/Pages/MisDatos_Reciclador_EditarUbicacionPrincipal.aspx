﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_Reciclador_EditarUbicacionPrincipal.aspx.cs" Inherits="WebApplication.Pages.MisDatos_Reciclador_EditarUbicacionPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_Persona_EditarUbicaciones_NuevaUbicacion.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class=" loginbox">
            <div class="Contenedor2">
                <div class="ninjadiv"></div>
                <asp:ScriptManager ID="ScriptManagerAlpha" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div>
                            <map:GoogleMap ID="MapUbicaciones"
                                runat="server"
                                Latitude="-34.9000015" Longitude="-54.9500008"
                                Zoom="16"
                                MapType="Roadmap">
                            </map:GoogleMap>
                            <map:GoogleMarkers ID="MapMarcadores"
                                TargetControlID="MapUbicaciones"
                                runat="server" OnDragEnd="MapMarcadores_DragEnd">
                            </map:GoogleMarkers>
                        </div>
                        <p>
                            Latitud:<asp:Label ID="lblLatitud" runat="server" Text="-34.9000015"></asp:Label>
                        </p>
                        <p>
                            Longitud:<asp:Label ID="lblLongitud" runat="server" Text="-54.9500008"></asp:Label>
                        </p>
                        <p>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:Button ID="btnEditar"  CssClass="Botones" runat="server" Text="Guardar" OnClick="btnEditar_Click"/>
        </div>
    </form>
</body>
</html>
