﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_Reciclador.aspx.cs" Inherits="WebApplication.Pages.MisDatos_Reciclador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_Reciclador.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form class="formulario" runat="server" id="form1">
        <div class="ContenedorPrincipal">
            <div>
                <label id="titulo">RECICLADOR</label>
            </div>
            <div id="cuadroimagen">
                <asp:Image ID="Image1" runat="server" Width="300px" Height="300px" ImageUrl="~/Imagenes/FotoPorDefecto.png" />
            </div>
            <div class="Contenedor1">
                <div class="separador">
                    <label>Nombre:</label>
                    <br>
                    <asp:TextBox ID="txtnombre" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
                <div class="separador">
                    <label>Apellido:</label>
                    <br>
                    <asp:TextBox ID="txtapellido" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
                <div class="separador">
                    <label>Edad:</label>
                    <br>
                    <asp:TextBox ID="txtedad" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
                <div class="separador">
                    <label>Email:</label>
                    <br>
                    <asp:TextBox ID="txtemail" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
                <div class="separador">
                    <label>NickName:</label>
                    <br>
                    <asp:TextBox ID="txtnickname" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
                <div class="separador">
                    <label>Valoracion:</label>
                    <br>
                    <asp:TextBox ID="txtvaloracion" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
                <br>
            </div>
            <div class="Contenedor2">
                <div class="ninjadiv">
                    <asp:HyperLink ID="hlEditUbicaciones" runat="server" NavigateUrl="~/Pages/MisDatos_Reciclador_EditarUbicacionPrincipal.aspx">Administrar ubicacion</asp:HyperLink>
                </div>
                <div>
                    <asp:ScriptManager ID="ScriptManagerAlpha" runat="server"></asp:ScriptManager>
                    <map:GoogleMap ID="MapUbicaciones"
                        runat="server"
                        Latitude="-34.9000015" Longitude="-54.9500008"
                        Zoom="16"
                        MapType="Roadmap">
                    </map:GoogleMap>
                    <map:GoogleMarkers ID="MapMarcadores"
                        TargetControlID="MapUbicaciones"
                        runat="server">
                    </map:GoogleMarkers>
                </div>
            </div>
            <div style="position: initial; margin-top: 20px; margin-bottom: 20px; margin-left: 30%;">
                <asp:Button ID="BtnModificarDatosId" runat="server" Text="Modificar Datos" OnClick="BtnModificarDatos" CssClass="Campo2" />
                <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="Atras" CssClass="Atras" />

            </div>
        </div>
    </form>
</body>
</html>
