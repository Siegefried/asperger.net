﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class Admin_Contenedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Admin_Containers_IndexMapa"] = -1;
                Session["Admin_Containers_ListaContenedores"] = new ControllerLogIn().getContainersAll();
                divVisible.Visible = false;
            }
            MapUbicaciones.MapType = MapType.Roadmap;
            MapMarcadores.Markers.Clear();
            foreach (DTContainer index in (List<DTContainer>)Session["Admin_Containers_ListaContenedores"])
            {
                Marker Temporal = new Marker
                {
                    Position = new LatLng(index.Latitud, index.Longitud),
                    Info = index.TipoBasura,
                    Draggable = false
                };
                if (index.Lleno == false)
                    Temporal.Icon.Url = "https://cdn.mapmarker.io/api/v1/pin?size=60&background=%23" + index.Color + "&icon=fa-recycle&color=%23FFFFFF&voffset=0&hoffset=1&";
                else
                    Temporal.Icon.Url = "https://cdn.mapmarker.io/api/v1/fa?size=100&icon=fa-exclamation-triangle&color=%23" + index.Color + "&";
                MapMarcadores.Markers.Add(Temporal);
            }
        }

        protected void MapMarcadores_Click(object sender, MarkerEventArgs e)
        {
            Session["Admin_Containers_IndexMapa"] = (int)e.Index;
            lblTipoResiduo.Text = "Residuo: " + ((List<DTContainer>)Session["Admin_Containers_ListaContenedores"])[(int)e.Index].TipoBasura;
            if (((List<DTContainer>)Session["Admin_Containers_ListaContenedores"])[(int)e.Index].Lleno)
            {
                lblEstado.Text = "El contenedor esta LLENO";
                lblEstado.ForeColor = System.Drawing.Color.Red;
                btnVaciarContainer.Visible = true;
                divVisible.Visible = true;
            }
            else
            {
                lblEstado.Text = "El contenedor no esta LLENO";
                lblEstado.ForeColor = System.Drawing.Color.Green;
                btnVaciarContainer.Visible = false;
                divVisible.Visible = true;
            }
        }

        protected void btnVaciarContainer_Click(object sender, EventArgs e)
        {
            new ControllerLogIn().modifyContainerLlenoAdmin(((List<DTContainer>)Session["Admin_Containers_ListaContenedores"])[(int)Session["Admin_Containers_IndexMapa"]].Id);
            Response.Redirect(Request.RawUrl);
        }

        protected void btnEliminarContainer_Click(object sender, EventArgs e)
        {
            new ControllerLogIn().EliminarContainer(((List<DTContainer>)Session["Admin_Containers_ListaContenedores"])[(int)Session["Admin_Containers_IndexMapa"]].Id);
            Response.Redirect(Request.RawUrl);
        }

        protected void btnNuevoContainer_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Admin_Contenedores_ContainerNuevo.aspx");
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Admin.aspx");
        }
    }
}