﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MisDatos_Persona.aspx.cs" Inherits="WebApplication.Pages.MisDatos_Persona" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/MisDatos_Persona.css" rel="stylesheet" type="text/css" />
    <%--    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("a").removeAttr("href");
        });
     </script> 
        <script type="text/javascript">
        $(document).ready(function () {
            $(".remove-attr").click(function(){
            $("div").remove(":contains('Thank you for using the Development Build of <a>ASP.NET Google Map Control</a> to build Google Maps faster.')");
            });
            });
</script> --%>
</head>
<body>
    <form class="formulario" runat="server" id="form1">
        <div class="ContenedorPrincipal">
            <div>
                <label id="titulo">PERSONA</label>
                <%--<button type="button" class="remove-attr">Remove Link</button>--%>
            </div>
            <div id="cuadroimagen">
                <asp:Image ID="Image1" runat="server" Width="300px" ImageUrl="~/Imagenes/FotoPorDefecto.png" Height="300px" />
                <%--                                <br />
                                <asp:FileUpload ID="BtnSubirImg" runat="server" onchange="showimagepreview(this)" />
                                <br />
                                <asp:CustomValidator ID="cvImagen" runat="server" ErrorMessage="Formato de imagen no valido. (.JPG, .JPEG, .BMP y .PNG)" Display="Dynamic" ControlToValidate="BtnSubirImg" OnServerValidate="cvImagen_ServerValidate"></asp:CustomValidator>
                                <br />
                                <asp:Button ID="BtnImgPerfil" runat="server" Text="Confirmar Cambio de Foto" OnClick="BtnCambiarFotoConfirmar" />--%>
            </div>
            <div class="Contenedor1">
                <div class="separador">
                    <label>Nombre:</label>
                    <br>
                    <asp:TextBox ID="txtnombre" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                    <%--                    <asp:Button ID="BtnNombre" runat="server" Text="Confirmar Cambio de Nombre" OnClick="BtnCambiarNombreConfirmar" CssClass="Campo2" />--%>
                </div>
                <br>
                <div class="separador">
                    <label>Apellido:</label>
                    <br>
                    <asp:TextBox ID="txtapellido" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                    <%--                    <asp:Button ID="BtnApellido" runat="server" Text="Confirmar Cambio de Apellido" OnClick="BtnCambiarApellidoConfirmar" CssClass="Campo2" />--%>
                </div>
                <br>
                <div class="separador">
                    <label>Edad:</label>
                    <br>
                    <asp:TextBox ID="txtedad" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                    <%--                    <asp:Button ID="BtnEdad" runat="server" Text="Confirmar Cambio de Edad" OnClick="BtnCambiarEdadConfirmar" CssClass="Campo2" />--%>
                </div>
                <br>
                <div class="separador">
                    <label>Email:</label>
                    <br>
                    <asp:TextBox ID="txtemail" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                    <%--                    <asp:Button ID="BtnEmail" runat="server" Text="Confirmar Cambio de Email" OnClick="BtnCambiarEmailConfirmar" CssClass="Campo2" />--%>
                </div>
                <br>
                <div class="separador">
                    <label>NickName:</label>
                    <br>
                    <asp:TextBox ID="txtnickname" CssClass="Campo" runat="server" ReadOnly="true"></asp:TextBox>
                    <%--                    <asp:Button ID="BtnNickname" runat="server" Text="Confirmar Cambio de NickName" OnClick="BtnCambiarNicknameConfirmar" CssClass="Campo2" />--%>
                </div>
                <br>
                <%--<div class="separador">
                    <label>Contraseña:</label>
                    <br>
                    <asp:TextBox ID="txtcontrasenia" CssClass="Campo" runat="server" ReadOnly="false"></asp:TextBox>
                    <asp:Button ID="BtnContrasenia" runat="server" Text="Confirmar Cambio de Contraseña" OnClick="BtnCambiarContraseniaConfirmar" CssClass="Campo2" />
                </div>
                <br>--%>
            </div>
            <div class="Contenedor2">
                <div class="ninjadiv">
                    <asp:HyperLink ID="hlEditUbicaciones" runat="server" NavigateUrl="~/Pages/MisDatos_Persona_EditarUbicaciones.aspx">Administrar ubicaciones</asp:HyperLink>
                </div>
                <div>
                    <asp:ScriptManager ID="ScriptManagerAlpha" runat="server"></asp:ScriptManager>
                    <map:GoogleMap ID="MapUbicaciones"
                        runat="server"
                        Latitude="-34.9000015" Longitude="-54.9500008"
                        Zoom="16"
                        MapType="Roadmap">
                    </map:GoogleMap>
                    <map:GoogleMarkers ID="MapMarcadores"
                        TargetControlID="MapUbicaciones"
                        runat="server">
                    </map:GoogleMarkers>
                </div>
            </div>
            <div style="position: initial; margin-top: 20px; margin-bottom: 20px; margin-left: 30%;">
                <asp:Button ID="BtnModificarDatosId" runat="server" Text="Modificar Datos" OnClick="BtnModificarDatos" CssClass="Campo2" />
                <asp:Button ID="btnAtras" runat="server" Text="Menu Principal" OnClick="Atras" CssClass="Atras" />

            </div>
        </div>
    </form>
</body>
</html>
