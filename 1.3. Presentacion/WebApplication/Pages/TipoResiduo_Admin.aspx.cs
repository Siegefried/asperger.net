﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class TipoResiduo_Admin : System.Web.UI.Page
    {
        private int x = 0;
        private DataTable table = new DataTable("Tabla1");
        protected void Page_Load(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            List<DTTipoResiduo> ListaTipoResiduos;
            ListaTipoResiduos = new ControllerLogIn().TraerDatosTiposDeBasura();
            table.Columns.Add(new DataColumn("Id", typeof(int)));
            table.Columns.Add(new DataColumn("Nombre", typeof(string)));
            table.Columns.Add(new DataColumn("Color", typeof(string)));
            table.Columns.Add(new DataColumn("Bloqueado", typeof(string)));
            foreach (DTTipoResiduo Index in ListaTipoResiduos)
            {
                DataRow row = table.NewRow();
                row["Nombre"] = Index.nombre;
                row["Color"] = Index.color; 
                row["Id"] = Index.id;
                if (Index.bloqueado)
                {
                    row["Bloqueado"] = "Si";
                }
                else
                {
                    row["Bloqueado"] = "No";
                }
                table.Rows.Add(row);
            }
            GridView1.DataSource = table;
            GridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //funcion que bloquea un tipo de residuo en base al id
            if (Page.IsValid)
            {
                long idTipoResiduo = long.Parse(TextBox6.Text);
                new ControllerLogIn().BloquearDesbloquearTipoResiduo(idTipoResiduo);
                Response.Redirect("/Pages/TipoResiduo_Admin.aspx");
            }
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                //funcion que desbloquea un tipo de residuo en base al id
                long idTipoResiduo = long.Parse(TextBox4.Text);
                new ControllerLogIn().BloquearDesbloquearTipoResiduo(idTipoResiduo);
                Response.Redirect("/Pages/TipoResiduo_Admin.aspx");
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                //Cambio a la view para Editar Tipo de Residuo
                List<DTTipoResiduo> ListaTipoResiduos;
                ListaTipoResiduos = new ControllerLogIn().TraerDatosTiposDeBasura();
                long idTipoResiduo = long.Parse(TextBox1.Text);
                foreach (DTTipoResiduo Index in ListaTipoResiduos)
                {
                    if (Index.id == idTipoResiduo)
                    {
                        TextBox3.Text = Index.nombre;
                        color2.Value = '#' + Index.color;
                    }
                }
                MultiView1.ActiveViewIndex = 2;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Cambio a la view para Crear Tipo de Residuo
            MultiView1.ActiveViewIndex = 1;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (new ControllerLogIn().ExisteTipoResiduo(TextBox2.Text))
            {
                Label3.Visible = true;
                MultiView1.ActiveViewIndex = 1;
            }
            else
            {
                //funcion que edita un tipo de residuo nuevo
                string color = color1.Value;
                string colorsinhastag = "";
                colorsinhastag = color.Substring(1);
                new ControllerLogIn().AgregarTipoDeBasura(TextBox2.Text, colorsinhastag);
                Response.Redirect("/Pages/TipoResiduo_Admin.aspx");
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            
                //funcion que agrega un tipo de residuo en base al id
                string color = color2.Value;
                string colorsinhastag = "";
                colorsinhastag = color.Substring(1);
                long idTipoResiduo = long.Parse(TextBox1.Text);
                new ControllerLogIn().EditarTipoResiduo(idTipoResiduo, TextBox3.Text, colorsinhastag);
                Response.Redirect("/Pages/TipoResiduo_Admin.aspx");
            
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            long idTipoResiduo = long.Parse(TextBox6.Text);
            if (new ControllerLogIn().isTipoResiduo(idTipoResiduo))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            long idTipoResiduo = long.Parse(TextBox4.Text);
            if (new ControllerLogIn().isTipoResiduo(idTipoResiduo))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args)
        {
            long idTipoResiduo = long.Parse(TextBox1.Text);
            if (new ControllerLogIn().isTipoResiduo(idTipoResiduo))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Atras(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Admin.aspx");
        }

        protected void View_Anterior(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void MergeGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                List<DTTipoResiduo> ListaTipoResiduos;
                ListaTipoResiduos = new ControllerLogIn().TraerDatosTiposDeBasura();
                int y = ListaTipoResiduos.Count;
                if(x != y)
                {
                    System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#" + ListaTipoResiduos[x].color);
                    e.Row.Cells[2].BackColor = col;
                    x++;
                }
            }
        }
    }
}