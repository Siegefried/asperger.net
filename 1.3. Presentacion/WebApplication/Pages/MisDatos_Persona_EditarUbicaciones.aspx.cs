﻿using DataTransferObjects.Entities;
using GoogleMaps;
using GoogleMaps.Markers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Controllers;

namespace WebApplication.Pages
{
    public partial class MisDatos_Persona_EditarUbicaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["LoggedUser"] = new ControllerLogIn().logIn("JeanAlgo", "123456");
            MapUbicaciones.MapType = MapType.Roadmap;
            MapMarcadores.MarkerOptions.Draggable = false;
            if (!IsPostBack)
            {
                if(new ControllerLogIn().isPersona(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    btnRegresar2.Visible = true;
                    btnRegresar2.Enabled = true;
                }
                Session["currentIndex"] = -1;
                if(new ControllerLogIn().isReciclador(((DTLogIn)Session["LoggedUser"]).nickname))
                {
                    Session["ubicaciones"] = new ControllerLogIn().getUbicacionReciclador(((DTLogIn)Session["LoggedUser"]).id);
                    btnRegresar.Enabled = true;
                    btnRegresar.Visible = true;
                }
                else
                {
                    Session["ubicaciones"] = new ControllerLogIn().getUbicacionesPersona(((DTLogIn)Session["LoggedUser"]).id);
                }
                MapMarcadores.Markers = new List<Marker>();
                foreach (Ubicacion Index in TraerUbicacionesViewState())
                {
                    MapMarcadores.Add(new Marker
                    {
                        Position = new LatLng(Index.Latitud, Index.Longitud),
                        Info = "Alias: " + Index.Alias,
                        Draggable = false
                    });
                }
                if (TraerUbicacionesViewState().Count() > 0)
                {
                    btnRemover.Enabled = true;
                }
            }
            MapMarcadores.MarkerOptions.Draggable = true;
        }

        protected void MapMarcadores_Click(object sender, MarkerEventArgs e)
        {
            MapMarcadores.MarkerOptions.Draggable = false;
            Session["currentIndex"] = (int)e.Index;
            PanelM.Visible = true;
            btnRemover.Visible = true;
            lblAlias.Text = "Alias: " + TraerUbicacionesViewState()[TraercurrentIndex()].Alias;
        }

        protected void btnRemover_Click(object sender, EventArgs e)
        {
            if (TraercurrentIndex() != -1)
            {
                new ControllerLogIn().BorrarBajaUbicacion(TraerUbicacionesViewState()[TraercurrentIndex()].Id);
                Session["currentIndex"] = -1;
                Response.Redirect("MisDatos_Persona.aspx");
            }
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_Reciclador.aspx");
        }

        protected void btnRegresar2_Click(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_Persona.aspx");
        }

        protected void Nueva_Ubicacion(object sender, EventArgs e)
        {
            Response.Redirect("MisDatos_Persona_EditarUbicaciones_NuevaUbicacion.aspx");
        }

        protected List<Ubicacion> TraerUbicacionesViewState()
        {
            return (List<Ubicacion>)Session["ubicaciones"];
        }

        protected int TraercurrentIndex()
        {
            return (int)Session["currentIndex"];
        }

    }
}