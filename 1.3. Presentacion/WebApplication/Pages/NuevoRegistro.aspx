﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoRegistro.aspx.cs" Inherits="WebApplication.Pages.NuevoRegistro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Estilos/NuevoRegistro.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form runat="server">
        <div>
            <label>Usuario</label>
            <asp:CustomValidator class="validacion" ID="cvUser"
                                runat="server"
                                ErrorMessage="Ya esta en uso este usuario"
                                OnServerValidate="cvUser_ServerValidate"
                                Display="Dynamic" ForeColor="Red" ControlToValidate="usuario"></asp:CustomValidator>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo requerido" ControlToValidate="usuario" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller" ></asp:RequiredFieldValidator>
            <asp:TextBox class="textbox" ID="usuario" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>Contraseña</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo requerido" ControlToValidate="contrasenia" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:TextBox class="textbox" ID="contrasenia" runat="server" TextMode="Password"></asp:TextBox>
        </div>
        <div>
            <label>Confirmar Contraseña</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Campo requerido" ControlToValidate="confirmarcontrasenia" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:CompareValidator class="validacion" ID="CompareValidator1" runat="server" ControlToCompare="contrasenia" ControlToValidate="confirmarcontrasenia" ErrorMessage="Este campo debe ser igual a la contraseña" ForeColor="Red" Display="Dynamic"></asp:CompareValidator>
            <asp:TextBox class="textbox" ID="confirmarcontrasenia" runat="server" TextMode="Password"></asp:TextBox>
        </div>
        <div>
            <label>Tipo de Usuario</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator8" runat="server" ErrorMessage="Debe elegir una opcion" ControlToValidate="rbTipoUsuario" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
        </div>
        <br />
        <br />
        <div>
            <asp:RadioButtonList ID="rbTipoUsuario" runat="server" Align="center">
                <asp:ListItem>Recolector</asp:ListItem>
                <asp:ListItem>Persona</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div>
            <label>Nombre</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo requerido" ControlToValidate="nombre" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:TextBox class="textbox" ID="nombre" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>Apellido</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Campo requerido" ControlToValidate="apellido" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:TextBox class="textbox" ID="apellido" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>Email</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator6" runat="server" ErrorMessage="Campo requerido" ControlToValidate="email" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator class="validacion" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato de mail no valido" ControlToValidate="email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:CustomValidator class="validacion" ID="vtMailUso"
                                runat="server"
                                ControlToValidate="email"
                                ErrorMessage="Ya esta en uso este mail"
                                Display="Dynamic" ForeColor="Red"
                                OnServerValidate="vtMailUso_ServerValidate"></asp:CustomValidator>
            <asp:TextBox class="textbox" ID="email" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>Edad</label>
            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator7" runat="server" ErrorMessage="Campo requerido" ControlToValidate="edad" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>
            <asp:TextBox class="textbox" ID="edad" runat="server"></asp:TextBox>
        </div>
        <div>
            <label>Imagen</label>
<%--            <asp:RequiredFieldValidator class="validacion" ID="RequiredFieldValidator9" runat="server" ErrorMessage="Imagen requerida" ControlToValidate="ImagenUsuario" Display="Dynamic" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:RequiredFieldValidator>--%>
        </div>
        <br />
        <br />
        <div>
            <asp:FileUpload ID="ImagenUsuario" runat="server" />
        </div>
        <div class="validacion"><asp:CustomValidator ID="cvImagen" runat="server" ErrorMessage="Formato de imagen no valido. (.JPG, .JPEG, .BMP y .PNG)" Display="Dynamic" ControlToValidate="ImagenUsuario" OnServerValidate="cvImagen_ServerValidate" ForeColor="Red" BorderColor="Black" Font-Names="Verdana" Font-Size="Smaller"></asp:CustomValidator></div>
        <asp:Button class="BotonRegistrar" ID="Registrar" runat="server" Text="Registrar" OnClick="Registrar_Click" />
        <hr>
        <asp:Button CssClass="btnGoogle" ID="btnGoogle" runat="server" Text="Registrar con Google" data-provider="google" onclick="RedirectToLogin_Click" CausesValidation="false" style="text-align: center" />
        <hr>
        <asp:Button CssClass="btnGoogle" ID="btnAtras" runat="server" Text="Regresar al menu principal" onclick="Atras" CausesValidation="false" style="text-align: center" />
    </form>
</body>
</html>
