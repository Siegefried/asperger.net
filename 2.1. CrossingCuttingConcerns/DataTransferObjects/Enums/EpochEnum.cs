﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Enums
{
    public enum Epoch
    {
        Año = Mes * 12,
        Trimestre = Mes * 3,
        Mes = Semana * 4 +2,
        Semana = Dia * 7,
        Dia = 1
    }

    /*class Epoch   Enum   trimonth, month, week, day
    {
    }*/
}
