﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Enums
{
    public enum Valoracion
    {
        [Description("Sin valoración")]
        porValorar = 0,
        [Description("Pesimo")]
        pesimo = 1,
        [Description("Mediocre")]
        mediocre = 2,
        [Description("Normal")]
        normal = 3,
        [Description("Bueno")]
        buena = 4,
        [Description("Excelente")]
        excelente = 5
    }
}
