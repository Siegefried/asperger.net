﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTContainer
    {
        public long Id { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public String Color { get; set; }
        public String TipoBasura { get; set; }
        public bool Lleno { get; set; }
    }
}
