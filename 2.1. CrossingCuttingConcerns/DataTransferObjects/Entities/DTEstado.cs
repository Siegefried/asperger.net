﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTEstado
    {
        public long Id { get; set; }
        public string Estado { get; set; }

        public override string ToString()
        {
            return Estado;
        }
    }
}
