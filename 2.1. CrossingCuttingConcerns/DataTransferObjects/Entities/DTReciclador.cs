﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTReciclador
    {
        public long Id { get; set; }
        public Ubicacion UbicacionPrincipal { get; set; }
        public DTLogIn usuario { get; set; }

        public override string ToString()
        {
            if (usuario != null)
            {
                return usuario.nombre + "" + usuario.apellido;
            }
            else
            {
                return "Id: " + Id + "NullObj";
            }
        }
    }
}
