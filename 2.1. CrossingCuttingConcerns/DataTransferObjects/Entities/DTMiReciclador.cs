﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTMiReciclador
    {
        public long IdReciclador { get; set; }
        public string NombreReciclador { get; set; }
        public byte[] ImagenReciclador { get; set; }
        public int CantidadReciclado { get; set; }
        public bool Bloqueado { get; set; }
    }
}