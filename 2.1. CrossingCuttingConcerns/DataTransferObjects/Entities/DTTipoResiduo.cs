﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTTipoResiduo
    {
        public string nombre { get; set; }
        public string color { get; set; }
        public long id { get; set; }
        public bool bloqueado { get; set; }
    }
}
