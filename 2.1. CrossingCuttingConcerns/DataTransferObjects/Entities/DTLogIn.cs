﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTLogIn
    {
        public long id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string email { get; set; }
        public string nickname { get; set; }
        public string password { get; set; }
        public int edad { get; set; }
        public bool bloqueado { get; set; }
        public override string ToString()
        {
            return string.Format("ID: {0} Nick: {1}", id, nickname);
        }
    }
}
