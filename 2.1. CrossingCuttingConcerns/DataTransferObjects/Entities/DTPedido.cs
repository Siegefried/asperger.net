﻿using DataTransferObjects.Entities;
using DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTPedido
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public bool Hora { get; set; }
        public String Persona { get; set; }
        public DTReciclador Reciclador { get; set; } 
        public Ubicacion Ubicacion { get; set; }
        public DTEstado Estado { get; set; }
        public string Observaciones { get; set; }
        public Valoracion Valoracion { get; set; }
        public List<DTMaterialCantidad> Materiales { get; set; }
    }
}
