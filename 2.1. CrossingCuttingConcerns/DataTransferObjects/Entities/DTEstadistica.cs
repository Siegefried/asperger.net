﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class DTEstadistica<T>
    {
        public T Entidad { get; set; }
        public List<DTEstadisticaPair> stats { get; set; }
        
    }

    public class DTEstadisticaPair
    {
        public string key { get; set; }
        public int value { get; set; }
    }
}
