﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects.Entities
{
    public class Ubicacion
    {
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string Alias { get; set; }
        public long Id { get; set; }

        public string getLatitudParameter()
        {
            return Latitud.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public string getLongitudParameter()
        {
            return Longitud.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }
        public override string ToString()
        {
            return Alias;
        }
    }
}
