﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceLogIn" in both code and config file together.
    [ServiceContract]
    public interface IServiceLogIn
    {
        [OperationContract]
        DTLogIn login(string user, string pass);

        [OperationContract]
        DTLogIn externalLogin(string email);

        [OperationContract]
        bool emailUserValidation(string email, string validationToken);

        [OperationContract]
        bool isAdmin(string nick);
        
        [OperationContract]
        bool isRecolector(string nick);

        [OperationContract]
        bool isPersona(string nick);

        [OperationContract]
        bool checkMail(string email);

        [OperationContract]
        void RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        [OperationContract]
        void RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        [OperationContract]
        bool checkUser(string nick);

        [OperationContract]
        Byte[] getUserImage(long Id);

        [OperationContract]
        List<Ubicacion> getUbicacionesPersona(long Id);

        [OperationContract]
        List<Ubicacion> getUbicacionReciclador(long Id);

        [OperationContract]
        List<DTPedido> getPedidosPersona(long Id);

        [OperationContract]
        List<DTPedido> getPedidosReciclador(long Id);

        [OperationContract]
        List<DTPedido> getAllPedidosReciclador(long Id);

        [OperationContract]
        void BorrarBajaUbicacion(long id);

        [OperationContract]
        void AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud);

        [OperationContract]
        void AddUbicacionReciclador(long idPersona, string Alias, double Latitud, double Longitud);

        [OperationContract]
        void CambiarFotoPerfilPersona(Byte[] InfoImagen, long id);

        [OperationContract]
        void CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        [OperationContract]
        List<DTContainer> getContainersNoLlenos();

        [OperationContract]
        void modifyContainerLleno(long IdC, long IdP);

        [OperationContract]
        bool CheckTipoDeBasura(string NombreTipoDeBasura);

        [OperationContract]
        void AgregarTipoDeBasura(string Nombre, string Color);

        [OperationContract]
        List<string> TraeTiposBasuraString();

        [OperationContract]
        void AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos);

        [OperationContract]
        void DoWork();

        [OperationContract]
        void updatePedidosResumen(DTPedido pedido);

        [OperationContract]
        List<DTMiReciclador> TraerMisRecicladores(long idPersona);

        [OperationContract]
        void BloquearReciclador(long idPersona, long idReciclador);

        [OperationContract]
        void DesbloquearReciclador(long idPersona, long idReciclador);

        [OperationContract]
        List<string> TraerListaUsuarios();

        [OperationContract]
        DTLogIn GetDatosPersona(string nickname);

        [OperationContract]
        List<DTContainer> getContainersAll();

        [OperationContract]
        void modifyContainerLlenoAdmin(long IdC);

        [OperationContract]
        List<DTMaterialCantidad> TraerInfoMaterialCantidad(long IdPedido);

        [OperationContract]
        void EliminarContainer(long IdC);

        [OperationContract]
        void AddContainer(long IdTB, double Latitud, double Longitud);

        [OperationContract]
        void BloquearDesbloquearUsuario(long Id);

        [OperationContract]
        List<DTTipoResiduo> TraerDatosTiposDeBasura();

        [OperationContract]
        void BloquearDesbloquearTipoResiduo(long Id);

        [OperationContract]
        void EditarTipoResiduo(long Id, string Nombre, string Color);

        [OperationContract]
        bool isUsurio(long Id);

        [OperationContract]
        bool isTipoResiduo(long Id);

        [OperationContract]
        bool ExisteTipoResiduo(string Nombre);

        [OperationContract]
        List<DTContainer> getContainersLlenos();

        [OperationContract]
        List<DTEstadistica<DTLogIn>> rankPersonas(int daysBefore);

        [OperationContract]
        List<DTEstadistica<DTLogIn>> rankRecicladores(int daysBefore);

        [OperationContract]
        List<DTEstadistica<DTTipoResiduo>> tiposResiduosMasReciclados(int daysBefore);

        [OperationContract]
        Ubicacion getUbicacionPrincipalRecicladores(long IdR);

        [OperationContract]
        void modifyUbicacionPrincipalRecicladores(long IdR, double Latitud, double Longitud);

        [OperationContract]
        List<DTPedido> TraerPedidosDisponiblesRecicladorFiltro(long IdR, long IdTR, bool Horario, DateTime Fecha);
        
        [OperationContract]
        void LockPedido(long IdR, long IdP);

        [OperationContract]
        void unLockPedido(long IdP);

        [OperationContract]
        List<Ubicacion> getRUTA(long IdR, List<Ubicacion> Pasos);

        [OperationContract]
        List<DTPedido> TraerPedidosReciclador(long IdR, bool Horario, DateTime Fecha);

        [OperationContract]
        void RecolectPedido(long IdP);

        [OperationContract]
        List<DTPedido> TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha);
    }
}
