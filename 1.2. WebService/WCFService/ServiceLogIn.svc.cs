﻿using BusinessLogic.Controllers;
using BusinessLogic.Interfaces;
using DataAccesClassLibrary;
using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Procesamiento;

namespace WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceLogIn" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceLogIn.svc or ServiceLogIn.svc.cs at the Solution Explorer and start debugging.
    public class ServiceLogIn : IServiceLogIn
    {
        public void DoWork()
        {
        }

        DTLogIn IServiceLogIn.login(string user, string pass)
        {
            IControllerUsuarios ctrllr = new ControllerUsuarios();

            return ctrllr.userLogIn(user, pass);
        }

        DTLogIn IServiceLogIn.externalLogin(string email)
        {
            IControllerUsuarios ctrllr = new ControllerUsuarios();
            return ctrllr.userExternalLogIn(email);
        }

        bool IServiceLogIn.emailUserValidation(string email, string validationToken)
        {
            IControllerUsuarios ctrllr = new ControllerUsuarios();
            return ctrllr.emailUserValidation(email, validationToken);

        }

        bool IServiceLogIn.isAdmin(string nick)
        {
            return new ControllerUsuarios().isAdmin(nick);
        }

        bool IServiceLogIn.isPersona(string nick)
        {
            return new ControllerUsuarios().isPersona(nick);
        }

        bool IServiceLogIn.isRecolector(string nick)
        {
            return new ControllerUsuarios().isRecolector(nick);
        }

        bool IServiceLogIn.checkMail(string email)
        {
            return new ControllerUsuarios().checkMail(email);
        }

        void IServiceLogIn.RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ControllerUsuarios().RegistrarPersona(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        void IServiceLogIn.RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ControllerUsuarios().RegistrarReciclador(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        bool IServiceLogIn.checkUser(string nick)
        {
            return new ControllerUsuarios().checkUser(nick);
        }

        Byte[] IServiceLogIn.getUserImage(long Id)
        {
            return new ControllerUsuarios().getUserImage(Id);
        }

        List<Ubicacion> IServiceLogIn.getUbicacionesPersona(long Id)
        {
            return new ControllerUsuarios().getUbicacionesPersona(Id);
        }

        List<Ubicacion> IServiceLogIn.getUbicacionReciclador(long Id)
        {
            return new ControllerUsuarios().getUbicacionReciclador(Id);
        }

        List<DTPedido> IServiceLogIn.getPedidosPersona(long Id)
        {
            return new ControllerUsuarios().getPedidosPersona(Id);
        }

        List<DTPedido> IServiceLogIn.getPedidosReciclador(long Id)
        {
            return new ControllerUsuarios().getPedidosReciclador(Id);
        }

        List<DTPedido> IServiceLogIn.getAllPedidosReciclador(long Id)
        {
            return new ControllerUsuarios().getAllPedidosReciclador(Id);
        }

        void IServiceLogIn.BorrarBajaUbicacion(long id)
        {
            new ControllerUsuarios().BorrarBajaUbicacion(id);
        }

        void IServiceLogIn.AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new ControllerUsuarios().AddUbicacionPersona(idPersona, Alias, Latitud, Longitud);
        }

        void IServiceLogIn.AddUbicacionReciclador(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new ControllerUsuarios().AddUbicacionReciclador(idPersona, Alias, Latitud, Longitud);
        }

        void IServiceLogIn.CambiarFotoPerfilPersona(Byte[] InfoImagen, long id)
        {
            new ControllerUsuarios().CambiarFotoPerfilPersona(InfoImagen, id);
        }

        void IServiceLogIn.CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new ControllerUsuarios().CambiarDatosUsuario(id, Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        List<DTContainer> IServiceLogIn.getContainersNoLlenos()
        {
            return new ControllerUsuarios().getContainersNoLlenos();
        }

        void IServiceLogIn.modifyContainerLleno(long IdC, long IdP)
        {
            new ControllerUsuarios().modifyContainerLleno(IdC, IdP);
        }

        bool IServiceLogIn.CheckTipoDeBasura(string NombreTipoDeBasura)
        {
            return new ControllerUsuarios().CheckTipoDeBasura(NombreTipoDeBasura);
        }

        void IServiceLogIn.AgregarTipoDeBasura(string Nombre, string Color)
        {
            new ControllerUsuarios().AgregarTipoDeBasura(Nombre, Color);
        }

        List<string> IServiceLogIn.TraeTiposBasuraString()
        {
            return new ControllerUsuarios().TraeTiposBasuraString();
        }

        void IServiceLogIn.AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos)
        {
            new ControllerUsuarios().AgregarPedido(Fecha, Hora, IdPersona, IdUbicacion, Observaciones, Residuos);
        }

        void IServiceLogIn.updatePedidosResumen(DTPedido pedido)
        {
            new ControllerUsuarios().updatePedidosResumen(pedido);
        }

        List<DTMiReciclador> IServiceLogIn.TraerMisRecicladores(long idPersona)
        {
            return new ControllerUsuarios().TraerMisRecicladores(idPersona);
        }

        void IServiceLogIn.BloquearReciclador(long idPersona, long idReciclador)
        {
            new ControllerUsuarios().BloquearReciclador(idPersona, idReciclador);
        }

        void IServiceLogIn.DesbloquearReciclador(long idPersona, long idReciclador)
        {
            new ControllerUsuarios().DesbloquearReciclador(idPersona, idReciclador);
        }

        List<string> IServiceLogIn.TraerListaUsuarios()
        {
            return new ControllerUsuarios().TraerListaUsuarios();
        }

        DTLogIn IServiceLogIn.GetDatosPersona(string nickname)
        {
            return new ControllerUsuarios().GetDatosPersona(nickname);
        }

        List<DTContainer> IServiceLogIn.getContainersAll() {
            return new ControllerUsuarios().getContainersAll();
        }

        void IServiceLogIn.modifyContainerLlenoAdmin(long IdC) {
            new ControllerUsuarios().modifyContainerLlenoAdmin(IdC);
        }

        List<DTMaterialCantidad> IServiceLogIn.TraerInfoMaterialCantidad(long IdPedido)
        {
            return new ControllerUsuarios().TraerInfoMaterialCantidad(IdPedido);
        }
        
        void IServiceLogIn.EliminarContainer(long IdC)
        {
            new ControllerUsuarios().EliminarContainer(IdC);
        }

        void IServiceLogIn.AddContainer(long IdTB, double Latitud, double Longitud)
        {
            new ControllerUsuarios().AddContainer(IdTB, Latitud, Longitud);
        }

        void IServiceLogIn.BloquearDesbloquearUsuario(long Id)
        {
            new ControllerUsuarios().BloquearDesbloquearUsuario(Id);
        }

        List<DTTipoResiduo> IServiceLogIn.TraerDatosTiposDeBasura()
        {
            return new ControllerUsuarios().TraerDatosTiposDeBasura();
        }

        void IServiceLogIn.BloquearDesbloquearTipoResiduo(long Id)
        {
            new ControllerUsuarios().BloquearDesbloquearTipoResiduo(Id);
        }

        void IServiceLogIn.EditarTipoResiduo(long Id, string Nombre, string Color)
        {
            new ControllerUsuarios().EditarTipoResiduo(Id, Nombre, Color);
        }

        bool IServiceLogIn.isUsurio(long Id)
        {
            return new ControllerUsuarios().isUsurio(Id);
        }

        bool IServiceLogIn.isTipoResiduo(long Id)
        {
            return new ControllerUsuarios().isTipoResiduo(Id);
        }

        bool IServiceLogIn.ExisteTipoResiduo(string Nombre)
        {
            return new ControllerUsuarios().ExisteTipoResiduo(Nombre);
        }

        
        List<DTContainer> IServiceLogIn.getContainersLlenos()
        {
            return new ControllerUsuarios().getContainersLlenos();
        }

        List<DTEstadistica<DTLogIn>> IServiceLogIn.rankPersonas(int daysBefore)
        {
            return new ControllerUsuarios().rankPersonas(daysBefore);
        }

        List<DTEstadistica<DTLogIn>> IServiceLogIn.rankRecicladores(int daysBefore)
        {
            return new ControllerUsuarios().rankRecicladores(daysBefore);
        }

        List<DTEstadistica<DTTipoResiduo>> IServiceLogIn.tiposResiduosMasReciclados(int daysBefore)
        {
            return new ControllerUsuarios().tiposResiduosMasReciclados(daysBefore);
        }

        Ubicacion IServiceLogIn.getUbicacionPrincipalRecicladores(long IdR)
        {
            return new ControllerUsuarios().getUbicacionPrincipalRecicladores(IdR);
        }

        void IServiceLogIn.modifyUbicacionPrincipalRecicladores(long IdR, double Latitud, double Longitud)
        {
            new ControllerUsuarios().modifyUbicacionPrincipalRecicladores(IdR, Latitud, Longitud);
        }

        List<DTPedido> IServiceLogIn.TraerPedidosDisponiblesRecicladorFiltro(long IdR, long IdTR, bool Horario, DateTime Fecha)
        {
            return new ControllerUsuarios().TraerPedidosDisponiblesRecicladorFiltro(IdR, IdTR, Horario, Fecha);
        }

        void IServiceLogIn.LockPedido(long IdR, long IdP)
        {
            new ControllerUsuarios().LockPedido(IdR, IdP);
        }

        void IServiceLogIn.unLockPedido(long IdP)
        {
            new ControllerUsuarios().unLockPedido(IdP);
        }

        List<Ubicacion> IServiceLogIn.getRUTA(long IdR, List<Ubicacion> Pasos) {
            return new XMLRuta().getRUTA(IdR, Pasos);
        }

        List<DTPedido> IServiceLogIn.TraerPedidosReciclador(long IdR, bool Horario, DateTime Fecha) {
            return new ControllerUsuarios().TraerPedidosReciclador(IdR, Horario, Fecha);
        }

        void IServiceLogIn.RecolectPedido(long IdP)
        {
            new ControllerUsuarios().RecolectPedido(IdP);
        }

        List<DTPedido> IServiceLogIn.TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha)
        {
            return new ControllerUsuarios().TraerPedidosDisponiblesRecicladorMultiFiltro(IdR, ltTipos, Horario, Fecha);
        }
    }
}
