﻿using BusinessLogic.Controllers;
using DataTransferObjects.Entities;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;
using WCFService;

namespace WCF.Procesamiento
{
    public class XMLRuta
    {
        public List<Ubicacion> getRUTA(long IdR, List<Ubicacion> Pasos)
        {
            Ubicacion Home = new ControllerUsuarios().getUbicacionPrincipalRecicladores(IdR);
            List<int> Resultado = new List<int>();
            string url = "https://maps.googleapis.com/maps/api/directions/xml?origin="+ Home.Latitud.ToString(System.Globalization.CultureInfo.InvariantCulture) +"," + Home.Longitud.ToString(System.Globalization.CultureInfo.InvariantCulture) + "&destination=" + Home.Latitud.ToString(System.Globalization.CultureInfo.InvariantCulture) + "," + Home.Longitud.ToString(System.Globalization.CultureInfo.InvariantCulture) + "&waypoints=optimize:true";
            foreach (Ubicacion Temporal in Pasos) {
                url = url + "|"+Temporal.Latitud.ToString(System.Globalization.CultureInfo.InvariantCulture) + ","+Temporal.Longitud.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
            url = url + "&key=AIzaSyC-g7KJQ3rbKaXjkOyWh6JsnOYohKKMlg0";
            var request = WebRequest.Create(url) as HttpWebRequest;
            var response = request.GetResponse();

            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            var result = readStream.ReadToEnd();
            XDocument xml = XDocument.Parse(result);
            var rslt = xml.Descendants("waypoint_index");
            foreach (XElement punto in rslt)
            {
                Resultado.Add(int.Parse(punto.Value));
            }
            List<Ubicacion> Res = new List<Ubicacion>();
            Res.Add(Home);
            foreach (int Index in Resultado) {
                Res.Add(Pasos[Index]);
            }
            Res.Add(Home);
            return Res;
        }
    }

    
}