#Asperger Punto Red
------
##Herramientas Primarias
* Visual Studio Community Edition 2015
	* [Sauce](https://stackoverflow.com/questions/44290672/how-to-download-visual-studio-community-edition-2015-not-2017)
	* [Web Installer](https://go.microsoft.com/fwlink/?LinkId=532606&clcid=0x409)
	* [ISO (7,1 GB)](https://go.microsoft.com/fwlink/?LinkId=615448&clcid=0x409)	
* Microsoft� SQL Server� 2016 Service Pack 2 Express (ENG)
	* [Sauce](https://stackoverflow.com/questions/39835986/sql-server-2016-express-full-download)
	* [Core (437 MB)](https://download.microsoft.com/download/4/1/A/41AD6EDE-9794-44E3-B3D5-A1AF62CD7A6F/sql16_sp2_dlc/en-us/SQLEXPR_x64_ENU.exe)
	* [Web Installer (5,6 MB)](https://www.microsoft.com/en-us/download/confirmation.aspx?id=56840)
* SQL Server Management Studio (524 MB)
	* [Sauce](https://en.wikipedia.org/wiki/SQL_Server_Management_Studio)
	* [Download Page](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017)
	* [SSMS 18.0 Direct Link ](https://go.microsoft.com/fwlink/?linkid=2088649)
		* Release number: 18.0 (GA), Build number: 15.0.18118.0, Release date: April 24, 2019.
##Herramientas Secundarias
* Mark Down
	* [Mark Down CheatSheet Bitbucket](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	* [Mark Down CheatSheet GitHub](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* GIT Client
	* GitKraken
		* [Home Page](https://www.gitkraken.com/)
		* [Win64](https://www.gitkraken.com/download/windows64)
* Trello
	* [HomePage](https://trello.com)
	* [Panel Clase/Profe](https://trello.com/b/W3DCQdQp/recicling-web)
	* [Team Nosotros](https://trello.com/aspergerpuntored/home)
* Bitbucket - Discord  Integration
	* Skyhook
		* [Home Page](https://skyhook.glitch.me/)
		* [Our Hook to BitBucket](https://skyhook.glitch.me/api/webhooks/576476689077501983/SZS4v1uZkNAEdykq2X1WTJeH5OJYN42VwhTbwy4KSkpv8TivgqdOrsPncn60yWHXtBb-/bitbucket)
		* [Sauce](https://github.com/Commit451/skyhook)
		