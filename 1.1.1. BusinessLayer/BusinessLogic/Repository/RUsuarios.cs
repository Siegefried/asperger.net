﻿using DataAccesClassLibrary;
using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Repository
{
    class RUsuarios
    {

        private ReciclatorvmEntities generateContext()
        {
            return RContextGenerator.generateContext();
        }

        public USUARIO GetUsuarioByNick(string nick)
        {
            try
            {
                USUARIO emp = null;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.USUARIOS.FirstOrDefault(s => s.Nickname == nick);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public USUARIO GetUsuarioByEmail(string email)
        {
            try
            {
                USUARIO emp = null;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.USUARIOS.FirstOrDefault(s => s.Mail == email);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool isReciclador(string nick)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.RECICLADORES.Any(s => s.Id == (context.USUARIOS.Where(x => x.Nickname == nick).Select(xx => xx.Id).FirstOrDefault()));
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool isAdmin(string nick)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.ADMINISTRADORES.Any(s => s.Id == (context.USUARIOS.Where(x => x.Nickname == nick).Select(xx => xx.Id).FirstOrDefault()));
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool isPersona(string nick)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.PERSONAS.Any(s => s.Id == (context.USUARIOS.Where(x => x.Nickname == nick).Select(xx => xx.Id).FirstOrDefault()));
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool checkMail(string email)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.USUARIOS.Any(s => s.Mail == email);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen, String mailValidation)
        {
            USUARIO Nuevo = new USUARIO();
            Nuevo.Mail = Mail;
            Nuevo.Nickname = User;
            Nuevo.Password = Pass;
            Nuevo.Edad = Edad;
            Nuevo.Nombre = Nombre;
            Nuevo.Apellido = Apellido;
            Nuevo.mailValidation = mailValidation;
            Nuevo.Bloqueado = false;
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.USUARIOS.Add(Nuevo);
                    PERSONA Nueva = new PERSONA();
                    Nueva.Id = Nuevo.Id;
                    context.PERSONAS.Add(Nueva);
                    IMAGENE TemporalImage = new IMAGENE();
                    TemporalImage.Id = Nuevo.Id;
                    TemporalImage.Data = InfoImagen;
                    context.IMAGENES.Add(TemporalImage);
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen, String mailValidation)
        {
            USUARIO Nuevo = new USUARIO();
            Nuevo.Mail = Mail;
            Nuevo.Nickname = User;
            Nuevo.Password = Pass;
            Nuevo.Edad = Edad;
            Nuevo.Nombre = Nombre;
            Nuevo.Apellido = Apellido;
            Nuevo.mailValidation = mailValidation;
            Nuevo.Bloqueado = false;

            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.USUARIOS.Add(Nuevo);
                    RECICLADORE Nueva = new RECICLADORE();
                    Nueva.Id = Nuevo.Id;
                    context.RECICLADORES.Add(Nueva);
                    IMAGENE TemporalImage = new IMAGENE();
                    TemporalImage.Id = Nuevo.Id;
                    TemporalImage.Data = InfoImagen;
                    context.IMAGENES.Add(TemporalImage);
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool checkUser(string nick)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.USUARIOS.Any(s => s.Nickname == nick);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Byte[] getUserImage(long Id)
        {
            try
            {
                Byte[] emp = null;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.IMAGENES.FirstOrDefault(s => s.Id == Id).Data;
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UBICACIONE> getUbicacionesPersona(long Id)
        {
            try
            {
                List<UBICACIONE> Res = new List<UBICACIONE>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    PERSONA Dueño = context.PERSONAS.Where(s => s.Id == Id).SingleOrDefault();
                    Res = Dueño.UBICACIONES.ToList();
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UBICACIONE> getUbicacionReciclador(long Id)
        {
            try
            {
                List<UBICACIONE> Res = new List<UBICACIONE>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    RECICLADORE Dueño = context.RECICLADORES.Where(s => s.Id == Id).SingleOrDefault();
                    if (Dueño.UbicacionPrincipal != null)
                    {
                        Res = context.UBICACIONES.Where(s => s.Id == Dueño.UbicacionPrincipal).ToList();
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BorrarBajaUbicacion(long id)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    UBICACIONE currentU = context.UBICACIONES.Where(s => s.Id == id).FirstOrDefault();
                    PERSONA currentP = currentU.PERSONAS.FirstOrDefault();
                    currentP.UBICACIONES.Remove(currentU);
                    if (!context.PEDIDOS.Any(s => s.IdUbicacion == currentU.Id))
                    {
                        context.UBICACIONES.Remove(currentU);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud)
        {
            UBICACIONE Nuevo = new UBICACIONE();
            Nuevo.Alias = Alias;
            Nuevo.Latitud = Latitud;
            Nuevo.Longitud = Longitud;
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    PERSONA currentP = context.PERSONAS.Where(s => s.Id == idPersona).SingleOrDefault();
                    context.UBICACIONES.Add(Nuevo);
                    currentP.UBICACIONES.Add(Nuevo);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddUbicacionReciclador(long idPersona, string Alias, double Latitud, double Longitud)
        {
            UBICACIONE Nuevo = new UBICACIONE();
            Nuevo.Alias = Alias;
            Nuevo.Latitud = Latitud;
            Nuevo.Longitud = Longitud;
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    RECICLADORE currentR = context.RECICLADORES.Where(s => s.Id == idPersona).SingleOrDefault();
                    context.UBICACIONES.Add(Nuevo);
                    currentR.UBICACIONE = Nuevo;
                    context.SaveChanges();
                }

                using (ReciclatorvmEntities context = generateContext())
                {
                    RECICLADORE currentR = context.RECICLADORES.Where(s => s.Id == idPersona).SingleOrDefault();
                    currentR.UbicacionPrincipal = context.UBICACIONES.Max(s => s.Id);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CambiarFotoPerfilPersona(Byte[] InfoImagen, long id)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    IMAGENE Nuevo = context.USUARIOS.FirstOrDefault(s => s.Id == id).IMAGENE;
                    Nuevo.Data = InfoImagen;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    USUARIO Nuevo = context.USUARIOS.FirstOrDefault(s => s.Id == id);
                    if (Nombre != null)
                        Nuevo.Nombre = Nombre;
                    if (Apellido != null)
                        Nuevo.Apellido = Apellido;
                    if (User != null)
                        Nuevo.Nickname = User;
                    if (Pass != null)
                        Nuevo.Password = Pass;
                    if (Edad != -1)
                        Nuevo.Edad = Edad;
                    if (Mail != null)
                        Nuevo.Mail = Mail;
                    if (InfoImagen != null && InfoImagen.Length != 0)
                        Nuevo.IMAGENE.Data = InfoImagen;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTContainer> getContainersNoLlenos()
        {
            try
            {
                List<CONTAINER> ContainersTemporal = new List<CONTAINER>();
                List<DTContainer> Res = new List<DTContainer>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    ContainersTemporal = context.CONTAINERS.Where(s => s.Lleno == false && s.Eliminado == false).ToList();
                    foreach (CONTAINER index in ContainersTemporal)
                    {
                        Res.Add(index.GetDTO());
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void modifyContainerLleno(long IdC, long IdP)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    CONTAINER ContainerTemporal = context.CONTAINERS.Where(s => s.Id == IdC).SingleOrDefault();
                    PERSONA PersonaTemporal = context.PERSONAS.Where(s => s.Id == IdP).SingleOrDefault();
                    ALERTA AlertaTemporal = new ALERTA
                    {
                        PERSONA = PersonaTemporal,
                        CONTAINER = ContainerTemporal,
                        FechaHora = DateTime.Now
                    };
                    ContainerTemporal.Lleno = true;
                    context.ALERTAS.Add(AlertaTemporal);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool mailValidation(string email, string token)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    USUARIO usr = context.USUARIOS.FirstOrDefault(s => s.Mail == email);
                    if (token != null)
                    {
                        if (usr != null)
                        {
                            if (usr.mailValidation.Equals(token))
                            {
                                usr.mailValidation = "[VALID]";
                                context.SaveChanges();
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        public bool CheckTipoDeBasura(string NombreTipoDeBasura)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.TIPOS_DE_BASURAS.Any(s => s.Nombre == NombreTipoDeBasura);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AgregarTipoDeBasura(string Nombre, string Color)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    TIPOS_DE_BASURA Nuevo = new TIPOS_DE_BASURA();
                    Nuevo.Nombre = Nombre;
                    Nuevo.Color = Color;
                    Nuevo.Bloqueado = false;
                    context.TIPOS_DE_BASURAS.Add(Nuevo);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> TraeTiposBasuraString()
        {
            try
            {
                List<string> TiposBasura = new List<string>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    TiposBasura = context.TIPOS_DE_BASURAS.Where(s => s.Bloqueado == false).Select(s => s.Nombre).ToList();
                }
                return TiposBasura;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos)
        {
            try
            {
                PEDIDO Nuevo = new PEDIDO();
                using (ReciclatorvmEntities context = generateContext())
                {
                    Nuevo.Fecha = Fecha.Date;
                    Nuevo.Hora = Hora;
                    Nuevo.Observaciones = Observaciones;
                    context.PEDIDOS.Add(Nuevo);
                    Nuevo.IdUbicacion = IdUbicacion;
                    Nuevo.Estado = 0;
                    Nuevo.Valoracion = 0;
                    context.SaveChanges();

                    foreach (DTMaterialCantidad Index in Residuos)
                    {
                        if (Index.Cantidad != 0)
                        {
                            PEDIDOS_TIPOS_DE_BASURAS res = new PEDIDOS_TIPOS_DE_BASURAS();
                            res.IdPedido = Nuevo.Id;
                            res.Cantidad = Index.Cantidad;
                            res.IdTipoBasura = context.TIPOS_DE_BASURAS.SingleOrDefault(s => s.Nombre == Index.Nombre).Id;
                            context.PEDIDOS_TIPOS_DE_BASURAS.Add(res);
                        }
                    }
                    Nuevo.IdPersona = IdPersona;
                    Nuevo.RECICLADORE = null;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTMiReciclador> TraerMisRecicladores(long idPersona)
        {
            try
            {
                List<DTMiReciclador> Res = new List<DTMiReciclador>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    //PERSONA TemporalPersona = context.PERSONAS.SingleOrDefault(s => s.Id == idPersona);
                    //List<long?> Temporal = context.PEDIDOS.Where(s => s.IdPersona == idPersona).Select(ss => ss.IdReciclador).Distinct().ToList();
                    foreach (long Index in context.PEDIDOS.Where(s => s.IdPersona == idPersona && s.ESTADO1.Id == 2).Select(ss => ss.IdReciclador).Distinct().ToList())
                    {
                        DTMiReciclador TemporalReciclador = new DTMiReciclador();
                        TemporalReciclador.NombreReciclador = context.RECICLADORES.Where(r => r.Id == Index).Select(rr => rr.USUARIO.Nombre).SingleOrDefault();
                        TemporalReciclador.IdReciclador = Index;
                        TemporalReciclador.ImagenReciclador = context.RECICLADORES.Where(r => r.Id == Index).Select(rr => rr.USUARIO.IMAGENE.Data).SingleOrDefault();
                        TemporalReciclador.Bloqueado = context.PERSONAS.SingleOrDefault(s => s.Id == idPersona).RECICLADORES.Any(rB => rB.Id == Index);
                        List<PEDIDO> TemporalPedidos = context.PEDIDOS.Where(s => s.IdPersona == idPersona && s.IdReciclador == Index && s.ESTADO1.Id == 2).ToList();
                        TemporalReciclador.CantidadReciclado = 0;
                        foreach (PEDIDO Index2 in TemporalPedidos)
                        {
                            TemporalReciclador.CantidadReciclado = TemporalReciclador.CantidadReciclado + Index2.PEDIDOS_TIPOS_DE_BASURAS.Sum(ptb => ptb.Cantidad);
                        }
                        //TemporalReciclador.CantidadReciclado = context.PEDIDOS_TIPOS_DE_BASURAS.Where(ptb => (context.PEDIDOS.Where(p => p.IdPersona == idPersona && p.IdReciclador == Index && p.ESTADO1.Id == 2).Select(pp => pp.Id).Distinct()).Contains(ptb.IdPedido)).Sum(ptb => ptb.Cantidad);
                        Res.Add(TemporalReciclador);
                    }
                    return Res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BloquearReciclador(long idPersona, long idReciclador)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    PERSONA TemporalPersona = context.PERSONAS.FirstOrDefault(s => s.Id == idPersona);
                    RECICLADORE TemporalReciclador = context.RECICLADORES.FirstOrDefault(s => s.Id == idReciclador);
                    TemporalPersona.RECICLADORES.Add(TemporalReciclador);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DesbloquearReciclador(long idPersona, long idReciclador)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    PERSONA TemporalPersona = context.PERSONAS.FirstOrDefault(s => s.Id == idPersona);
                    RECICLADORE TemporalReciclador = context.RECICLADORES.FirstOrDefault(s => s.Id == idReciclador);
                    TemporalPersona.RECICLADORES.Remove(TemporalReciclador);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> TraerListaUsuarios()
        {
            try
            {
                List<string> Usuarios = new List<string>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    Usuarios = context.USUARIOS.Select(s => s.Nickname).ToList();
                }
                return Usuarios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTLogIn GetDatosPersona(string nickname)
        {
            try
            {
                USUARIO datostemp = null;
                DTLogIn datos = new DTLogIn();
                using (ReciclatorvmEntities context = generateContext())
                {
                    datostemp = context.USUARIOS.Where(s => s.Nickname == nickname).SingleOrDefault();
                    if (datostemp != null)
                    {
                        datos.id = datostemp.Id;
                        datos.nickname = datostemp.Nickname;
                        datos.password = datostemp.Password;
                        datos.nombre = datostemp.Nombre;
                        datos.apellido = datostemp.Apellido;
                        datos.edad = datostemp.Edad;
                        datos.email = datostemp.Mail;
                        datos.bloqueado = datostemp.Bloqueado;
                        return datos;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTContainer> getContainersAll()
        {
            try
            {
                List<CONTAINER> ContainersTemporal = new List<CONTAINER>();
                List<DTContainer> Res = new List<DTContainer>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    ContainersTemporal = context.CONTAINERS.Where(s => s.Eliminado == false).ToList();
                    foreach (CONTAINER index in ContainersTemporal)
                    {
                        Res.Add(index.GetDTO());
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void modifyContainerLlenoAdmin(long IdC)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.CONTAINERS.Where(s => s.Id == IdC).SingleOrDefault().Lleno = false;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTMaterialCantidad> TraerInfoMaterialCantidad(long IdPedido)
        {
            try
            {
                List<PEDIDOS_TIPOS_DE_BASURAS> ListaPedidosTipoBasura = null;
                TIPOS_DE_BASURA TiposDeBasura = null;

                List<DTMaterialCantidad> DatosPedido = new List<DTMaterialCantidad>();
                int x = 0;
                using (ReciclatorvmEntities context = generateContext())
                {
                    ListaPedidosTipoBasura = context.PEDIDOS_TIPOS_DE_BASURAS.Where(s => s.IdPedido == IdPedido).ToList();
                }
                foreach (PEDIDOS_TIPOS_DE_BASURAS Index in ListaPedidosTipoBasura)
                {
                    DTMaterialCantidad TipoResiduo = new DTMaterialCantidad();
                    TipoResiduo.Cantidad = Index.Cantidad;
                    using (ReciclatorvmEntities context = generateContext())
                    {
                        TiposDeBasura = context.TIPOS_DE_BASURAS.Where(s => s.Id == Index.IdTipoBasura).FirstOrDefault();
                    }
                    TipoResiduo.Nombre = TiposDeBasura.Nombre;
                    DatosPedido.Insert(x, TipoResiduo);
                    x++;

                }
                return DatosPedido;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarContainer(long IdC)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    //context.CONTAINERS.Remove(context.CONTAINERS.SingleOrDefault(s => s.Id == IdC)); //Eliminar digievoluciona a!!!!!
                    context.CONTAINERS.SingleOrDefault(s => s.Id == IdC).Eliminado = true; //Deshabiladowskymon Uf, preciso dormir
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddContainer(long IdTB, double Latitud, double Longitud)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    UBICACIONE NewUbicacion = new UBICACIONE { Alias = "", Latitud = Latitud, Longitud = Longitud };
                    context.UBICACIONES.Add(NewUbicacion);
                    CONTAINER NewContainer = new CONTAINER
                    {
                        Lleno = false,
                        IdTipoBasura = IdTB,
                        UBICACIONE = NewUbicacion
                    };
                    context.CONTAINERS.Add(NewContainer);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BloquearDesbloquearUsuario(long Id)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    USUARIO TemporalUsuario = context.USUARIOS.FirstOrDefault(s => s.Id == Id);
                    if (TemporalUsuario.Bloqueado == false)
                    {
                        TemporalUsuario.Bloqueado = true;
                    }
                    else
                    {
                        TemporalUsuario.Bloqueado = false;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTTipoResiduo> TraerDatosTiposDeBasura()
        {
            try
            {
                List<DTTipoResiduo> ListaTiposBasura = new List<DTTipoResiduo>();
                List<TIPOS_DE_BASURA> TiposBasura = new List<TIPOS_DE_BASURA>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    TiposBasura = context.TIPOS_DE_BASURAS.Where(s => s.Id == s.Id).ToList();
                    foreach (TIPOS_DE_BASURA Index in TiposBasura)
                    {
                        ListaTiposBasura.Add(Index.GetDTO());
                    }
                }
                return ListaTiposBasura;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BloquearDesbloquearTipoResiduo(long Id)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    TIPOS_DE_BASURA TemporalTipoResiduo = context.TIPOS_DE_BASURAS.FirstOrDefault(s => s.Id == Id);
                    if (TemporalTipoResiduo.Bloqueado == false)
                    {
                        TemporalTipoResiduo.Bloqueado = true;
                    }
                    else
                    {
                        TemporalTipoResiduo.Bloqueado = false;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditarTipoResiduo(long Id, string Nombre, string Color)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    TIPOS_DE_BASURA TemporalTipoResiduo = context.TIPOS_DE_BASURAS.FirstOrDefault(s => s.Id == Id);
                    TemporalTipoResiduo.Nombre = Nombre;
                    TemporalTipoResiduo.Color = Color;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool isUsurio(long Id)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.USUARIOS.Any(s => s.Id == Id);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool isTipoResiduo(long Id)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.TIPOS_DE_BASURAS.Any(s => s.Id == Id);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExisteTipoResiduo(string Nombre)
        {
            try
            {
                bool emp = false;
                using (ReciclatorvmEntities context = generateContext())
                {
                    emp = context.TIPOS_DE_BASURAS.Any(s => s.Nombre == Nombre);
                }
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTContainer> getContainersLlenos()
        {
            //ToDo -> getCountainerConLlenado(true|false)
            try
            {
                List<CONTAINER> ContainersTemporal = new List<CONTAINER>();
                List<DTContainer> Res = new List<DTContainer>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    ContainersTemporal = context.CONTAINERS.Where(s => s.Lleno == true && s.Eliminado == false).ToList();
                    foreach (CONTAINER index in ContainersTemporal)
                    {
                        Res.Add(index.GetDTO());
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTEstadistica<DTLogIn>> rankPersonas(int daysBefore)
        {
            try
            {
                DateTime daysAgo = DateTime.Today.AddDays(daysBefore * -1);
                List<DTEstadistica<DTLogIn>> statsPersonasRet = new List<DTEstadistica<DTLogIn>>();
                List<DTEstadistica<PERSONA>> statsPersonas = new List<DTEstadistica<PERSONA>>();

                using (ReciclatorvmEntities context = generateContext())
                {
                    statsPersonas = context.PEDIDOS
                        .Where(x => x.Fecha >= daysAgo)
                        .GroupBy(info => info.IdPersona)
                        .Select(
                            group => new DTEstadistica<PERSONA>
                            {
                                Entidad = context.PERSONAS.Where(t => t.Id == group.Key.Value).FirstOrDefault(),
                                stats = new List<DTEstadisticaPair>()
                                {
                                    new DTEstadisticaPair() { key = "CantidadPedidos", value = group.Count() },
                                    new DTEstadisticaPair() { key = "CantidadKilos", value = group.Sum( x => x.PEDIDOS_TIPOS_DE_BASURAS.FirstOrDefault().Cantidad)}
                                }//stats = new List<DTEstadisticaPair>() END
                            }//group => new DTEstadistica<PERSONA> END
                        ).ToList();//.Select( END

                    // El GetDTO() moria dentro de la sentencia LinQ
                    foreach (DTEstadistica<PERSONA> u in statsPersonas)
                    {
                        DTEstadistica<DTLogIn> temp = new DTEstadistica<DTLogIn>();
                        temp.stats = new List<DTEstadisticaPair>();
                        temp.stats = u.stats;
                        temp.Entidad = u.Entidad.USUARIO.GetDTO();
                        statsPersonasRet.Add(temp);
                    }
                }//using END
                return statsPersonasRet; //.ToList();
            }// try END
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTEstadistica<DTLogIn>> rankRecicladores(int daysBefore)
        {
            try
            {
                DateTime daysAgo = DateTime.Today.AddDays(daysBefore * -1);
                List<DTEstadistica<DTLogIn>> statsRecicladoresRet = new List<DTEstadistica<DTLogIn>>();
                List<DTEstadistica<RECICLADORE>> statsRecicladores = new List<DTEstadistica<RECICLADORE>>();

                using (ReciclatorvmEntities context = generateContext())
                {
                    statsRecicladores = context.PEDIDOS
                        .Where(x => x.Fecha >= daysAgo && x.IdReciclador >0)
                        .GroupBy(info => info.IdReciclador)
                        .Select(
                            group => new DTEstadistica<RECICLADORE>
                            {
                                Entidad = context.RECICLADORES.Where(t => t.Id == group.Key.Value).FirstOrDefault(),
                                stats = new List<DTEstadisticaPair>()
                                {
                                    new DTEstadisticaPair() { key = "CantidadPedidos", value = group.Count() },
                                    new DTEstadisticaPair() { key = "CantidadKilos", value = group.Sum( x => x.PEDIDOS_TIPOS_DE_BASURAS.FirstOrDefault().Cantidad)},
                                    new DTEstadisticaPair() { key = "Calificacion", value = (int)group.Average( x => x.Valoracion).Value }
                                }//stats = new List<DTEstadisticaPair>() END
                            }//ggroup => new DTEstadistica<RECICLADORE> END
                        ).ToList();//.Select( END

                    // El GetDTO() moria dentro de la sentencia LinQ
                    foreach (DTEstadistica<RECICLADORE> u in statsRecicladores)
                    {
                        DTEstadistica<DTLogIn> temp = new DTEstadistica<DTLogIn>();
                        temp.stats = new List<DTEstadisticaPair>();
                        temp.stats = u.stats;
                        temp.Entidad = u.Entidad.USUARIO.GetDTO();
                        statsRecicladoresRet.Add(temp);
                    }
                }//using END
                return statsRecicladoresRet; //.ToList();
            }// try END
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTEstadistica<DTTipoResiduo>> tiposResiduosMasReciclados(int daysBefore)
        {
            try
            {
                DateTime daysAgo = DateTime.Today.AddDays(daysBefore * -1);
                List<DTEstadistica<DTTipoResiduo>> statsResiduoRet = new List<DTEstadistica<DTTipoResiduo>>();
                List<DTEstadistica<TIPOS_DE_BASURA>> statsResiduo = new List<DTEstadistica<TIPOS_DE_BASURA>>();

                using (ReciclatorvmEntities context = generateContext())
                {
                    statsResiduo = context.PEDIDOS_TIPOS_DE_BASURAS
                        .Where(x => x.PEDIDO.Fecha >= daysAgo)
                        .GroupBy(info => info.IdTipoBasura)
                        .Select(
                            group => new DTEstadistica<TIPOS_DE_BASURA>
                            {
                                Entidad = context.TIPOS_DE_BASURAS.Where(t => t.Id == group.Key).FirstOrDefault(),
                                stats = new List<DTEstadisticaPair>()
                                {
                                    new DTEstadisticaPair() { key = "Cantidad", value = group.Count() },
                                    new DTEstadisticaPair() { key = "CantidadKilos", value = group.Sum( x => x.Cantidad)}
                                }//stats = new List<DTEstadisticaPair>() END
                            }//group => new DTEstadistica<TIPOS_DE_BASURA> END
                        ).ToList();//.Select( END

                    // El GetDTO() moria dentro de la sentencia LinQ
                    foreach (DTEstadistica<TIPOS_DE_BASURA> u in statsResiduo)
                    {
                        DTEstadistica<DTTipoResiduo> temp = new DTEstadistica<DTTipoResiduo>();
                        temp.stats = new List<DTEstadisticaPair>();
                        temp.stats = u.stats;
                        temp.Entidad = u.Entidad.GetDTO();
                        statsResiduoRet.Add(temp);
                    }
                }//using END

                return statsResiduoRet; //.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Ubicacion getUbicacionPrincipalRecicladores(long IdR)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    if (context.RECICLADORES.SingleOrDefault(s => s.Id == IdR).UBICACIONE == null)
                    {
                        return null;
                    }
                    else
                    {
                        return context.RECICLADORES.SingleOrDefault(s => s.Id == IdR).UBICACIONE.GetDTO();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void modifyUbicacionPrincipalRecicladores(long IdR, double Latitud, double Longitud)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    RECICLADORE CurrentReciclador = context.RECICLADORES.SingleOrDefault(s => s.Id == IdR);
                    if (CurrentReciclador.UBICACIONE == null)
                    {
                        UBICACIONE NuevaUbicacion = new UBICACIONE
                        {
                            Alias = "",
                            Latitud = Latitud,
                            Longitud = Longitud
                        };
                        context.UBICACIONES.Add(NuevaUbicacion);
                        CurrentReciclador.UBICACIONE = NuevaUbicacion;
                    }
                    else
                    {
                        CurrentReciclador.UBICACIONE.Latitud = Latitud;
                        CurrentReciclador.UBICACIONE.Longitud = Longitud;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorFiltro(long IdR, long IdTR, bool Horario, DateTime Fecha)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    List<PEDIDO> SemiRes = new List<PEDIDO>();
                    if (IdTR == -1)
                    { // -1 es igual a no tomar el filtro
                        SemiRes = context.PEDIDOS.Where(S => !S.PERSONA.RECICLADORES.Any(SS => SS.Id == IdR) && S.Hora == Horario && (S.RECICLADORE == null || S.RECICLADORE.Id == IdR) && S.Fecha == Fecha.Date && S.Estado != 2).ToList();
                    }
                    else
                    {
                        SemiRes = context.PEDIDOS.Where(S => !S.PERSONA.RECICLADORES.Any(SS => SS.Id == IdR) && S.Hora == Horario && S.RECICLADORE == null && S.Fecha == Fecha.Date && S.PEDIDOS_TIPOS_DE_BASURAS.Any(SS => SS.IdTipoBasura == IdTR) && S.Estado != 2).ToList();
                    }
                    List<DTPedido> Res = new List<DTPedido>();
                    foreach (PEDIDO Index in SemiRes)
                    {
                        Res.Add(Index.GetDTO());
                    }
                    return Res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LockPedido(long IdR, long IdP)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.PEDIDOS.SingleOrDefault(S => S.Id == IdP).IdReciclador = context.RECICLADORES.SingleOrDefault(SS => SS.Id == IdR).Id;
                    context.PEDIDOS.SingleOrDefault(S => S.Id == IdP).Estado = 1;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void unLockPedido(long IdP)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.PEDIDOS.SingleOrDefault(S => S.Id == IdP).IdReciclador = null;
                    context.PEDIDOS.SingleOrDefault(S => S.Id == IdP).Estado = 0;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTPedido> TraerPedidosReciclador(long IdR, bool Horario, DateTime Fecha)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    List<PEDIDO> SemiRes = new List<PEDIDO>();
                    SemiRes = context.RECICLADORES.SingleOrDefault(S => S.Id == IdR).PEDIDOS.Where(SS => SS.Fecha == Fecha.Date && SS.Hora == Horario).ToList();
                    List<DTPedido> Res = new List<DTPedido>();
                    foreach (PEDIDO Index in SemiRes)
                    {
                        Res.Add(Index.GetDTO());
                    }
                    return Res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RecolectPedido(long IdP)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    context.PEDIDOS.SingleOrDefault(S => S.Id == IdP).Estado = 2;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha)
        {
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    List<PEDIDO> SemiRes = new List<PEDIDO>();
                    
                    if (ltTipos.Count == 0)
                    { // -1 es igual a no tomar el filtro
                        SemiRes = context.PEDIDOS.Where(S => !S.PERSONA.RECICLADORES.Any(SS => SS.Id == IdR) 
                        && S.Hora == Horario 
                        && (S.RECICLADORE == null || S.RECICLADORE.Id == IdR) 
                        && S.Fecha == Fecha.Date 
                        && S.Estado != 2
                        ).ToList();
                    }
                    else
                    {
                        List<long> TiposResiduoFiltrar = new List<long>();
                        foreach (DTTipoResiduo Index in ltTipos)
                        {
                            TiposResiduoFiltrar.Add(context.TIPOS_DE_BASURAS.SingleOrDefault(S=>S.Id==Index.id).Id);
                        }

                        SemiRes = context.PEDIDOS.Where(S => !S.PERSONA.RECICLADORES.Any(SS => SS.Id == IdR) 
                        && S.Hora == Horario 
                        && (S.RECICLADORE == null || S.RECICLADORE.Id == IdR)
                        && S.Fecha == Fecha.Date 
                        && S.Estado != 2
                        && S.PEDIDOS_TIPOS_DE_BASURAS.Any(ss=> TiposResiduoFiltrar.Contains(ss.IdTipoBasura))
                        ).ToList();
                    }
                    List<DTPedido> Res = new List<DTPedido>();
                    foreach (PEDIDO Index in SemiRes)
                    {
                        Res.Add(Index.GetDTO());
                    }
                    return Res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }// class RUsuarios END
}// namespace BusinessLogic.Repository END
