﻿using DataAccesClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObjects.Enums;
using DataTransferObjects.Entities;

namespace BusinessLogic.Repository
{
    class RPedidos
    {
        private ReciclatorvmEntities generateContext()
        {
            return RContextGenerator.generateContext();
        }

        public List<DTPedido> getPedidosPersona(long Id)
        {
            try
            {
                List<DTPedido> Res = new List<DTPedido>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    DateTime yearAgo = DateTime.Today.AddDays(-365);
                    List<PEDIDO> Temporal = context.PEDIDOS.Where(s => s.IdPersona == Id && s.Fecha >= yearAgo).ToList();

                    foreach (PEDIDO Index in Temporal) {
                        DTPedido tempPedido = null;
                        tempPedido = Index.GetDTO();

                        UBICACIONE tempUbi = context.UBICACIONES.Where(s => s.Id == tempPedido.Ubicacion.Id).FirstOrDefault();
                        tempPedido.Ubicacion = tempUbi.GetDTO();

                        Res.Add(tempPedido);
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTPedido> getPedidosReciclador(long Id)
        {
            try
            {
                List<DTPedido> Res = new List<DTPedido>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    DateTime yearAgo = DateTime.Today.AddDays(-365);
                    List<PEDIDO> Temporal = context.PEDIDOS.Where(s => s.IdReciclador== Id && s.Fecha >= yearAgo).ToList();

                    foreach (PEDIDO Index in Temporal)
                    {
                        DTPedido tempPedido = null;
                        tempPedido = Index.GetDTO();

                        UBICACIONE tempUbi = context.UBICACIONES.Where(s => s.Id == tempPedido.Ubicacion.Id).FirstOrDefault();
                        tempPedido.Ubicacion = tempUbi.GetDTO();

                        Res.Add(tempPedido);
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTPedido> getAllPedidosReciclador(long Id)
        {
            try
            {
                List<DTPedido> Res = new List<DTPedido>();
                using (ReciclatorvmEntities context = generateContext())
                {
                    List<PEDIDO> Temporal = context.PEDIDOS.Where(s => s.IdReciclador == Id).ToList();
                    foreach (PEDIDO Index in Temporal)
                    {
                        DTPedido tempPedido = null;
                        tempPedido = Index.GetDTO();

                        UBICACIONE tempUbi = context.UBICACIONES.Where(s => s.Id == tempPedido.Ubicacion.Id).FirstOrDefault();
                        tempPedido.Ubicacion = tempUbi.GetDTO();

                        Res.Add(tempPedido);
                    }
                }
                return Res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PEDIDO getPedido(long Id)
        {
            try
            {
                PEDIDO ped = null;
                using (ReciclatorvmEntities context = generateContext())
                {
                    ped = context.PEDIDOS.Where(s => s.Id == Id).SingleOrDefault();
                }
                return ped;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void createPedido(long Id)
        {
            try
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updatePedido(long Id)
        {
            try
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void updatePedidosResumen(DTPedido pedido)
        {          
            try
            {
                using (ReciclatorvmEntities context = generateContext())
                {
                    PEDIDO pedi = context.PEDIDOS.Where(s => s.Id == pedido.Id).SingleOrDefault();

                    if (pedido.Observaciones != null)
                    {
                        pedi.Observaciones = pedido.Observaciones;
                    }
                    pedi.Valoracion = (int)pedido.Valoracion;

                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }//class RPedidos END
}// namespace BusinessLogic.Repository END
