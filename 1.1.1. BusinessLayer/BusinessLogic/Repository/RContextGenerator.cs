﻿using DataAccesClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Repository
{
    class RContextGenerator
    {
        public static ReciclatorvmEntities generateContext()
        {
            return new ReciclatorvmEntities(DynamicConnectionBuilder.build());
        }

    }
}
