﻿using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerUsuarios
    {
        DTLogIn userLogIn(string nick, string pass);

        DTLogIn userExternalLogIn(string email);

        bool emailUserValidation(string email, string validationToken);

        bool isRecolector(string nick);

        bool isAdmin(string nick);

        bool isPersona(string nick);

        bool checkMail(string email);

        void RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        void RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        bool checkUser(string nick);

        Byte[] getUserImage(long Id);

        List<Ubicacion> getUbicacionesPersona(long Id);

        void BorrarBajaUbicacion(long id);

        void AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud);

        void CambiarFotoPerfilPersona(Byte[] InfoImagen, long id);

        void CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen);

        List<DTContainer> getContainersNoLlenos();

        void modifyContainerLleno(long IdC, long IdP);

        List<string> TraeTiposBasuraString();

        void AgregarTipoDeBasura(string Nombre, string Color);

        void AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos);
        List<DTMiReciclador> TraerMisRecicladores(long idPersona);

        void BloquearReciclador(long idPersona, long idReciclador);

        void DesbloquearReciclador(long idPersona, long idReciclador);

        List<string> TraerListaUsuarios();

        DTLogIn GetDatosPersona(string nickname);

        List<DTContainer> getContainersAll();

        void modifyContainerLlenoAdmin(long IdC);

        List<DTMaterialCantidad> TraerInfoMaterialCantidad(long IdPedido);

        void EliminarContainer(long IdC);

        void AddContainer(long IdTB, double Latitud, double Longitud);

        List<DTPedido> TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha);
    }
}
