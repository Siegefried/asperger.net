﻿using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using BusinessLogic.Util;
using DataAccesClassLibrary;
using DataTransferObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BusinessLogic.Controllers
{
    public class ControllerUsuarios : IControllerUsuarios
    {

        public DTLogIn userLogIn(string nick, string pass)
        {
            USUARIO usr = new RUsuarios().GetUsuarioByNick(nick);
            if (usr != null)
            {
                if (usr.Password == pass)
                {
                    return usr.GetDTO();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public DTLogIn userExternalLogIn(string email)
        {
            USUARIO usr = new RUsuarios().GetUsuarioByEmail(email);
            if (usr != null)
            {
                return usr.GetDTO();
            }
            else
            {
                return null;
            }
        }

        public bool isRecolector(string nick)
        {
            return new RUsuarios().isReciclador(nick);
        }

        public bool isAdmin(string nick)
        {
            return new RUsuarios().isAdmin(nick);
        }

        public bool isPersona(string nick)
        {
            return new RUsuarios().isPersona(nick);
        }

        public bool checkMail(string email)
        {
            return new RUsuarios().checkMail(email);
        }

        public void RegistrarPersona(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            String token = this.generateUserValidationToken();
            new RUsuarios().RegistrarPersona(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen, token);
            //ToDo: REFACT
            //USUARIO usr = new RUsuarios().GetUsuarioByNick(User);
            new SendMail().registrationConfirmation(User, Mail, token);
        }

        public void RegistrarReciclador(string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            String token = this.generateUserValidationToken();
            new RUsuarios().RegistrarReciclador(Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen, token);
            //ToDo: REFACT
            //USUARIO usr = new RUsuarios().GetUsuarioByNick(User);
            new SendMail().registrationConfirmation(User, Mail, token);
        }

        public bool checkUser(string nick)
        {
            return new RUsuarios().checkUser(nick);
        }

        public Byte[] getUserImage(long Id)
        {
            return new RUsuarios().getUserImage(Id);
        }

        private string generateUserValidationToken()
        {
            int length = 45;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());

            //return "dummytoktoken_ovEndlessTrveHate";
        }

        public List<Ubicacion> getUbicacionesPersona(long Id)
        {
            List<UBICACIONE> Temporal = new RUsuarios().getUbicacionesPersona(Id);
            List<Ubicacion> Res = new List<Ubicacion>();
            foreach (UBICACIONE Index in Temporal)
            {
                Res.Add(Index.GetDTO());
            }
            return Res;
        }

        public List<Ubicacion> getUbicacionReciclador(long Id)
        {
            List<UBICACIONE> Temporal = new RUsuarios().getUbicacionReciclador(Id);
            List<Ubicacion> Res = new List<Ubicacion>();
            foreach (UBICACIONE Index in Temporal)
            {
                Res.Add(Index.GetDTO());
            }
            return Res;
        }

        public List<DTPedido> getPedidosPersona(long Id)
        {
            return new RPedidos().getPedidosPersona(Id);
        }

        public List<DTPedido> getPedidosReciclador(long Id)
        {
            return new RPedidos().getPedidosReciclador(Id);
        }

        public List<DTPedido> getAllPedidosReciclador(long Id)
        {
            return new RPedidos().getAllPedidosReciclador(Id);
        }

        public void BorrarBajaUbicacion(long id)
        {
            new RUsuarios().BorrarBajaUbicacion(id);
        }

        public void AddUbicacionPersona(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new RUsuarios().AddUbicacionPersona(idPersona, Alias, Latitud, Longitud);
        }

        public void AddUbicacionReciclador(long idPersona, string Alias, double Latitud, double Longitud)
        {
            new RUsuarios().AddUbicacionReciclador(idPersona, Alias, Latitud, Longitud);
        }

        public void CambiarFotoPerfilPersona(Byte[] InfoImagen, long id)
        {
            new RUsuarios().CambiarFotoPerfilPersona(InfoImagen, id);
        }

        public void CambiarDatosUsuario(long id, string Nombre, string Apellido, string User, string Pass, int Edad, string Mail, Byte[] InfoImagen)
        {
            new RUsuarios().CambiarDatosUsuario(id, Nombre, Apellido, User, Pass, Edad, Mail, InfoImagen);
        }

        public List<DTContainer> getContainersNoLlenos()
        {
            //List<DTContainer> Res = new List<DTContainer>();
            //List<CONTAINER> Temporal = new RUsuarios().getContainersNoLlenos();
            //foreach (CONTAINER index in Temporal) {
            //    Res.Add(index.GetDTO());
            //}
            //return Res;
            return new RUsuarios().getContainersNoLlenos();
        }

        public void modifyContainerLleno(long IdC, long IdP)
        {
            new RUsuarios().modifyContainerLleno(IdC, IdP);
        }

        public bool emailUserValidation(string email, string validationToken)
        {
            return new RUsuarios().mailValidation(email, validationToken);
        }

        public bool CheckTipoDeBasura(string NombreTipoDeBasura)
        {
            return new RUsuarios().CheckTipoDeBasura(NombreTipoDeBasura);
        }

        public void AgregarTipoDeBasura(string Nombre, string Color)
        {
            new RUsuarios().AgregarTipoDeBasura(Nombre, Color);
        }

        public List<string> TraeTiposBasuraString()
        {
            return new RUsuarios().TraeTiposBasuraString();
        }

        public void AgregarPedido(DateTime Fecha, bool Hora, long IdPersona, long IdUbicacion, string Observaciones, List<DTMaterialCantidad> Residuos)
        {
            new RUsuarios().AgregarPedido(Fecha, Hora, IdPersona, IdUbicacion, Observaciones, Residuos);
        }

        public void updatePedidosResumen(DTPedido pedido)
        {
            new RPedidos().updatePedidosResumen(pedido);
        }

        public List<DTMiReciclador> TraerMisRecicladores(long idPersona)
        {
            return new RUsuarios().TraerMisRecicladores(idPersona);
        }

        public void BloquearReciclador(long idPersona, long idReciclador)
        {
            new RUsuarios().BloquearReciclador(idPersona, idReciclador);
        }

        public void DesbloquearReciclador(long idPersona, long idReciclador)
        {
            new RUsuarios().DesbloquearReciclador(idPersona, idReciclador);
        }

        public List<string> TraerListaUsuarios()
        {
            return new RUsuarios().TraerListaUsuarios();
        }

        public DTLogIn GetDatosPersona(string nickname)
        {
            return new RUsuarios().GetDatosPersona(nickname);
        }

        public List<DTContainer> getContainersAll()
        {
            return new RUsuarios().getContainersAll();
        }

        public void modifyContainerLlenoAdmin(long IdC)
        {
            new RUsuarios().modifyContainerLlenoAdmin(IdC);
        }

        public List<DTMaterialCantidad> TraerInfoMaterialCantidad(long IdPedido)
        {
            return new RUsuarios().TraerInfoMaterialCantidad(IdPedido);
        }

        public void EliminarContainer(long IdC)
        {
            new RUsuarios().EliminarContainer(IdC);
        }

        public void AddContainer(long IdTB, double Latitud, double Longitud)
        {
            new RUsuarios().AddContainer(IdTB, Latitud, Longitud);
        }

        public void BloquearDesbloquearUsuario(long Id)
        {
            new RUsuarios().BloquearDesbloquearUsuario(Id);
        }

        public List<DTTipoResiduo> TraerDatosTiposDeBasura()
        {
            return new RUsuarios().TraerDatosTiposDeBasura();
        }

        public void BloquearDesbloquearTipoResiduo(long Id)
        {
            new RUsuarios().BloquearDesbloquearTipoResiduo(Id);
        }

        public void EditarTipoResiduo(long Id, string Nombre, string Color)
        {
            new RUsuarios().EditarTipoResiduo(Id, Nombre, Color);
        }

        public bool isUsurio(long Id)
        {
            return new RUsuarios().isUsurio(Id);
        }

        public bool isTipoResiduo(long Id)
        {
            return new RUsuarios().isTipoResiduo(Id);
        }

        public bool ExisteTipoResiduo(string Nombre)
        {
            return new RUsuarios().ExisteTipoResiduo(Nombre);
        }

        public List<DTContainer> getContainersLlenos()
        {
            return new RUsuarios().getContainersLlenos();
        }

        public List<DTEstadistica<DTLogIn>> rankPersonas(int daysBefore)
        {
            return new RUsuarios().rankPersonas(daysBefore);
        }

        public List<DTEstadistica<DTLogIn>> rankRecicladores(int daysBefore)
        {
            return new RUsuarios().rankRecicladores(daysBefore);
        }

        public List<DTEstadistica<DTTipoResiduo>> tiposResiduosMasReciclados(int daysBefore)
        {
            return new RUsuarios().tiposResiduosMasReciclados(daysBefore);
        }

        public Ubicacion getUbicacionPrincipalRecicladores(long IdR)
        {
            return new RUsuarios().getUbicacionPrincipalRecicladores(IdR);
        }

        public void modifyUbicacionPrincipalRecicladores(long IdR, double Latitud, double Longitud)
        {
            new RUsuarios().modifyUbicacionPrincipalRecicladores(IdR, Latitud, Longitud);
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorFiltro(long IdR, long IdTR, bool Horario, DateTime Fecha) {
            return new RUsuarios().TraerPedidosDisponiblesRecicladorFiltro(IdR, IdTR, Horario, Fecha);
        }

        public void LockPedido(long IdR, long IdP) {
            new RUsuarios().LockPedido(IdR, IdP);
        }

        public void unLockPedido(long IdP)
        {
            new RUsuarios().unLockPedido(IdP);
        }

        public List<DTPedido> TraerPedidosReciclador(long IdR, bool Horario, DateTime Fecha) {
            return new RUsuarios().TraerPedidosReciclador(IdR, Horario, Fecha);
        }

        public void RecolectPedido(long IdP)
        {
            new RUsuarios().RecolectPedido(IdP);
        }

        public List<DTPedido> TraerPedidosDisponiblesRecicladorMultiFiltro(long IdR, List<DTTipoResiduo> ltTipos, bool Horario, DateTime Fecha)
        {
            return new RUsuarios().TraerPedidosDisponiblesRecicladorMultiFiltro(IdR, ltTipos, Horario, Fecha);
        }
    }
}
